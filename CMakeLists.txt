project (OmniSensr_SampleAppExec)
set (PROJECT_NAME pi_health)

SET(CMAKE_CXX_FLAGS "-std=c++0x")

set(IOT_CLIENT_DIR "../../../external/aws-iot-sdk/aws_iot_src")

set (PROJECT_SRC	
	main.cpp
)

set (INCLUDE_ALL_DIRS	
	${PROJECT_HEADER_FILES}  
	${VML_ROOT}/include/modules/utility
	${VML_ROOT}/include/modules/iot
	${IOT_CLIENT_DIR}/shadow
	${IOT_CLIENT_DIR}/utils
)

add_executable(${PROJECT_NAME} 
	${PROJECT_SRC}	
	${INCLUDE_ALL_DIRS}
)
target_include_directories(${PROJECT_NAME}  
	PUBLIC 
	${INCLUDE_ALL_DIRS}
	${VML_ROOT}/include/projects/${PROJECT_NAME} 
)

include_directories( 
	${INCLUDE_ALL_DIRS}
)

target_link_libraries(${PROJECT_NAME} 
	LINK_PUBLIC 
	vmlutility 
	vmliot 
	pthread
	mosquitto
	mosquittopp
)