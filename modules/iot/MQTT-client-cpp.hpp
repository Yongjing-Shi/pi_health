/*
 * MQTT-client-cpp.hpp
 *
 *  Created on: Apr 28, 2015
 *      Author: pshin
 */

#ifndef MQTT_CLIENT_CPP_HPP_
#define MQTT_CLIENT_CPP_HPP_

#include "mosquittopp.h"
#include <vector>
#include <string>
#include <sstream>

class MosqMQTT : public mosqpp::mosquittopp
{
protected:

	virtual void on_connect(int rc);
	virtual void on_disconnect(int rc);
	virtual void on_publish(int mid);
	virtual void on_subscribe(int mid, int qos_count, const int *granted_qos);
	virtual void on_message(const struct mosquitto_message *message);
	virtual bool match(const char *topic, const char *key);

public:

	std::ostringstream 	mClientId;
	pid_t 				mPid;

	std::string     	mBrokerAddr;	/* Host name for the MQTT broker. */
	int                	mBrokerPort;	/* Host port for the MQTT broker. */
	int                	mKeepalive;		/* How many seconds the broker should wait between sending out keep-alive messages. */
	std::string     	mPubTopic_Data;		/* Topic to publish with */
	std::string     	mUsername;		/* username */
	std::string     	mPassword;		/* password */
	int 				mQoS;			/* Quality-of-service */
	std::vector<std::string>     mSubTopics;	/* Topics to subscribe */


	MosqMQTT(const char *id=NULL, bool clean_session=true);
	~MosqMQTT();

	void start();
	int send_message(std::string pubTopic, std::string _message, int qos);
	void subscribeToTopic(std::string subTopic);
};

#endif /* MQTT_CLIENT_CPP_HPP_ */
