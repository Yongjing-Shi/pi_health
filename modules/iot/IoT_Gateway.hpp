/*
 * IoT_Gateway.hpp
 *
 *  Created on: Mar 3, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_IOT_IOT_GATEWAY_HPP_
#define INCLUDE_MODULES_IOT_IOT_GATEWAY_HPP_

#include "stdint.h"
//#include "modules/iot/IoT_Common.hpp"
//#include "aws_iot_error.h"
//#include "aws_iot_shadow_json_data.h"
#include "IoT_Common.hpp"
#include "aws_iot_error.h"
#include "aws_iot_shadow_json_data.h"
#include <vector>

// -- For MQTT
#include "MQTT-client-cpp.hpp"

using namespace std;

// -- The IoT_Gateway class inherits MQTT communication capability from a Mosquitto MQTT library, MosqMQTT.
class IoT_Gateway : public MosqMQTT {

protected:

	static vector<devState_t > mDevState;

	string mMqttConfigPath;
	string mMqttTopic_State;
	string mMqttTopic_Data;

	string mAP_IPaddr;
	string mStoreName;
	string mSerialNumber;

	string mNIC;
	string mSoftwareVersion;

	double	mInitialTime;
	pthread_attr_t mThreadAttr;
	pthread_t mMessageSending_thread;

	ostringstream mStateMsgToSend;
	ostringstream mDataMsgToSend;

	void (*mDeltaStateCallbackFunc)(string);
	void (*mControlChannelCallbackFunc)(string);

	static void *MessageSendingThread(void *apData);
	void MessageSending_Loop();

	bool ParseSetting(string configpath);

			// -- Parameters and functions for MosqMQTT class
			bool mIsMqttConnected;

			void on_message(const struct mosquitto_message *message);
			void on_connect(int rc);
			void on_disconnect(int rc);
			void on_subscribe(int mid, int qos_count, const int *granted_qos);

public:

	IoT_Gateway(const char * _id = "temptemp", bool clean_session = false);
	~IoT_Gateway();

	bool Run(string appID, string topic_state, string iot_gateway_configfile);
	void SendStateInfo();
	void SendData(const string &msg);

	void RegisterDeltaCallbackFunc(void (*IoT_DeltaState_CallBack_func)(string)) {
		mDeltaStateCallbackFunc = IoT_DeltaState_CallBack_func;
	};

	void RegisterControlCallbackFunc(void (*IoT_ControlChannel_CallBack_func)(string)) {
		mControlChannelCallbackFunc = IoT_ControlChannel_CallBack_func;
	};

	size_t AddStateParam(unsigned int type, string key, void *value, void *cb(void *));

	static devState_t *FindStateParam(string paramKey);

};



#endif /* INCLUDE_MODULES_IOT_IOT_GATEWAY_HPP_ */
