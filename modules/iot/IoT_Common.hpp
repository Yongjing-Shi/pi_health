/*
 * IoT_Common.hpp
 *
 *  Created on: March 3, 2016
 *      Author: paulshin
 */

#ifndef MODULE_IOT_COMMON_HPP_
#define MODULE_IOT_COMMON_HPP_

#include <string>

/* OmniSensr_AWS_IoT_Connector expects that this control channel is used with the specific store name.
 * For example, if a IoT Cloud-side application wants to send a project setting via this channel, then the cloud-side application should send that via 'control/<store_name>' channel like 'control/GTE6097'
 * The message itself may contain a more specific header info at the beginning of each message.
 * Or, the cloud-side application may use more nested topic structure, and in such case, the OmniSensr-side application should subscribe to appropriate topics (e.g., 'control/<store_name>/<more_detailed_topic>') to receive the project setting.
 */
#define AWS_IOT_CONTROL_CHANNEL_PREAMBLE	"control"

/*
 * The Delta Thing Shadow messages from the AWS IoT Cloud will be available from this channel at the internal MQTT broker
 */
#define AWS_IOT_SHADOW_DELTA_CHANNEL_NAME	"state/delta"

/*
 * OmniSensr-side applications wouldn't need to care about this since AWS_IoT_Gateway will use this to prepend to the state topics that the OmniSensr-side applications use to publish their state to the internal MQTT broker.
 * And, the OmniSensr_AWS_IoT_Connector will subscribe all the nested topics from this channel to aggregate the state info from all the OmniSensr-side applications.
 */
#define AWS_IOT_APP_STATE_CHANNEL_PREAMBLE	"state"

/*
 * OmniSensr-side applications wouldn't need to care about this since AWS_IoT_Gateway will use this to publish data to a topic 'data/<store_name_prefix>/<store_name_suffix>/<serial_number>/<app_id>' (e.g., data/vmhq/16801/A0000000001/wifi)
 */
#define AWS_IOT_APP_DATA_CHANNEL_PREAMBLE	"data"

/*
 * This will be prepended by AWS_IOT_APP_STATE_CHANNEL_PREAMBLE (e.g., 'state'), thus will become 'AWS_IOT_APP_STATE_CHANNEL_PREAMBLE/APP_ID_blabla' (e.g., 'state/sample') (see AWS_IOT_APP_STATE_CHANNEL_PREAMBLE in IoT_Common.hpp)
 * These are reserved App IDs for specific Apps.
 */
#define APP_ID_WIFI_PROCESSOR	"wifiRSS"	// -- App ID for WiFi_PacketProcessor
#define APP_ID_DEMO				"demo"		// -- App ID for Demographics
#define APP_ID_VISION_TRACKER	"vision"	// -- App ID for Vision Tracker
#define APP_ID_DOOR_COUNTER		"door"		// -- App ID for Door Counter
#define APP_ID_HEALTH			"health"	// -- App ID for Device Health App
#define APP_ID_WIFI_TRACKER		"wifiTrack"	// -- App ID for WiFi_Shopper_Tracker

#define APP_ID_SAMPLE_APP		"sample"	// -- App ID for Sample App

/*
 * You can use this App ID for your testing. This is not a reserved one.
 */
#define APP_ID_TEST				"test"		// -- App ID for a temporary test app

/*
 * The type of devState_t should be one of the followings EXCEPT SHADOW_JSON_STRING and SHADOW_JSON_OBJECT since they are not yet supported as of aws-iot-sdk ver 1.1.0
	typedef enum {
		SHADOW_JSON_INT32,
		SHADOW_JSON_INT16,
		SHADOW_JSON_INT8,
		SHADOW_JSON_UINT32,
		SHADOW_JSON_UINT16,
		SHADOW_JSON_UINT8,
		SHADOW_JSON_FLOAT,
		SHADOW_JSON_DOUBLE,
		SHADOW_JSON_BOOL,
		SHADOW_JSON_STRING,
		SHADOW_JSON_OBJECT
	} JsonPrimitiveType;
*/

struct devState_t {
	unsigned int type;		// -- type of devState_t should be one of the JsonPrimitiveType (shown above) EXCEPT SHADOW_JSON_STRING and SHADOW_JSON_OBJECT since they are not yet supported as of aws-iot-sdk ver 1.1.0
	std::string key;		// -- key for Thing Shadow. This must be ONE word, for example, "wifi-state", NOT "wifi state".
    void *value;		// -- Actual value
    void (*cb_func)(void *);
};

#define IOT_FLOAT_PRECISION			5		// -- In AWS IoT SDK, the precision of a float variable is only up to 5 decimal points, even though the float-type state in Thing Shadow message can transfer a much granular number as a string.
#define IOT_DOUBLE_PRECISION		12		// -- In AWS IoT SDK, the precision of a double variable is only up to 12 decimal points, even though the double-type state in Thing Shadow message can transfer a much granular number as a string.

const int MAX_IOT_STATE_TOKENS_PER_LINE = 1023;	// -- Max # of tokens in a line of tcpdump input
const char* const AWS_IOT_THINGSHADOW_DELIMITER = "\"{}:";


#endif /* MODULE_IOT_COMMON_HPP_ */
