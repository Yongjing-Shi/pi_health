#ifndef __MATRIX_H__
#define __MATRIX_H__

#define	SQR(a)		((a)*(a))
#define ABS(a)		(a>0?(a):-(a))

#define TRANSPOSE 1
#define N_TRANSPOSE 0

#define COL 1
#define ROW 0

#include <math.h>

void inline getSubMatrix(double* dst, const unsigned int dst_cols, const double* src, const unsigned int src_cols, const unsigned int x,const  unsigned int y, const unsigned int width, const unsigned int height);
void inline getVect(double* dst, const double* src, const unsigned int src_cols, const unsigned int x, const unsigned int y, const unsigned int height, const unsigned int col_vect);
void inline matrixMult(const double* src1, const unsigned int src1_rows, const unsigned int src1_cols, const double* src2, const unsigned int src2_rows, const unsigned int src2_cols, double *dst, const double scale, const unsigned int transpose);
void inline matrixSub(const double* src1, const double* src2, const unsigned int rows, const unsigned int cols, double *dst);
void inline matrixAdd(const double* src1, const double* src2, const unsigned int rows, const unsigned int cols, double *dst);

unsigned int inline matrixInv2x2(const double *src,double *dst);
unsigned int inline matrixInv3x3(const double *src,double *dst);


void inline matrixVectMult(double* src1, unsigned int src1_rows, unsigned int src1_cols, double* src2, unsigned int src2_rows, double *dst);
void inline vectMatrixMult(double* src1, unsigned int src1_cols, double* src2, unsigned int src2_rows, unsigned int src2_cols, double *dst, unsigned int transpose);


//*******************************************************************************************
// I NEED TO PUT THESE MATRIX FUNCTIONS SOMEWHERE ELSE!!!!
//*******************************************************************************************
void inline getSubMatrix(double* dst, const unsigned int dst_cols, const double* src, const unsigned int src_cols, const unsigned int x, const unsigned int y, const unsigned int height, const unsigned int width) {
	
	unsigned int i, j;
	
	for (i=0;i<width;i++) 
		for (j=0;j<height;j++)
			dst[i+j*dst_cols] = src[x+i+(y+j)*src_cols];
	
}
//*******************************************************************************************
void inline getVect(double* dst, const double* src, const unsigned int src_cols, const unsigned int x, const unsigned int y, const unsigned int height, const unsigned int col_vect) {

	unsigned int j;
	
	if (col_vect) {
		for (j=0;j<height;j++)
			dst[j] = src[x+(y+j)*src_cols];
	}
	else {
		for (j=0;j<height;j++)
			dst[j] = src[x+j+y*src_cols];
	}
	
}
//*******************************************************************************************
// Matrix Multiplication:
// dst = src1*src2  if (transpose = 0)
// or
// dst = src1*transpose(src2) if (transpose = 1)
//*******************************************************************************************
void inline matrixMult(const double* src1, const unsigned int src1_rows,
                       const unsigned int src1_cols, const double* src2,
                       const unsigned int src2_rows, const unsigned int src2_cols,
                       double *dst, const double scale, const unsigned int transpose) {

	unsigned int i,j,k;
	//it is faster to have two different loops, one for regular multiplication
	//and another for transposed, then to include the transposed flag
	//in each iteration (think about it latter!!!)
	//I should probably have different loops for scale=1 also
	if (transpose) { 
		for (i=0;i<src1_rows;i++)  {
			for (j=0;j<src2_rows;j++) {
				dst[j+i*src2_rows] = 0;
				for (k=0;k<src1_cols;k++)
					dst[j+i*src2_rows] += src1[k+i*src1_cols]*src2[k+j*src2_cols];
				dst[j+i*src2_rows] *= scale;
			}
		}
	}
	else {
		for (i=0;i<src1_rows;i++)  {
			for (j=0;j<src2_cols;j++) {
				dst[j+i*src2_cols] = 0;
				for (k=0;k<src1_cols;k++)
					dst[j+i*src2_cols] += src1[k+i*src1_cols]*src2[j+k*src2_cols];
				dst[j+i*src2_cols] *= scale;
			}
		}
	}

}
//*******************************************************************************************
void inline matrixSub(const double* src1, const double* src2, const unsigned int rows, const unsigned int cols, double *dst) {

	unsigned int i,j;
	
	for (i=0;i<rows;i++)
		for (j=0;j<cols;j++) 

			 dst[j+i*cols] = src1[j+i*cols]-src2[j+i*cols];


}
//*******************************************************************************************
void inline matrixAdd(const double* src1, const double* src2, const unsigned int rows, const unsigned int cols, double *dst) {

	unsigned int i,j;
	
	for (i=0;i<rows;i++)
		for (j=0;j<cols;j++)
			dst[j+i*cols] = src1[j+i*cols]+src2[j+i*cols];

}
//*******************************************************************************************
void inline matrixScale(const double* matrix, const double scale, const unsigned int rows, const unsigned int cols, double *dst) {

	unsigned int i,j;

	for (i=0;i<rows;i++)
		for (j=0;j<cols;j++)
			dst[j+i*cols] = scale*matrix[j+i*cols];

}
//*******************************************************************************************
void inline matrixTranspose(const double* matrix, const unsigned int rows, const unsigned int cols, double *dst) {

    unsigned int i,j;

    for (i=0;i<rows;i++)
        for (j=0;j<cols;j++)
            dst[j+i*cols] = matrix[i+j*cols];

}
//*******************************************************************************************
// returns 0 if the matrix is singular
//*******************************************************************************************
unsigned int inline matrixInv2x2(const double *src,double *dst) {

	double det;
	
	det = src[0]*src[3]-src[1]*src[2];
	if (det == 0)
		return 0;
		
	dst[0] = src[3]/det;
	dst[1] = -src[1]/det;
	dst[2] = -src[2]/det;
	dst[3] = src[0]/det;
	
	return 1;
}
//*******************************************************************************************
// returns 0 if the matrix is singular
//*******************************************************************************************
unsigned int inline matrixInv3x3(const double *src, double *dst) {

	double A[2][2], U[2][2], Ui[2][2];
	double B[2], C[2];
	double di;
	
	double tmp2x2_1[2][2];
	double tmp2_1[2];
	double tmp1_1;
	
	//double dst[9];
	//unsigned int i = 0;
	
	getSubMatrix(&A[0][0],2,src,3,0,0,2,2);
	getSubMatrix(B,1,src,3,2,0,2,1);
	getSubMatrix(C,2,src,3,0,2,1,2);
	di = 1/src[8];
	
	//find schur's complement
	matrixMult(B,2,1,C,1,2,&tmp2x2_1[0][0],di,N_TRANSPOSE);
	matrixSub(&A[0][0],&tmp2x2_1[0][0],2,2,&U[0][0]);
	if (matrixInv2x2(&U[0][0],&Ui[0][0]) == 0) {
		return 0;
	}
	
	dst[0] = Ui[0][0];
	dst[1] = Ui[0][1];
	dst[3] = Ui[1][0];
	dst[4] = Ui[1][1];
	
	matrixMult(&Ui[0][0],2,2,B,2,1,tmp2_1,-di,N_TRANSPOSE);
	
	dst[2] = tmp2_1[0];
	dst[5] = tmp2_1[1];
	
	matrixMult(C,1,2,&Ui[0][0],2,2,tmp2_1,-di,N_TRANSPOSE);
	
	dst[6] = tmp2_1[0];
	dst[7] = tmp2_1[1];
	
	matrixMult(tmp2_1,1,2,B,2,1,&tmp1_1,-di,N_TRANSPOSE);
	
	dst[8] = di+tmp1_1;
	/*
	for(i=0; i < 9; i++) {
	   dst_s[i] = dst[i]; 
	}*/
	
	return 1;
}

//*******************************************************************************************
// Matrix Vector multiplication:
// dst = src1*src2
// assumes a column vector
//*******************************************************************************************
void inline matrixVectMult(double* src1, unsigned int src1_rows, unsigned int src1_cols, double* src2, unsigned int src2_rows, double *dst) {
	
	unsigned int j,k;
	
	for (j=0;j<src1_rows;j++) {
		dst[j] = 0;
		for (k=0;k<src1_cols;k++)
			dst[j] += src1[k+j*src1_cols]*src2[k];
	}
}
//*******************************************************************************************
// Vector Matrix multiplication:
// dst = src1*src2
// assumes a row vector
//*******************************************************************************************
void inline vectMatrixMult(double* src1, unsigned int src1_cols, double* src2, unsigned int src2_rows, unsigned int src2_cols, double *dst, unsigned int transpose) {

	unsigned int j,k;
	
	if (transpose) {
		for (j=0;j<src2_rows;j++) {
			dst[j] = 0;
			for (k=0;k<src1_cols;k++)
				dst[j] += src2[k+j*src2_cols]*src1[k];
		}
	}
	else {
		for (j=0;j<src2_cols;j++) {
			dst[j] = 0;
			for (k=0;k<src1_cols;k++)
				dst[j] += src2[j+k*src2_cols]*src1[k];
		}
	}
}


#endif // __MATRIX_H__
