///   \file    TimeKeeper.hpp
///   \brief   A collection of functions for measuring process speed

#ifndef _TIME_KEEPER_H
#define _TIME_KEEPER_H

///   \brief    Start the global reference time
///   \date     Jun 24, 2006
///   \author   jpark
void gStartTime();

///   \brief    Save the current time
///   \date     Jun 24, 2006
///   \author   jpark
void gSaveTime();

///   \brief    Print the current time
///   \date     Jun 24, 2006
///   \author   jpark
void gPrintTime();

///   \brief    Print the ellapsed time since the last saved time
///   \date     Jun 24, 2006
///   \author   jpark
void gPrintTimeSinceLastSaved();

///   \brief    Returns the ellapsed time since the last saved time
///   \date     Jun 24, 2006
///   \author   jpark
///   \return   The ellapsed time since the last saved time
double gTimeSinceLastSaved();

///   \brief    Print the ellapsed time since the start
///   \date     Jun 24, 2006
///   \author   jpark
void gPrintTimeSinceStart();

///   \brief    Returns the ellapsed time since the start
///   \date     Jun 24, 2006
///   \author   jpark
///   \return   The ellapsed time since the start
double gTimeSinceStart();

///   \brief    Retuns the current time
///   \date     Jun 24, 2006
///   \author   jpark
///   \return   The current time
double gCurrentTime();

#endif
