/*
 * KalmanFilter_4S.hpp
 *
 *  Created on: Dec 11, 2011
 *      Author: paulshin
 */

#ifndef KALMANFILTER_4S_HPP_
#define KALMANFILTER_4S_HPP_

typedef struct {
	unsigned int	r, g, b;
}  __attribute__ ((packed)) Color_t;

class KalmanFilter_4S
{

public:

	KalmanFilter_4S(); //
	virtual ~KalmanFilter_4S(); //

	//char mAP_IPaddr[18];
	int	mCenterOfSensingField_X;
	int	mCenterOfSensingField_Y;

	Color_t mColor;

	int	mNoOfUpdatesSoFar;
	bool mNeedToInitialize;

	void Initialize(double x, double y, double P11 = 5000*5000.f, double P22 = 5000*5000.f);
	void DeepInitialize(double kalmanProcessNoiseVariance_t = 1., double kalmanMeasurementNoiseVariance_t = 1., double kalmanTimeScale = 1., double minCovCoefficient_pos = 1., double minCovCoefficient_accel = 1., double x = 100., double y = 100., double P11 = 5000*5000.f, double P33 = 5000*5000.f);
	void ResetColor();

//	void InitCovariance();
	void Update(double x, double y, double measurementNoiseScaleFactor = 1.0);
	//void Update_wo_timstamping(double x, double y);
	void Update_wo_timstamping(double x, double y, double measurementNoiseScaleFactor = 1.0);

			void set_state(const double *eX);
//			void set_state_predict(const double *eX);
	        void get_state(double *eX);
	        void get_predicted_state(double *eX);
	        void setCovariance(int *cov);
//	        void setCovariance_predict(int *cov);
	        void getCovariance(int *cov);
	        void getCovariance_float(double *cov);
	        void setCovariance_float(const double *cov);
//	        void getXYCovariance_float(double (*cov)[2]);
//	        void setXYCovariance(int *cov);
//	        void setXYCovariance_int16_onlyXY(int16_t *cov);
//
//	        void setProcessNoise(double q00, double q11);
//	        void setMeasurementNoise(double r00, double r11);

//			void getXYCovariance_float_predicted(double (*cov)[2]);

	void PredictForPrediction();
//	void 	PredictForAdaptation_for_visualization();
//	double GaussianRandNumGenerator();
	double timeElapsedFromLastUpdate();
	double timeElapsedFromInitialUpdate();

	/*

	void drawUncertaintyEllipse(double scale, double r, double g, double b, bool line_stipple, double line_width);
    void drawUncertaintyEllipse_prediction(double scale, double r, double g, double b, double line_width);
    void drawUncertaintyEllipse_prediction_vis(double scale, double r, double g, double b);		// -- Only for visualization
    void drawTrackedTargetID(int feature_id);
    */

protected:

	double x_h_predict[4], x_h_update[4];
	double P_predict[4][4], P_update[4][4]; //, P_update_temp[4][4];
	double Phi[4][4], Q[2][2], R[2][2], G[4][2], H[2][4], Kgain[4][2];
	double x_0[4], P_0[16];
	double z[2];

	double Phi_predict[4][4], x_h_predictpredict[4], P_predictpredict[4][4];

	double	mKalmanProcessNoiseVariance;
	double	mKalmanPredictNoiseVariance;
	double	mKalmanMeasurementNoiseVariance;
	double 	mKalmanTimeScale;
	double 	mMinCovCoefficient_pos;
	double 	mMinCovCoefficient_accel;

	double 	mInitTime;
	double	mMeasurementTime_prev;		// -- Unit: sec

	void PredictForUpdate(double x, double y);
};


#endif /* KALMANFILTER_4S_HPP_ */
