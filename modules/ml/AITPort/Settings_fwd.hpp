/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AIT_Settings_fwd_HPP
#define AIT_Settings_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace aitvml
{

class Settings;
typedef boost::shared_ptr<Settings> SettingsPtr;

}; // namespace AITPort

#endif // Settings_fwd_HPP

