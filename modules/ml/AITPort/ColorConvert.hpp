/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ColorConvert_HPP
#define ColorConvert_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <modules/ml/AITPort/String.hpp>
#include <modules/ml/AITPort/Rectangle.hpp>
#include <modules/ml/AITPort/Image.hpp>
#include <modules/ml/AITPort/PvImageProc.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace aitvml
{

namespace colorConverters
{

class RGBtoCrCgI
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
    aux = (int)r+(int)g+(int)b+1;
		out0 = (Uint8)((256*(int)r)/aux);
		out1 = (Uint8)((256*(int)g)/aux);
    out2 = (Uint8)((aux-1)/3);
  }

};

class RGBtoI1I2I3
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
		out0 = (Uint8)(((int)r+(int)g+(int)b)/3);
		out1 = (Uint8)(((int)r-(int)g)/2 + 128);
		out2 = (Uint8)((((int)g)*2-(int)r-(int)b)/4 + 128);
  }

};

class RGBtoFastGray
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out, Aux& aux)
  {
		out = (Uint8)(((int)r+(int)g+(int)b)/3);
  }

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
		convert(r,g,b,out0,aux);
		out1 = out0;
		out2 = out0;
  }

};

class RGBtoGray
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out, Aux& aux)
  {
		out = (Uint8)((((int)r)*299+((int)g)*587+((int)b)*114)/1000);
  }

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
		convert(r,g,b,out0,aux);
		out1 = out0;
		out2 = out0;
  }

};

class RGBtoRGB
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
		out0 = r;
		out1 = g;
    out2 = b;
  }

};

class RGBtoNormal0
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
    out0 = (Uint8)(std::max)(0,(std::min)(255,((int)b-(int)g)*10+128));
		out1 = (Uint8)(std::max)(0,(std::min)(255,((int)g-(int)r)*10+128));
		out2 = (Uint8)(((int)r+(int)g+(int)b)/3);
  }

};

class RGBtoNormal1
{
public:
  typedef int Aux;

  static void convert(const Uint8 r, const Uint8 g, const Uint8 b,
                      Uint8& out0, Uint8& out1, Uint8& out2, Aux& aux)
  {
		out0 = (Uint8)(std::max)(0,(std::min)(255,((int)r-(int)g) + 128));
		out1 = (Uint8)(std::max)(0,(std::min)(255,(((int)g)*2-(int)r-(int)b) + 128));
		out2 = (Uint8)(((int)r+(int)g+(int)b)/3);
  }

};

} // namespace colorConverters

template <class Converter> 
class ColorConvertBase
{
public:

  static void convert3CTo3C(const Image32& in, 
                            Image32& out, 
                            const Rectanglei& roi)
  {
    PVASSERT(Rectanglei(0,0,in.width(),in.height()).inside(roi));
    
    out.resize(roi.width(),roi.height());
    
    Uint8 *pSrc = (Uint8*)(in.pointer()+in.width()*roi.y0+roi.x0);
    Uint8 *pDst = (Uint8*)(out.pointer());
    Uint skipLineSrc = 4*(in.width()-roi.width());
    Uint8 *pDstEnd = pDst + 4*(out.width()*out.height());
    
    typename Converter::Aux aux;
    
    while (pDst < pDstEnd)
    {
      int count = out.width();
      while (count-- > 0)
      {
        Converter::convert(pSrc[R_IDX],pSrc[G_IDX],pSrc[B_IDX],
                           pDst[R_IDX],pDst[G_IDX],pDst[B_IDX],
                           aux);
        pSrc += 4;
        pDst += 4;
      }
      pSrc += skipLineSrc;
    }

    PVASSERT(pDst == pDstEnd);
  }

  static void convert3CTo3P(const Image32& in, 
                            Image8& out0, 
                            Image8& out1, 
                            Image8& out2, 
                            const Rectanglei& roi)
  {
    PVASSERT(Rectanglei(0,0,in.width(),in.height()).inside(roi));
    
    out0.resize(roi.width(),roi.height());
    out1.resize(roi.width(),roi.height());
    out2.resize(roi.width(),roi.height());
    
    Uint8 *pSrc = (Uint8*)(in.pointer()+in.width()*roi.y0+roi.x0);
    Uint8 *pDst0 = (Uint8*)(out0.pointer());
    Uint8 *pDst1 = (Uint8*)(out1.pointer());
    Uint8 *pDst2 = (Uint8*)(out2.pointer());
    Uint skipLineSrc = 4*(in.width()-roi.width());
    Uint8 *pDstEnd = pDst0 + out0.width()*out0.height();
    
    typename Converter::Aux aux;
    
    while (pDst0 < pDstEnd)
    {
      int count = out0.width();
      while (count-- > 0)
      {
        Converter::convert(pSrc[R_IDX],pSrc[G_IDX],pSrc[B_IDX],
                           *pDst0,*pDst1,*pDst2,
                           aux);
        pSrc += 4;
        pDst0++;
        pDst1++;
        pDst2++;
      }
      pSrc += skipLineSrc;
    }
    
    PVASSERT(pDst0 == pDstEnd);
  }

  static void convert3CTo1P(const Image32& in, 
                            Image8& out, 
                            const Rectanglei& roi)
  {
    PVASSERT(Rectanglei(0,0,in.width(),in.height()).inside(roi));
    
    out.resize(roi.width(),roi.height());
    
    Uint8 *pSrc = (Uint8*)(in.pointer()+in.width()*roi.y0+roi.x0);
    Uint8 *pDst = (Uint8*)(out.pointer());
    Uint skipLineSrc = 4*(in.width()-roi.width());
    Uint8 *pDstEnd = pDst + out.width()*out.height();
    
    typename Converter::Aux aux;
    
    while (pDst < pDstEnd)
    {
      int count = out.width();
      while (count-- > 0)
      {
        Converter::convert(pSrc[R_IDX],pSrc[G_IDX],pSrc[B_IDX],
                           *pDst, aux);
        pSrc += 4;
        pDst++;
      }
      pSrc += skipLineSrc;
    }
    
    PVASSERT(pDst == pDstEnd);
  }

};

/**
 * Fast color conversion algorithms. This will work for the whole image.
 * @see PvImageProc
 */
class ColorConvert
{
  //
  // LIFETIME
  //

public:

  //
  // OPERATIONS
  //

public:

  /**
   * General conversions.
   */
  static void convert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                      Image32& out, PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi = Rectanglei());

  /**
   * General conversions.
   */
  static void convert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                      Image8& out0, Image8& out1, Image8& out2, 
                      PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi = Rectanglei());

  /**
   * Convert from 3 channels to 1 channel.
   */
  static void convert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                      Image8& out, PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi = Rectanglei());

  /**
   * Convert from 1 channel to 3 channels.
   */
  static void convert(const Image8& in, PvImageProc::colorSpaceConst inColSpace,
                      Image32& out, PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi = Rectanglei());

private:

  /**
   * Conversions performed by ipp.
   */
  static void ippConvert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                         Image32& out, PvImageProc::colorSpaceConst outColSpace,
                         Rectanglei roi);


  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:


};

}; // namespace aitvml

#endif // ColorConvert_HPP

