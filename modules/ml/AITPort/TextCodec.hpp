/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AIT_TextCodec_HPP
#define AIT_TextCodec_HPP

// SYSTEM INCLUDES
//

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <zlib.h>

// AIT INCLUDES
//

#include <modules/ml/AITPort/String.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "modules/ml/AITPort/TextCodec_fwd.hpp"

namespace aitvml
{


/**
 * TextCodec Class
 * Encodes and decodes text streams/files
 */
class TextCodec
{

public:

  /**
   * Constructor.
   */
  TextCodec();

  /**
   * Destructor
   */
  virtual ~TextCodec();


  //
  // OPERATIONS
  //

  /**
   * Encodes a given string of data and writes it onto a file
   * 
   * fileName [in] is the name of the output file to be writen
   * data [in] is the data to be encoded
   *
   * returns void
   */
  static void encodeToMemory(const std::string &inData, std::vector<Uint8>& outData);
  static void encodeToFile(const std::string& fileName, const std::string& data);

  /**
   * Decodes the data in a given file and puts the result in a string
   * 
   * fileName [in] is the name of the input file
   * data [out] is the data to be encoded
   *
   * returns void
   */
  static void decodeFromMemory(std::vector<Uint8>& encodedString, std::string& stringData);
  static void decodeFromFile(const std::string& fileName, std::string& stringData);

  /**
   * Checks whether the given file is encoded or not
   * 
   * fileName [in] is the name of the file to be checked
   *
   * returns true if the file is encoded, false otherwise
   */
  static bool isEncoded(const std::string& fileName);

protected:

  /**
   * Encrypt the given buffer using simple XOR encryption
   * 
   * buffer is the address of the string to be encrypted
   *
   * returns void
   */
  static void encrypt(std::vector<unsigned char>& buffer);
  static void decrypt(std::vector<unsigned char>& buffer);

private:

  static std::string CRYPT_KEY;
};
}; // namespace aitvml

#endif // TextCodec_HPP
