/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IlluminationGradient_HPP
#define IlluminationGradient_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <modules/ml/AITPort/String.hpp>
#include <modules/ml/AITPort/Image.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "modules/ml/AITPort/IlluminationGradient_fwd.hpp"

namespace aitvml
{

namespace vision
{


/**
 * This class normalizes the lighting on an image.
 * @par Responsibilities:
 * - Calculates the lighting gradient and substracts it out of the image.
 * - Use a binary mask to calculate the gradient.
 */
class IlluminationGradient : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  IlluminationGradient();

  /**
   * Destructor
   */
  virtual ~IlluminationGradient();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this image.
   * @param dstImg [out] output of this method.
   * @param srcImg [in] Image to normalize.
   */
  void process(Image8& dstImg, const Image8& srcImg);

  //
  // ACCESS
  //

public:

  /**
   * Set the binary mask for this image. Only pixels that are different
   * from zero will be considered in the computation. The mask must be of the
   * same size of the image. If no mask is specified, the whole
   * image will be processed.
   * @param mask [in] Pointer to the mask.
   */
  void setMask(const Image8& mask);

  /**
   * Returns a reference to the mask.
   */
  const Image8& getMask() const { return mMask; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Pointer to the binary mask.
   */
  Image8 mMask;



};


}; // namespace vision

}; // namespace aitvml

#endif // IlluminationGradient_HPP

