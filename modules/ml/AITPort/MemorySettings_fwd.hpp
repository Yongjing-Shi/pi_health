/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AIT_MemorySettings_fwd_HPP
#define AIT_MemorySettings_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace aitvml
{

class MemorySettings;
typedef boost::shared_ptr<MemorySettings> MemorySettingsPtr;

}; // namespace aitvml

#endif // MemorySettings_fwd_HPP

