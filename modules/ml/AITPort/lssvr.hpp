
#ifndef lssvr_h
#define lssvr_h

#ifdef  __cplusplus
extern "C" {
#endif

typedef struct of_LsvFilter
{
  int xy0;
  int width, height;
  float *weight;

  struct of_LsvFilter *next;
}
LsvFilter;

typedef struct of_LsvVector
{
  float *alpha;
  float *val;

  struct of_LsvVector *next;
}
LsvVector;

typedef struct of_Lsv
{
  int img_width, img_height;
  int num_filters;
  int num_vectors;
  int num_outputs;
  int num_features;
  float *offset;
  float *sigma2;
  LsvFilter *filter;
  LsvVector *vector;
}
Lsv;

const char *lsv_err( void );
void lsv_reset_err( void );

Lsv *lsv_new( int img_width, int img_height, int num_outputs, int num_features );
void lsv_delete( Lsv *lsv );

void lsv_add_filter( Lsv *lsv,
                     int x0, int y0, int width, int height,
                     float *weight );
void lsv_add_vector( Lsv *lsv, int out_num, float alpha, float *val );
void lsv_add_allout_vector( Lsv *lsv, float *alpha, float *val );
void lsv_set_sigma( Lsv *lsv, int out_num, float sigma );
void lsv_set_offset( Lsv *lsv, int out_num, float offset );

void lsv_apply_filters( Lsv *lsv, float *val, unsigned char *img );
void lsv_apply_filters_at( Lsv *lsv, float *val, unsigned char *img, int width, int height, int startX, int startY );
void lsv_apply( Lsv *lsv, float *outputs, unsigned char *img );

void lsv_write( Lsv *lsv, const char *file );
Lsv *lsv_read( const char *file );

void gen_lsv_filter(int startx0, int starty0, float FL, int Nsample, int num_features, Lsv *lsv);

#ifdef __cplusplus
}
#endif

#endif
