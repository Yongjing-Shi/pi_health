#ifndef _IMAGE_HPP_
#define _IMAGE_HPP_

//#pragma once

#include <math.h>

#include <modules/ml/AITPort/Matrix.hpp>
#include <modules/ml/AITPort/PvUtil.hpp>
#include <modules/ml/AITPort/PvImageArcDefs.hpp>
#include <modules/ml/AITPort/Rectangle.hpp>

#define HUE(pixel) RED(pixel)
#define SATURATION(pixel) RED(pixel)
#define VALUE(pixel) RED(pixel)

namespace aitvml
{

// forward declaration of RGB image class (used by Image8)
class Image32;

enum
{
  RESCALE_SKIP=1,
  RESCALE_LINEAR,
  RESCALE_CUBIC,
  RESCALE_SUPER
};

/**
 * This class carries the general functionality of an image class.
 * It inherits the properties from Matrix. The main difference
 * is, that the elements of an image are accessed as (x,y) (which
 * in the matrix world means (col,row)).
 */
template<class T> class Image : public Matrix<T>
{
public:
  Image(void) : Matrix<T>() {};
  Image(uint w,uint h) : Matrix<T>(h,w) {};

  T& operator()(const uint x, const uint y) { return this->mat[y][x];};
  T operator()(const uint x, const uint y) const { return this->mat[y][x];};
  T& operator()(const uint pos) { return this->mat[0][pos];};
  T operator()(const uint pos) const { return this->mat[0][pos];};

  void safe_set(const uint x, const uint y, const T val) { Matrix<T>::safe_set(y,x,val); }

  inline uint width(void) const { return this->ncols; };
  inline uint height(void) const { return this->nrows; };

  void resize(const uint w,const uint h) 
    { BasicMatrix<T>::resize(h,w); };

  void rescale(const Image<T>& from, const uint w,const uint h,const uint mode=RESCALE_SKIP);
  void rescale(const uint w,const uint h,const uint mode=RESCALE_SKIP);

  void crop(const uint x,const uint y,const uint w,const uint h) 
     { BasicMatrix<T>::crop(y,x,y+h-1,x+w-1); };

  void crop(const BasicMatrix<T>& mx,const uint x,const uint y,const uint w,const uint h) 
     { BasicMatrix<T>::crop(mx,y,x,y+h-1,x+w-1); };

  void paste(const BasicMatrix<T>& mx, const int x, const int y);

  void circle(const int x, const int y,const float r,const T col);
  void ellipse(const int cx, const int cy, const double rx, const double ry, const T color);
  void ellipseFill(const int cx, const int cy, const double rx, const double ry, const T color);
  void hline(int x,const int y,int len,const T col);
  void vline(const int x,int y,int len,const T col);
  void line(int x0, int y0, int x1, int y1,const T col); 
  void rectangle(int x0, int y0,int x1, int y1,const T col);
  void rectangle(const Rectanglei r, const T col) { rectangle(r.x0,r.y0,r.x1,r.y1,col); }
  void rectFill(int x0, int y0,int x1, int y1,const T col);  
  void rectFill(const Rectanglei r, const T col) { rectFill(r.x0,r.y0,r.x1,r.y1,col); }

  void centerRotateRescale(const Image<T>& from,
                     const float rotation, const float saleFactor, 
					 const float xCenter, const float yCenter,
					 const uint newWidth, const uint newHeight);

#ifdef OLD_AIT_CODE
public:
  inline void set(const uint x,const uint y,T val) 
    { if(x<ncols && y<nrows) mat[y][x]=val; };
#endif

private:
  int clamp(const int a, const int b, const int c) { return (std::max)(b, (std::min)(a, c)); }
};

/**
 * Gray image class. The pixels are unsigned chars.
 */
class Image8 : public Image<unsigned char>
{
public:
  Image8(void) : Image<unsigned char>() {};
  Image8(uint w,uint h) : Image<unsigned char>(w,h) {};

  /// Adds a constant to every value. If result is greater than 255, then = 255.
  void AddConstantBrightness(unsigned char brightness);

  /// Subtracts a constant to every value. If result is less than  0, then = 0.
  void SubtractConstantBrightness(unsigned char brightness);

  int load(const char *name);
  int save(const char *name);
  void band(Image32 &rgbimg, char band); 

  void rescale(const Image8& from, const uint w,const uint h,const uint mode=RESCALE_SKIP);
  void rescale(const uint w,const uint h,const uint mode=RESCALE_SKIP) 
  {
    if (w == width() && h == height()) return;
    rescale(Image8(*this),w,h,mode);
  }

  void centerRotateRescaleShift(const Image8& from,
                     const float rotation, const float saleFactor,
					 const float xCenter, const float yCenter,
					 const float xShift, const float yShift,
					 const uint newWidth, const uint newHeight);

  void Lequalize(Image8 from, int radius);
};

/**
 * Color (RGB) image class. The pixels are unsigned integers.
 */
class Image32 : public Image<unsigned int>
{
public:
  enum colorConst { PV_RGBA, PV_BGRA };

  Image32(void) : Image<unsigned int>(), alpha(false), internalFormat(PV_BGRA) {};
  Image32(uint w,uint h) : Image<unsigned int>(w,h), alpha(false), internalFormat(PV_BGRA) {};

  int load(const char *name);
  int save(const char *name);

  int loadpng(const char *name);
  int savepng(const char *name);

  int loadjpg(const char *name);
  int savejpg(const char *name, const int quality=90);

  bool getAlphaFlag() const { return alpha; }
  void setAlphaFlag(const bool val) { alpha = val; }

  int getInternalFormat() const { return internalFormat; }
  void setInternalFormat(int dstFormat) { internalFormat = dstFormat; }

  void swapChannels(const Image32& src, Rectanglef& subRegion);
	void swapChannels(const Image32& src);
  void swapChannels() { swapChannels(*this); }

#ifdef OLD_AIT_CODE
  unsigned char& r(const uint x, const uint y) { return *((unsigned char *)(&mat[y][x])+R_IDX);};
  unsigned char r(const uint x, const uint y) const { return *((unsigned char *)(&mat[y][x])+R_IDX);};
  unsigned char& g(const uint x, const uint y) { return *((unsigned char *)(&mat[y][x])+G_IDX);};
  unsigned char g(const uint x, const uint y) const { return *((unsigned char *)(&mat[y][x])+G_IDX);};
  unsigned char& b(const uint x, const uint y) { return *((unsigned char *)(&mat[y][x])+B_IDX);};
  unsigned char b(const uint x, const uint y) const { return *((unsigned char *)(&mat[y][x])+B_IDX);};  
  unsigned char& a(const uint x, const uint y) { return *((unsigned char *)(&mat[y][x])+A_IDX);};
  unsigned char a(const uint x, const uint y) const { return *((unsigned char *)(&mat[y][x])+A_IDX);};

  unsigned char& r(const uint pos) { return *((unsigned char *)(&mat[0][pos])+R_IDX);};
  unsigned char r(const uint pos) const { return *((unsigned char *)(&mat[0][pos])+R_IDX);};
  unsigned char& g(const uint pos) { return *((unsigned char *)(&mat[0][pos])+G_IDX);};
  unsigned char g(const uint pos) const { return *((unsigned char *)(&mat[0][pos])+G_IDX);};
  unsigned char& b(const uint pos) { return *((unsigned char *)(&mat[0][pos])+B_IDX);};
  unsigned char b(const uint pos) const { return *((unsigned char *)(&mat[0][pos])+B_IDX);};  
  unsigned char& a(const uint pos) { return *((unsigned char *)(&mat[0][pos])+A_IDX);};
  unsigned char a(const uint pos) const { return *((unsigned char *)(&mat[0][pos])+A_IDX);};

  void set(const uint pos, const unsigned char r, const unsigned char g, const unsigned char b)
  {
    mat[0][pos]=PV_RGB(r,g,b);
  }
	void set(const uint x, const uint y, const unsigned char r, const unsigned char g, const unsigned char b)
  {
		mat[y][x]=PV_RGB(r,g,b);
  }
#endif

  void rescale(const Image32& from, const uint w,const uint h,const uint mode=RESCALE_SKIP);
  void rescale(const uint w,const uint h,const uint mode=RESCALE_SKIP) { rescale(Image32(*this),w,h,mode); }

  void centerRotateRescaleShift(const Image32& from,
                     const float rotation, const float saleFactor,
					 const float xCenter, const float yCenter,
					 const float xShift, const float yShift,
					 const uint newWidth, const uint newHeight);
  
  void rotate(const Image32& from, const double angle, const unsigned int bkColor, const uint mode=RESCALE_SKIP);

protected:
  bool alpha;
  int internalFormat;   ///< internal ordering of RGB bytes in memory

};

/*********************************************************************
 ** 
 ** Method implementations for Image<T>
 **
 *********************************************************************/

/**
 * Resample the image to a new size.
 * @param from [in] Input image to resample.
 * @param width,height [in] new size of image
 * @param mode [in] rescaling mode. Possible values are:
 * - RESCALE_SKIP: Perform nearest neighbor rescale.
 * - RESCALE_LINEAR: Perform linear interpolation.
 * - RESCALE_CUBIC: Use cubic interpolation (IPP only).
 * - RESCALE_SUPER: Use supersampling interpolation (IPP only).
 */
template<class T> void Image<T>::rescale(const Image<T>& from,
                                           const uint w,const uint h,
                                           const uint m)
{
  PVASSERT(&from != this);

  if (w==from.width() && h==from.height()) 
  {
    (*this) = from;
    return;
  }
  
  uint x,y;
  uint wo=from.width()-1,ho=from.height()-1;
  T **mxx;
  
  resize(w,h);

  mxx=this->pointerpointer();
  
  switch(m)
  {
  case RESCALE_SKIP:
    if (w <= 1 || h <= 1)
    {
      (*this) = from;
      return;
    }
    for(y=0;y<h;y++)
    {
      for(x=0;x<w;x++)
      {
        (*this)(x,y)=from((x*wo)/(w-1),(y*ho)/(h-1));
      }
    } 
    break;
  case RESCALE_LINEAR:
    {
      const int newWidth= w;
      const int newHeight= h;
      const float scaleX = (float)width()/newWidth;
      const float scaleY = (float)height()/newHeight;

      // First scale in X
      Image<T> tmpImg(newWidth, from.height());
      int x, y;
      for (y=0; y<from.height(); y++) {
        float fPos = 0.0;
        for (x=0; x<newWidth; x++) {
          int pos = (std::min)(from.width()-2, static_cast<unsigned int>(fPos));
          double frac = fPos-pos;
          tmpImg(x,y) = clamp(static_cast<T>(0.5+(1.0-frac)*(from(pos,y))+frac*(from(pos+1,y))), 0, 255);
          fPos += scaleX;
        }
      }

      // Then scale in Y
      for (x=0; x<newWidth; x++) {
        float fPos = 0.0;
        for (y=0; y<newHeight; y++) {
          int pos = (std::min)(from.height()-2, static_cast<unsigned int>(fPos));
          double frac = fPos-pos;
          (*this)(x,y) = clamp(static_cast<T>(0.5+(1.0-frac)*tmpImg(x,pos)+frac*tmpImg(x,pos+1)), 0, 255);
          fPos += scaleY;
        }
      }
    }
    break;
  default: 
    PvUtil::exitError("Resample mode not available.");
    break;
  }  
}

/**
 * Resample the image to a new size.
 * @param width,height [in] new size of image
 * @param mode [in] rescaling mode. Possible values are:
 * - RESCALE_SKIP: Perform nearest neighbor rescale.
 * - RESCALE_LINEAR: Perform linear interpolation.
 * - RESCALE_CUBIC: Use cubic interpolation (IPP only).
 * - RESCALE_SUPER: Use supersampling interpolation (IPP only).
 */
template<class T> void 
Image<T>::rescale(const uint w,const uint h,
                    const uint m)
{
  if (w == width() && h == height()) return;
  rescale(Image<T>(*this),w,h,m);
}

/**
 * Pastes a given image (passed as the first arg.) into 'this' image
 * at the supplied coordinates.
 * The function takes care of not writing into invalid memory.
 * The coordinates can be negative.
 * @param mx [in] image (or basic matrix) to be pasted
 * @param x,y [in] coordinates at which to pase the image
 */
template<class T> void Image<T>::paste(const BasicMatrix<T>& mx, const int x, const int y)
{
  // Calculate in destination rect coordinates.
  const Rectanglei src(x,y,x+mx.cols(),y+mx.rows());
  const Rectanglei dst(0,0,width(),height());
  Rectanglei inter;

  if (!inter.intersect(src,dst))
  {
    return;
  }

  int xp,yp;

  for(yp=inter.y0;yp<inter.y1;yp++)
  {
    for(xp=inter.x0;xp<inter.x1;xp++)
    {
      (*this)(xp,yp) = mx(yp-y,xp-x);
    }
  }
}

/**
 * Draws a circle with radius r and color val
 * @param x,y [in] centre of circle
 * @param r [in] radius
 * @param val [in] color
 */
template<class T> void Image<T>::circle(const int x, 
					  const int y,
					  const float r,const T val)
{
  ellipse(x,y,r,r,val);
}

/**
 * Draws an ellipse.
 * @param cx,cy [in] centre of ellipse
 * @param rx,ry [in] radius for each axis.
 * @param val [in] color
 */
template<class T> void Image<T>::ellipse(const int cx, 
                                           const int cy, 
                                           const double rx, 
                                           const double ry, 
                                           const T color)
{
  // Midpoint Ellipse algorithm
  
  int x=0, y = (int)ry;
  double rx2 = (double)rx*rx, ry2 = (double)ry*ry; // These must be double to avoid overflow
  double p = ry2 - rx2*ry + 0.25*(double)rx2;

  // 1st Region

  safe_set(cx-x,cy+y,color);
  safe_set(cx+x,cy+y,color);
  safe_set(cx-x,cy-y,color);
  safe_set(cx+x,cy-y,color);

  while (ry2*x < rx2*y) {
    x++;
    if (p < 0) { 
      p += 2*ry2*x + ry2;
    } else {
      y--;
      p += 2*ry2*x - 2*rx2*y + ry2;
    }

    safe_set(cx-x,cy+y,color);
    safe_set(cx+x,cy+y,color);
    safe_set(cx-x,cy-y,color);
    safe_set(cx+x,cy-y,color);
  }

  // 2nd Region

  double x_plus_half = (double)x+0.5;
  int y_minus_1 = y-1;
  p = x_plus_half*x_plus_half*ry2 + rx2*y_minus_1*y_minus_1 - rx2*ry2;

  while (y > 0) {
    y--;
    if (p > 0) {
      p += -2*rx2*y + rx2;
    } else {
      x++;
      p += 2*ry2*x - 2*rx2*y + rx2;
    }

    safe_set(cx-x,cy+y,color);
    safe_set(cx+x,cy+y,color);
    safe_set(cx-x,cy-y,color);
    safe_set(cx+x,cy-y,color);
  }
}

/**
 * Draws a filled ellipse.
 * @param cx,cy [in] centre of ellipse
 * @param rx,ry [in] radius for each axis.
 * @param val [in] color
 */
template<class T> void Image<T>::ellipseFill(const int cx, 
                                               const int cy, 
                                               const double rx, 
                                               const double ry, 
                                               const T color)
{
  // Midpoint Ellipse algorithm
  
  int x=0, y = (int)ry;
  double rx2 = (double)rx*rx, ry2 = (double)ry*ry; // These must be double to avoid overflow
  double p = ry2 - rx2*ry + 0.25*(double)rx2;

  // 1st Region

  hline(cx-x,cy+y,2*x,color);
  hline(cx-x,cy-y,2*x,color);

  while (ry2*x < rx2*y) {
    x++;
    if (p < 0) { 
      p += 2*ry2*x + ry2;
    } else {
      y--;
      p += 2*ry2*x - 2*rx2*y + ry2;
    }

    hline(cx-x,cy+y,2*x,color);
    hline(cx-x,cy-y,2*x,color);

  }

  // 2nd Region

  double x_plus_half = (double)x+0.5;
  int y_minus_1 = y-1;
  p = x_plus_half*x_plus_half*ry2 + rx2*y_minus_1*y_minus_1 - rx2*ry2;

  while (y > 0) {
    y--;
    if (p > 0) {
      p += -2*rx2*y + rx2;
    } else {
      x++;
      p += 2*ry2*x - 2*rx2*y + rx2;
    }

    hline(cx-x,cy+y,2*x,color);
    hline(cx-x,cy-y,2*x,color);
  }
}

/**
 * Draws a horizontal line.
 * @param x,y [in] beginning of line
 * @param len [in] length of line 
 * @param col [in] color
 */
template<class T> void Image<T>::hline(int x,
										 const int y,
										 int len,const T col)
{   
  T *data; 

  if(y<0 || y>=(int)height()) return;
  int x0 = (std::max)(x,0);
  int x1 = (std::min)((int)width(),x+len);
  len = x1-x0;

  if(len<=0) return;

  data=this->pointer()+x0+y*width();
  while(len!=0)
  {
    *(data++)=col;
    len--;
  }
}

/**
 * Draws a vertical line.
 * @param x,y [in] beginning of line
 * @param len [in] length of line 
 * @param col [in] color
 */
template<class T> void Image<T>::vline(const int x, int y,
										 int len,const T col)
{
  T *data; 

  if(x<0 || x>=(int)width()) return;
  int y0 = (std::max)(y,0);
  int y1 = (std::min)((int)height(),y+len);
  len = y1-y0;

  if(len<=0) return;

  data=this->pointer()+x+y0*width();
  while(len!=0)
  {
    *(data)=col;
    data+=width();
    len--;
  }
}

/**
 * Draws a line.
 * @param x1,y1,x2,y2 [in] beginning/end of line
 * @param col [in] color
 */
template<class T> void Image<T>::line(int x1, int y1,
										int x2, int y2,const T col)
{
  int w=width(),h=height();
  int x,y,dx,adx,dy,ady;
  T *data=this->pointer();

  dx=x2-x1;adx=dx<0 ? -dx : dx;
  dy=y2-y1;ady=dy<0 ? -dy : dy;

  if ((dx == 0) && (dy == 0)) 
  {
    if(x1>=0 && y1>=0 && x1<w && y1<h) data[x1+y1*w]=col;
    return;
  }
  
  if((adx>ady && dx<0) || (adx<=ady && dy<0)) 
  { 
    x=x2;x2=x1;x1=x;
    y=y2;y2=y1;y1=y;
    dx=-dx;
    dy=-dy;
  }
  
  if(dx>dy)
  {
    for(x=0;x<=dx;x++)
    {
      y=y1+dy*x/dx;
      if((x+x1)>=0 && y>=0 && (x+x1)<w && y<h) data[(x+x1)+y*w]=col;
    }
  }
  else
  {
    for(y=0;y<=dy;y++)
    {
      x=x1+dx*y/dy;
      if(x>=0 && (y+y1)>=0 && x<w && (y+y1)<h) data[x+(y+y1)*w]=col;
    }
  }
}

/**
 * Draws a rectangle.
 * @param x1,y1,x2,y2 [in] Top/left bottom/right coordinates
 * @param col [in] color
 */
template<class T> void Image<T>::rectangle(int x1, int y1,int x2, int y2,T col)
{
  hline(x1,y1,x2-x1,col);
  hline(x1,y2,x2-x1,col);
  vline(x1,y1,y2-y1,col);
  vline(x2,y1,y2-y1,col);
}

/**
 * Draws a filled rectange. The last coordinate is not included inside the rectangle.
 * @param x1,y1,x2,y2 [in] coordinates of rectangle corners
 * @param col [in] color
 */
template<class T> void Image<T>::rectFill(int x1, int y1,int x2, int y2,T col)
{
  this->setAll(col,
    (std::max)(0,y1),
    (std::max)(0,x1),
    (std::min)((int)height()-1,y2-1),
    (std::min)((int)width()-1,x2-1)
    );
}

/**
 * A version of centerRotateRescale(..) with zero shifts
 */
template<class T> 
void Image<T>::centerRotateRescale(const Image<T>& from,
                     const float rotation, const float scaleFactor, 
					 const float xCenter, const float yCenter,
					 const uint frameWidth, const uint frameHeight)
{
	centerRotateRescaleShift(from, rotation, scaleFactor, xCenter, yCenter, 0.0, 0.0, frameWidth, frameHeight);	
}

} // namespace aitvml


#endif  // _IMAGE_HPP_
