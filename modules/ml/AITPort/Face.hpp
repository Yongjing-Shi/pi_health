/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef Face_HPP
#define Face_HPP

namespace aitvml
{

class Face
{
public:
  
  //Constructor.
  Face();
  
  //Destructor
  virtual ~Face();
  
  // initialization
  void initialize();
  
  // bounding box of face
  int x,y,w,h;

  
protected:
  
private:
  
};

} // namespace aitvml

#endif


