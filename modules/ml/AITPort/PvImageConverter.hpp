/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVIMAGECONVERTER_HPP
#define PVIMAGECONVERTER_HPP
#include "modules/ml/AITPort/Image.hpp"

namespace aitvml
{

class PvImageConverter
{
public:
  static void convert(const aitvml::Image32 &in, aitvml::Image8 &out);
  static void convert(const aitvml::Image8 &in, aitvml::Image32 &out);
  static void convert(const aitvml::Image8 &in, aitvml::Image<int> &out);
  static void convertBinary(const aitvml::Image8 &in, aitvml::Image32 &out);
};

}

#endif


