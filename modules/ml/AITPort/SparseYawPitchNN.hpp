#ifndef SPARSEYAWPITCHNN_HPP
#define SPARSEYAWPITCHNN_HPP

#include "modules/ml/AITPort/ModelPerson.hpp"

#include "DiskMatDataSet.h"
#include "ClassFormatDataSet.h"
#include "ClassNLLCriterion.h"
#include "MSECriterion.h"
#include "OneHotClassFormat.h"
#include "ClassMeasurer.h"
#include "MSEMeasurer.h"

#include "StochasticGradient.h"
#include "KFold.h"

#include "ConnectedMachine.h"
#include "Linear.h"
#include "Tanh.h"
#include "LogSoftMax.h"

#include "MeanVarNorm.h"
#include "DiskXFile.h"
#include "CmdLine.h"
#include "Random.h"

#include "modules/ml/AITPort/lssvr.hpp"

namespace aitvml
{

class SparseYawPitchNN
{

public:

  typedef struct _FileParams
  {
    char *valid_file;
    char *file;

    int max_load;
    int max_load_valid;
    real accuracy;
    real learning_rate;
    real decay;
    int max_iter;
    int the_seed;

    char *dir_name;
    char *model_file;
    int k_fold;
    bool binary_mode;
    bool regression_mode;
    int class_against_the_others;
  } FileParams;

  FileParams mFileParams;

  Torch::SafeRandom mRandom;
  
  Torch::CmdLine mCmd;

  std::string mModelListFile, mModelDir, mLsvFile;
  int imgWidth, imgHeight;
  int numInputs;
  int numTargets;
  int numHiddenUnits1, numHiddenUnits2, numHiddenUnits3;
  real weight_decay;

  Lsv *mLsv;
//  Torch::ConnectedMachine mMlp[25];
  std::vector< Torch::ConnectedMachine * > mMlp;
  std::vector< Torch::MeanVarNorm *> mMvNorm;
//  float mModelYaw[25], mModelPitch[25];
  std::vector< float > mModelYaw;
  std::vector< float > mModelPitch;
//  int numInputTmp[25];
  int numModel;
  int numCloseModel;

  float mYaw, mPitch;
  FILE *fp;
  float mEstYaw, mEstPitch;
  
  // Constructor
  SparseYawPitchNN()
  {
  }

  // Destructor
  virtual ~SparseYawPitchNN()
  {
    finish();
  }
  
  void init(std::string modelListFile_, std::string modelDir_, int imgWidth_, int imgHeight_);
  void init(std::string modelListFile_, std::string modelDir_, std::string lsvFile_, int imgWidth_, int imgHeight_);

  void finish()
  {
    for (int i = 0; i < mMlp.size(); i++)
    {
      delete mMlp[i];
    }
    mMlp.clear();
    for (int i = 0; i < mMvNorm.size(); i++)
    {
      delete mMvNorm[i];
    }
    mMvNorm.clear();
    mModelYaw.clear();
    mModelPitch.clear();
  }

  void get_args();
  void estimateYawPitch(int numEstModel, Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);
  void affine(float *vecX, float *vecY, float scale, aitvml::Matrix<float> rotMat, aitvml::Matrix<float> rotCenter, aitvml::Matrix<float> trans);
};

typedef boost::shared_ptr<SparseYawPitchNN> SparseYawPitchNNPtr;

//void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);

} // namespace aitvml

#endif
