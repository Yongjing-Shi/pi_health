/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef __LINUXWINDOWSCONVERSION_HPP__
#define __LINUXWINDOWSCONVERSION_HPP__

/**
 *
 **/
#ifdef WIN32

#define strcasecmp(s1, s2) _stricmp(s1, s2)
#define strncasecmp(s1, s2, n) _strnicmp(s1, s2, n)
#define vsnprintf(str, size, format, ap) _vsnprintf(str, size, format, ap)
#define pthread_self() GetCurrentThreadId()

typedef struct _stat StatStruct;

//#define stat(f,s) _stat(f,s)

#else

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#ifndef FAR
#define FAR
#endif

#ifndef WINAPI
//#define WINAPI
#endif

#ifndef INVALID_HANDLE_VALUE
#define INVALID_HANDLE_VALUE -1
#endif

#ifndef E_FAIL
#define E_FAIL -1
#endif

#ifndef S_OK
#define S_OK 0
#endif

#ifndef HANDLE
typedef void* HANDLE;
#endif

#ifndef DWORD
typedef unsigned int DWORD;
#endif

#ifndef LRESULT
typedef unsigned int LRESULT;
#endif

#ifndef UINT
typedef unsigned int UINT;
#endif

#ifndef LPARAM
typedef unsigned int LPARAM;
#endif

#ifndef WPARAM
typedef unsigned int WPARAM;
#endif

#ifndef LPVOID
typedef void* LPVOID;
#endif

#ifndef LPCSTR
typedef const char* LPCSTR;
#endif

#ifndef LPCTSTR
typedef const char* LPCTSTR;
#endif

#ifndef TCHAR
typedef char TCHAR;
#endif

#ifndef LONG
typedef long LONG;
#endif

#ifndef HRESULT
typedef LONG HRESULT;
#endif

#define GetCurrentThreadId() pthread_self()
#define GetCurrentThread() pthread_self()
#define _stricmp(s1, s2) strcasecmp(s1, s2)
#define stricmp(s1, s2) strcasecmp(s1, s2)
#define _strnicmp(s1, s2, n) strncasecmp(s1, s2, n)
#define strnicmp(s1, s2, n) strncasecmp(s1, s2, n)
#define _vsnprintf(str, size, format, ap) vsnprintf(str, size, format, ap)

#define _strdate(buf)\
{\
        time_t __tsecs__;\
        struct tm* __mytm__;\
        ::time(&__tsecs__);\
        __mytm__ = localtime(&__tsecs__);\
        sprintf((buf), "%0d/%0d/%d", __mytm__->tm_mon, (__mytm__->tm_mday+1), ((__mytm__->tm_year > 99) ? (__mytm__->tm_year-100) : __mytm__->tm_year));\
}

#define _strtime(buf)\
{\
        time_t __tsecs__;\
        struct tm* __mytm__;\
        ::time(&__tsecs__);\
        __mytm__ = localtime(&__tsecs__);\
        sprintf((buf), "%0d:%0d:%d", __mytm__->tm_hour, __mytm__->tm_min, __mytm__->tm_sec);\
}

#ifndef itoa
#define itoa(i,s,b) sprintf((s), "%d", (i))
#endif

#define Sleep(s) sleep(s)
#define CreateDirectory(p, m) mkdir(p, (unsigned int)m)
#define WSAGetLastError() errno
#define GetLastError() errno

#define ExitProcess(v) _exit(v)

#define _stat(f,s) stat(f,s)

typedef struct stat StatStruct;

typedef pthread_mutex_t CRITICAL_SECTION;

#define InitializeCriticalSection(m) pthread_mutex_init(m, NULL)
#define DeleteCriticalSection(m) pthread_mutex_destroy(m)
#define EnterCriticalSection(m) pthread_mutex_lock(m)
#define LeaveCriticalSection(m) pthread_mutex_unlock(m)

#ifndef COLORREF
typedef DWORD COLORREF;
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef RGB
#define RGB(r,g,b) ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))
#endif

#endif

#endif
