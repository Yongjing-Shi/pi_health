/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceDetectorAdaBoost.hpp
 *
 *  Created on: Jan 12, 2016
 *      Author: dkim
 */

#ifndef FaceDetectorAdaBoost_HPP
#define FaceDetectorAdaBoost_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

#include "modules/ml/AITPort/String.hpp"
#include "modules/ml/AITPort/FaceDetector.hpp"
#include "modules/ml/AITPort/FaceDetectorAdaBoost_fwd.hpp"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <iostream>
#include <stdio.h>

namespace aitvml
{

namespace vision
{


/**
 * Implements AdaBoost face detector. Uses OpenCV implementation.
 * @par Responsibilities:
 * - Implements AdaBoost face detector. Uses OpenCV implementation
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class FaceDetectorAdaBoost : public FaceDetector
{
  //
  // LIFETIME
  //

public:

  FaceDetectorAdaBoost();
  virtual ~FaceDetectorAdaBoost();

  /**
   * Initialization.
   */
  void init(const std::string modelName);
  void init(const std::string modelName, float scaleF, int minNbr, int minFaceSize, int maxFaceSize);


public:

  /**
   * Perform the actual detection.
   */
  void detect(const Image8 &image, bool asynch = true);
  void detect(const cv::Mat imgMat, bool async = true);

public:

  void setNumNeighbors(const int n) { mNumNeighbors = n; }


protected:

  cv::CascadeClassifier	mpCascade;

  bool mIsInitialized;

  /**
   * Number of faces that must be in the neighborhood to tell that
   * there is a face in a particular location. This is a parameter
   * for cvHaarDetectObjects().
   */

  int mNumNeighbors;
  double scaleFactor;
  int min_nbr;
  int minsize;
  int maxsize;


};


}; // namespace vision

}; // namespace aitvml

#endif // FaceDetectorAdaBoost_HPP

