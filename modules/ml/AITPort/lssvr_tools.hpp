
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

static char g_err[ 4096 ];

#define NEW( v )                                                \
{                                                               \
  if( (v) != 0 )                                                \
  {                                                             \
    sprintf( g_err, "Bug -- trying to allocate " #v ","         \
                    " but the pointer is not NULL" );           \
    goto err;                                                   \
  }                                                             \
  (v) = malloc( sizeof( *(v) ) );                               \
  if( (v) == 0 )                                                \
  {                                                             \
    sprintf( g_err, "Cannot allocate %d bytes for " #v,         \
                    sizeof( *(v) ) );                           \
    goto err;                                                   \
  }                                                             \
  memset( (v), 0, sizeof( *(v) ) );                             \
}

#define NEW_ARRAY( v, n )                                       \
{                                                               \
  if( (v) != 0 )                                                \
  {                                                             \
    sprintf( g_err, "Bug -- trying to allocate " #v ","         \
                    " but the pointer is not NULL" );           \
    goto err;                                                   \
  }                                                             \
  (v) = malloc( (n) * sizeof( *(v) ) );                         \
  if( (v) == 0 )                                                \
  {                                                             \
    sprintf( g_err, "Cannot allocate %d * %d bytes"             \
                    " for " #v,                                 \
                    (n), sizeof( *(v) ) );                      \
    goto err;                                                   \
  }                                                             \
  memset( (v), 0, (n) * sizeof( *(v) ) );                       \
}

#define DELETE( v )                                             \
{                                                               \
  if( (v) != 0 )                                                \
    free( v );                                                  \
  (v) = 0;                                                      \
}

#define OPEN( fp, name )                                        \
{                                                               \
  if( (fp) != 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to open '%s' for reading,"   \
                    " but the file pointer is not 0",           \
                    name );                                     \
    goto err;                                                   \
  }                                                             \
  (fp) = fopen( name, "rb" );                                   \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Cannot open '%s' for reading -- %s",       \
                    name, strerror( errno ) );                  \
    goto err;                                                   \
  }                                                             \
}

#define CREATE( fp, name )                                      \
{                                                               \
  if( (fp) != 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to create '%s',"             \
                    " but the file pointer is not 0",           \
                    name );                                     \
    goto err;                                                   \
  }                                                             \
  (fp) = fopen( name, "wb" );                                   \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Cannot create '%s' -- %s",                 \
                    name, strerror( errno ) );                  \
  }                                                             \
}

#define MODIFY( fp, name )                                      \
{                                                               \
  if( (fp) != 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to open '%s' for modifying," \
                    " but the file pointer is not 0",           \
                    name );                                     \
    goto err;                                                   \
  }                                                             \
  (fp) = fopen( name, "r+b" );                                  \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Cannot open '%s' for modifying -- %s",     \
                    name, strerror( errno ) );                  \
    goto err;                                                   \
  }                                                             \
}

#define READ( fp, v )                                           \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to read from a null"         \
                    "file pointer" );                           \
    goto err;                                                   \
  }                                                             \
  if( fread( (v), sizeof( *(v) ), 1, (fp) ) != 1 )              \
  {                                                             \
    sprintf( g_err, "Failed to read one item from a file" );    \
    goto err;                                                   \
  }                                                             \
}

#define READ_ARRAY( fp, v, n )                                  \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to read from a null"         \
                    "file pointer" );                           \
    goto err;                                                   \
  }                                                             \
  if( fread( (v), sizeof( *(v) ), (n), (fp) ) != (n) )          \
  {                                                             \
    sprintf( g_err, "Failed to read %d items from a file",      \
                    (n) );                                      \
    goto err;                                                   \
  }                                                             \
}

#define WRITE( fp, v )                                          \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to write to a null"          \
                    "file pointer" );                           \
    goto err;                                                   \
  }                                                             \
  if( fwrite( (v), sizeof( *(v) ), 1, (fp) ) != 1 )             \
  {                                                             \
    sprintf( g_err, "Failed to write one item to a file" );     \
    goto err;                                                   \
  }                                                             \
}

#define WRITE_ARRAY( fp, v, n )                                 \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to write to a null"          \
                    "file pointer" );                           \
    goto err;                                                   \
  }                                                             \
  if( fwrite( (v), sizeof( *(v) ), (n), (fp) ) != (n) )         \
  {                                                             \
    sprintf( g_err, "Failed to write %d items to a file",       \
                    (n) );                                      \
    goto err;                                                   \
  }                                                             \
}

#define SEEK( fp, loc )                                         \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to seek on a null"           \
                    "file pointer" );                           \
    goto err;                                                   \
  }                                                             \
  if( fseek( (fp), (loc), SEEK_SET ) != 0 )                     \
  {                                                             \
    sprintf( g_err, "Seek to location %d failed", (loc) );      \
    goto err;                                                   \
  }                                                             \
}

#define SEEK_TO_END( fp )                                       \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    sprintf( g_err, "Bug -- trying to seek on a null"           \
                    "file pointer" );                           \
    goto err;                                                   \
  }                                                             \
  if( fseek( (fp), 0L, SEEK_END ) != 0 )                        \
  {                                                             \
    sprintf( g_err, "Seek to end of file failed" );             \
    goto err;                                                   \
  }                                                             \
}

#define CLOSE( fp )                                             \
{                                                               \
  if( (fp) != 0 )                                               \
    fclose( (fp) );                                             \
  (fp) = 0;                                                     \
}
