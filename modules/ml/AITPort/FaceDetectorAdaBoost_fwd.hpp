/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * Recognition.hpp
 *
 *  Created on: Jan 12, 2016
 *      Author: dkim
 */

#ifndef FaceDetectorAdaBoost_fwd_HPP
#define FaceDetectorAdaBoost_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace aitvml
{

namespace vision
{

class FaceDetectorAdaBoost;
typedef boost::shared_ptr<FaceDetectorAdaBoost> FaceDetectorAdaBoostPtr;

class FaceDetectorAdaBoostForeground;
typedef boost::shared_ptr<FaceDetectorAdaBoostForeground> FaceDetectorAdaBoostForegroundPtr;

}; // namespace vision

}; // namespace aitvml

#endif // FaceDetectorAdaBoost_fwd_HPP

