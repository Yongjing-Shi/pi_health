/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SkinToneMotionDetector_HPP
#define SkinToneMotionDetector_HPP

// SYSTEM INCLUDES
//
#include <boost/utility.hpp>
#include <string>
#include <vector>

// AIT INCLUDES
//

#include <modules/ml/AITPort/Image.hpp>
#include <modules/ml/AITPort/PvImageConverter.hpp>
#include "modules/ml/AITPort/SkinToneDetector.hpp"


// LOCAL INCLUDES
//

//#include "SkinToneMotionDetector_fwd.hpp"

// FORWARD REFERENCES
//

//#include "SkinToneMotionDetector_fwd.hpp"

namespace aitvml
{

namespace vision
{


/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class SkinToneMotionDetector
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  SkinToneMotionDetector();

  /**
   * Destructor
   */
  virtual ~SkinToneMotionDetector();

public:

protected:

  //
  // ACCESS
  //

//	Image32 mMotionImage;

public:

	void detectSkinToneMotion(Image32* curImage, Image32* prevImage, Image8* skinToneImage, Image8* motionImage);

};


}; // namespace vision

}; // namespace aitvml

#endif // SkinToneMotionDetector_HPP


