/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <math.h>
#include <list>
#include <assert.h>


namespace aitvml
{

//  Useful math functions.


/*************************************************************************************/
//Classes for various types of exceptions that may arise.
/*************************************************************************************/


/*************************************************************************************/
//class dotProductIncompatible
/*************************************************************************************/
//Reason : If two vectors are incompatible for dot product.
//The members of this class are the sizes of the two vectors.
class dotProductIncompatible
{
  int sizeOfVector1;
  int sizeOfVector2;

  
public:
  //Constructors
  dotProductIncompatible()  //default constructor
  {
    sizeOfVector1 = -1;
    sizeOfVector2 = -1;
  }
  
  dotProductIncompatible(int sv1, int sv2)
  {
    sizeOfVector1 = sv1;
    sizeOfVector1 = sv2;
  }
  
};

//Function : dotProduct
//Purpose  : dot product of two vectors
//Parameters :
//             v1 : First vector
//             v2 : Second vector
//Note       : T1 and T2 are any of the basic numerical types like int,float,double etc.
//The version below will work in all probability.The only problem could be a slicing problem.
template <class T1,class T2>
float dotProduct(const T1* v1,const T2* v2,int dimension)
{
  float result = 0.0f;  //How to do this
  for(int i = 0; i < dimension; i++)
    result += v1[i]*v2[i];
  
  return result;
}

/*************************************************************************************/
//Function   : norm
//Purpose    : p norm of a vector
//Parameters :
//            v1        : Vector whose norm is desired
//            dimension : Dimension of the vector(no. of components)
//            p         : the order of the norm (defaulted to 2 i.e. Euclidean norm)
//The version below will surely work. Only problem is that it assumes a type(int) for parameter 1.
/*************************************************************************************/
/*float norm(int* v1, int dimension, int p = 2)
{
  float result =  0.0;
  for(int i = 0;i < dimension;i++)
    result = exp(p * log(abs(v1[i])));   // |v1(i)|^p
  
  result = exp((1.0/(float)p) * log(result));
  return result;
}*/

//Function   : norm
//Purpose    : p norm of a vector
//Parameters :
//            v1        : Vector whose norm is desired
//            dimension : Dimension of the vector(no. of components)
//            p         : the order of the norm (defaulted to 2 i.e. Euclidean norm)
//I expect this to work.
template <class T>
float norm(T* v1, int dimension, int p = 2)
{
  
  float result =  0.0;
  for(int i = 0;i < dimension;i++)
    result += exp(p * log(abs(v1[i])));   // |v1(i)|^p
  
  result = exp((1.0/(float)p) * log(result));
  
  return result;
}

/*************************************************************************************/
//Function   : EuclideanNorm
//Purpose    : Euclidean norm of a vector
//Parameters :
//            v1        : Vector whose norm is desired
//            dimension : Dimension of the vector(no. of components)
/*************************************************************************************/
template <class T>
float EuclideanNorm(T* v1, int dimension)
{
  float result = 0.0f;
  for(int i = 0;i < dimension; i++)
    result += v1[i] * v1[i];
  
  return (sqrt(result));
}

/*************************************************************************************/
//Function : vectorAddition
//Purpose  : Addition of 2 vectors
/*************************************************************************************/
template <class T>
T* vectorAddition(T* v1, T* v2,int dimension)
{
  T* result = new T[dimension];
  for(int i = 0;i < dimension;i++)
    result[i] = v1[i] + v2[i];
  
  return result;
}

/*************************************************************************************/
//Function : vectorDifference
//Purpose  : Addition of 2 vectors
//Parameters : v1 - First vector; v2 - Second vector; dimension - Dimension of the vectors
//Returns    : v1 - v2.
/*************************************************************************************/
template <class T>
T* vectorDifference(T* v1, T* v2,int dimension)
{
  T* result = new T[dimension];
  for(int i = 0;i < dimension;i++)
    result[i] = v1[i] - v2[i];
  
  return result;
}

/*************************************************************************************/
//Kernel functions
/*************************************************************************************/

/*************************************************************************************/
//Function : KPolynomial
//Purpose  : Evaluates Polynomial kernel
//Parameters :
//             ptrX1     : First vector (passed as a 1-D array,specifically as a pointer)
//             ptrX2     : Second vector
//             dimension : Dimension of each of the vectors
//             degree    : Degree of the poynomial (defaulted to 2.)
/*************************************************************************************/
template <class T>
T KPolynomial(T* ptrX1,T* ptrX2,int dimension,int degree = 2)
{
  T term = (T)(1 + dotProduct(ptrX1,ptrX2,dimension));
  return (T)(exp(degree * log((float)term)));  //return (1 + x1.x2) ^ degree
}

/*************************************************************************************/
//Function : KGaussianRBF
//Purpose  : Evaluates Gaussian Radial Basis  kernel
//Parameters :
//             ptrX1     : First vector (passed as a 1-D array,specifically as a pointer)
//             ptrX2     : Second vector
//             dimension : Dimension of each of the vectors
//Equation used : exp(||x1-x2||^2)  as in osuna97
/*************************************************************************************/
template<class T>
float KGaussianRBF(T* ptrX1,T* ptrX2,int dimension,double sigma = 0.7071067811865)  //sigma defaulted to 1/sqrt(2)
{
  float result = 0.0f;

  // Calculate the euclidean norm of the difference vector.
  for(int i = 0;i < dimension; i++)
  {
    const T diff = ptrX1[i] - ptrX2[i];
    result += diff*diff;
  }
  
//  float term = sqrt(result);
  
  return (float)exp(-result/(2*sigma*sigma));
}

/*************************************************************************************/
//Function : KGaussianRBFVarNormalized
//Purpose  : Evaluates Gaussian Radial Basis  kernel with variance normalization
//Parameters :
//             ptrX1     : First vector (passed as a 1-D array,specifically as a pointer)
//             ptrX2     : Second vector
//             dimension : Dimension of each of the vectors
//             var       : Variance vector
//Equation used : exp(||x1-x2||^2)  as in osuna97
/*************************************************************************************/
template<class T>
float KGaussianRBFVarianceNormalized(T* ptrX1,T* ptrX2,int dimension,double sigma = 0.7071067811865, T* var=NULL)  //sigma defaulted to 1/sqrt(2)
{
  float result = 0.0f;

  // Calculate the euclidean norm of the difference vector.
  for(int i = 0;i < dimension; i++)
  {
    const T diff = ptrX1[i] - ptrX2[i];
    double mul = diff*diff;
    double mulvar = mul/var[i];
    result += mulvar; //printf("%f - %f -> %f / %f = %f -> %f\n",ptrX1[i],ptrX2[i],mul,var[i],mulvar,result);
  }
  
//  float term = sqrt(result);
//  printf("*** %f %f\n",sigma,exp(-result/sigma));
  return (float)exp(-result/sigma);
}

#ifdef __INTEL_COMPILER
/*************************************************************************************/
//Function : KGaussianRBF
//Purpose  : Same as the previous, but optimized for floats using SSE2.
/*************************************************************************************/
/*
#include <fvec.h>
float KGaussianRBF(float* ptrX1,float* ptrX2,int dimension,double sigma)  //sigma defaulted to 1/sqrt(2)
{
  F32vec4 *pX1 = (F32vec4 *)ptrX1;
  F32vec4 *pX2 = (F32vec4 *)ptrX2;
  F32vec4 result(0,0,0,0);
  F32vec4 diff;


  // Calculate the euclidean norm of the difference vector.
  for(int i = 0;i < dimension/4; i++)
  {
    diff = pX1[i] - pX2[i];
    result += diff*diff;
  }
  
  float term = sqrt(result[0]+result[1]+result[2]+result[3]);
  
  return exp(-term*term/(2*sigma*sigma));
}
*/
#endif //__INTEL_COMPILER

/*************************************************************************************/
//Function : K
//Purpose  : Evaluates kernel function K(x,y) where x and y are vectors
//Parameters :
//             ptrX1     : First vector (passed as a 1-D array,specifically as a pointer)
//             ptrX2     : Second vector
//             dimension : Dimension of each of the vectors
//Note     : Redundant as yet, might be useful later on.
/*************************************************************************************/
template <class T>
float K(T* ptrX1, T* ptrX2, int dimension,char kernel='n',double kernelParameter = -1)
{
  int degree = -1;
  double sigma = -1;
  
  switch(kernel)
  {
  case 'd':
    return dotProduct(ptrX1,ptrX2,dimension);
    break;
  case 'p':
    degree = (int)kernelParameter;
    return KPolynomial(ptrX1,ptrX2,dimension,degree);
    break;
  case 'r':
    sigma = kernelParameter;
    return KGaussianRBF(ptrX1,ptrX2,dimension,sigma);
    break;
  default:
    return dotProduct(ptrX1,ptrX2,dimension);
    break;
  }
}

} // namespace aitvml
