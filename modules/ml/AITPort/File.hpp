/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef __FILE_HPP__
#define __FILE_HPP__

#include <vector>
#include <string>

namespace aitvml
{
class File
{
public:
  
  class file_not_found : public std::exception
  {
    virtual const char* what() { return "File not found"; }
  };

  class write_error : public std::exception
  {
  public:
    write_error() : mWhat("Unable to write.") {}
    write_error(const std::string msg) : mWhat(msg) {}
    virtual ~write_error() throw() {}	// to prevent looser throw specifier error
  private:
    std::string mWhat;
    virtual const char* what() { return mWhat.c_str(); }
  };

  /**
   * Read from a text-mode file to a string memory block. Optimized for large blocks.
   * @throws File::file_not_found.
   */
  static void readTextFileToMemory(const std::string& fname, std::string& stringData);

  /**
   * Dump the contents of the string to a file. This is intended to be 
   * used efficiently with large files.
   * @throws File::write_error.
   */
  static void writeMemoryToTextFile(const std::string& fname, const std::string& stringData);

  /**
   * Read from binary a file to a memory block. Optimized for large blocks.
   * @param maxBytes [in] Maximum bytes to read. 0 Means read all bytes.
   * @throws File::file_not_found.
   */
  static void readBinaryFileToMemory(const std::string& fname, std::vector<unsigned char>& data, int maxBytes = 0);

  /**
   * Dump the contents of the memory to a file. This is intended to be 
   * used efficiently with large files.
   * @throws File::write_error.
   */
  static void writeMemoryToBinaryFile(const std::string& fname, std::vector<unsigned char>& data);

  /**
   * Get a temporary file name from the system. It will create a file with 0 bytes with that name.
   */
  static std::string getTempFileName();

  /**
   * Delete a file from the system.
   */
  static void deleteFile(const std::string& fname);

};

};

#endif
