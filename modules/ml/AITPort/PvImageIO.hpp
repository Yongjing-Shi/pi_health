/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVIMAGEIO_HPP
#define PVIMAGEIO_HPP

namespace aitvml
{

// RAW PGM
unsigned char *it_loadpgm(const char *name,unsigned char **image,
			  unsigned int *width,unsigned int *height);
int it_savepgm(const char *name,unsigned char *image,
	       unsigned int width,unsigned int height);

// RAW PPM
unsigned int *it_loadppm(const char *name,unsigned int **image,
			 unsigned int *width,unsigned int *height);
int it_saveppm(const char *name,unsigned int *image,
	       unsigned int width,unsigned int height);

// PNG
unsigned int *it_loadpng(const char *name,unsigned int **image,
			 unsigned int *width,unsigned int *height, bool *alpha);
int it_savepng(const char *name,unsigned int *image,
	       unsigned int width,unsigned int height, bool alpha);

// JPEG
int it_savejpg(const char *name,unsigned int *image,
	       unsigned int width,unsigned int height, int quality=90);
unsigned int *it_loadjpg(const char *name,unsigned int **image,
			 unsigned int *width,unsigned int *height);

}

#endif
