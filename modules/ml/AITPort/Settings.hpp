/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AIT_Settings_HPP
#define AIT_Settings_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <list>
#include <string>
#include <iostream>
#include <stdexcept>

// AIT INCLUDES
//

#include <modules/ml/AITPort/String.hpp>
#include <modules/ml/AITPort/Vector3.hpp>
#include <modules/ml/AITPort/Rectangle.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "modules/ml/AITPort/Settings_fwd.hpp"

namespace aitvml
{

class Settings
{

public:
  // EXCEPTIONS

  class SettingNotFoundException : public std::runtime_error
  {
  public:
    SettingNotFoundException(const std::string& msg) : std::runtime_error(msg)
    {
    }
  };


  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  Settings(const std::string& appPath = "");

  /**
   * Destructor
   */
  virtual ~Settings();


  //
  // OPERATIONS
  //

public:

  /**
   * Retreives a string from the settings
   * @param key [in] Name of the variable, can be a path. The path is separated by
   * '/' characters. (don't use the '\\' character)
   * @param defaultValue [in] If the variable is not on the registry, this value is returned
   * @param writeDefault [in] If this flag is true (default) it will store the default
   * value in the registry when no values are specified.
   * @param user [in] If this value is true (default) the data is saved under the
   * HKEY_CURRENT_USER branch of the registry, otherwise is regisered
   * under HKEY_LOCAL_MACHINE and the values stored will be common
   * to all users.
   * @return Returns the value of the string stored or the default value
   * @remark Any variables defined in the retreived value are resolved using replaceVars().
   * Values written are not replaced, this means that if the defaultValue has a variable,
   * the variable with the enclosing symbols ('<>') will be written in the registry.
   */
  std::string getString(const std::string& key, const std::string& defaultValue, 
    bool writeDefault = true);

  /**
   * Retreives a string from the settings
   * @param key [in] Name of the variable, can be a path. The path is separated by
   * '/' characters. (don't use the '\\' character)
   * @return Returns the value of the string stored or the default value
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  std::string getString(const std::string& key);

  /**
   * Saves a string on the settings
   * @param key [in] Name of the variable, can be a path
   * @param val [in] Value to be stored
   * @param user [in] If this value is true (default) the data is saved under the
   * HKEY_CURRENT_USER branch of the registry, otherwise is regisered
   * under HKEY_LOCAL_MACHINE and the values stored will be common
   * to all users.
   */
  void setString(const std::string& key, const std::string& val);

  /**
   * Retreives a boolean from the settings. See getString for information
   * on parameters
   */
  bool getBool(const std::string& key, const bool defaultValue, bool writeDefault = true);

  /**
   * Retreives a boolean from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  bool getBool(const std::string& key);

  /**
   * Saves a bool value on the settings. See setString for info on 
   * parameters
   */
  void setBool(const std::string& key, const bool val);

  /**
   * Retreives an integer from the settings. See getString for information
   * on parameters
   */
  int getInt(const std::string& key, const int defaultValue, bool writeDefault = true);

  /**
   * Retreives an integer from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  int getInt(const std::string& key);

  /**
   * Saves a int value on the settings. See setString for info on 
   * parameters
   */
  void setInt(const std::string& key, const int val);

  /**
   *	Returns a list of integers from the settings. The integers are assumed to be
   * comma separated
   * @todo Move this code to another class.
   */
  std::list<int> getIntList(const std::string& key, const std::string& defaultValue, bool writeDefault = true);

  /**
   * Converts a list of doubles to a string, saved at the following key	
   * @todo Move this code to another class.
   */
  void setIntList(const std::string& key, const std::list<int> values);

  /**
   * Retreives a double from the settings. See getString for information
   * on parameters
   */
  double getDouble(const std::string& key, const double defaultValue, bool writeDefault = true);

  /**
   * Gets the value from a string, raises SettingNotFound exception if it 
   * doesn't exist
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  double getDouble(const std::string& key);

  /**
   * Saves a double on the settings. See setString for info on 
   * parameters
   *
   */
  void setDouble(const std::string& key, const double val);

  /**
   * Returns a list of doubles from the settings. The doubles are assumed to be
   * comma separated.
   * @todo Move this code to another class.
   */
  std::list<double> getDoubleList(const std::string& key, const std::string& defaultValue, bool writeDefault = true);

  /**
   * Converts a list of doubles to a string, saved at the following key	
   * @todo Move this code to another class.
   */
  void setDoubleList(const std::string& key, const std::list<double> values);

  /**
   * Retreives a float from the settings. See getString for information
   * on parameters
   */
  float getFloat(const std::string& key, const float defaultValue, bool writeDefault = true)
  { 
    return (float) getDouble(key,defaultValue,writeDefault); 
  }

  /**
   * Retreives a float from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  float getFloat(const std::string& key)
  { 
    return (float) getDouble(key); 
  }

  /**
   * Saves a float on the settings. See setString for info on 
   * parameters
   */
  void setFloat(const std::string& key, const float val)
  {
    setDouble(key,val);
  }

  /**
   * Retreives a Vector3f from the settings. See getString for information
   * on parameters
   */
  Vector3f getCoord3f(const std::string& key, const Vector3f& defaultValue, bool writeDefault = true);

  /**
   * Retreives a Vector3f from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  Vector3f getCoord3f(const std::string& key);

  /**
   * Saves a Vector3f on the settings. See setString for info on 
   * parameters
   */
  void setCoord3f(const std::string& key, const Vector3f& val);

  /**
   * Retreives a Vector2f from the settings. See getString for information
   * on parameters
   */
  Vector2f getCoord2f(const std::string& key, const Vector2f& defaultValue, bool writeDefault = true);

  /**
   * Retreives a Vector2f from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  Vector2f getCoord2f(const std::string& key);

  /**
   * Saves a Vector2f on the settings. See setString for info on 
   * parameters
   */
  void setCoord2f(const std::string& key, const Vector2f& val);

  /**
   * Retreives a Rectanglef from the settings. See getString for information
   * on parameters
   */
  Rectanglef getRectf(const std::string& key, const Rectanglef& defaultValue, bool writeDefault = true);

  /**
   * Retreives a Rectanglef from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  Rectanglef getRectf(const std::string& key);

  /**
   * Saves a Rectanglef on the settings. See setString for info on 
   * parameters
   */
  void setRectf(const std::string& key, const Rectanglef& val);

  /**
   * Retreives a Vector3i from the settings. See getString for information
   * on parameters
   */
  Vector3i getCoord3i(const std::string& key, const Vector3i& defaultValue, bool writeDefault = true);

  /**
   * Retreives a Vector3i from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  Vector3i getCoord3i(const std::string& key);

  /**
   * Saves a Vector3i on the settings. See setString for info on 
   * parameters
   */
  void setCoord3i(const std::string& key, const Vector3i& val);

  /**
   * Retreives a Vector2i from the settings. See getString for information
   * on parameters
   */
  Vector2i getCoord2i(const std::string& key, const Vector2i& defaultValue, bool writeDefault = true);

  /**
   * Retreives a Vector2i from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  Vector2i getCoord2i(const std::string& key);

  /**
   * Saves a Vector2i on the settings. See setString for info on 
   * parameters
   */
  void setCoord2i(const std::string& key, const Vector2i& val);

  /**
   * Retreives a Rectanglei from the settings. See getString for information
   * on parameters
   */
  Rectanglei getRecti(const std::string& key, const Rectanglei& defaultValue, bool writeDefault = true);

  /**
   * Retreives a Rectanglei from the settings. See getString for information
   * on parameters
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  Rectanglei getRecti(const std::string& key);

  /**
   * Saves a Rectanglei on the settings. See setString for info on 
   * parameters
   */
  void setRecti(const std::string& key, const Rectanglei& val);

  /**
   * Deletes a value from the registry. The current implementation
   * does not deletes the subfolders, only the values; however, if
   * all the values and folders are deleted from the application folder,
   * it would delete the folder application. Also if all the applications
   * from the AdvancedInterfaces folder are deleted, it deletes the AdvancedInterfaces
   * folder.
   * @param key [in] Name of the variable, can be a path
   * @param val [in] Value to be stored
   * @param user [in] If this value is true (default) the data is deleted from the
   * HKEY_CURRENT_USER branch of the registry, otherwise is deleted from
   * HKEY_LOCAL_MACHINE and the values stored will be common to all users.
   */
  void deleteValue(const std::string& key);

  /**
   * Set a new path reference for this settings object.
   */
  void changePath(std::string newPath);

  /**
   * Get the current path.
   */
  const std::string& getPath() const { return mCurrentPath; }

  /**
   * Solve any variables located in the input string. The variables are enclosed with
   * the characters '<' and '>'. Their value is looked in the AdvancedInterfaces/Intelligence
   * folder in the registry. If no value is found in this folder, the variables are not replaced.
   */
  static std::string replaceVars(const std::string& value, const std::string& currentPath = "");


  /**
   * Parse the command line. It will search for either the --settings [] parameter,
   * or for option-less command line arguments and parse the file.
   * @param fname [in] Command line from the shell.
   */
  static void parseCommandLine(const std::string& cmdLine);

  /**
   * Parse the XML settings file.
   * @param fname [in] File name of the file to be parsed, default is "settings.xml".
   * @param merge [in] Indicates if the current settings should be merged with the
   * existing settings.
   */
  static void parseXml(const std::string& fname = "settings.xml", bool merge = true);

  /**
   * Parse an XML string.
   * @param xmlStr [in] String containing XML to parsed.
   * @param merge [in] Indicates if the current settings should be merged with the
   * existing settings.
   */
  static void parseMemoryXml(const std::string& xmlStr, bool merge = true);

  /**
   * Get a string variable. The full path must be specified.
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  static std::string getSetting(const std::string& key)
  {
    Settings s;
    return s.getString(key);
  }

  /**
   * Clear the memory settings.
   */
  static void reset();

  /**
   * Indicates to the system that it should use memory settings instead of 
   * registry settings (which is the default). This call should be made at 
   * the beginning of the program, and once made it can not be
   * reverted.
   */
  static void setMemorySettings();

  /**
   * Write the memory node out to file in XML format.
   * @param fname [in] File name of the file to be created from the memory structure.
   */
  static void writeXml(const std::string& fname, bool resolveMacros);

  /**
   * Write the memory node to the standard output.
   */
  static void writeXml(bool resolveMacros);


  //
  // OPERATIONS
  //

protected:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Current folder in the settings.
   */
  std::string mCurrentPath;

};


}; // namespace aitvml

#endif // Settings_HPP
