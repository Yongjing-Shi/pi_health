/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef PVIMAGEPROC_HPP
#define PVIMAGEPROC_HPP

#include <modules/ml/AITPort/Image.hpp>
#include <modules/ml/AITPort/Matrix.hpp>
#include <modules/ml/AITPort/Vector3.hpp>
//#include "ippdefs.h"
//#include "ippi.h"

namespace aitvml
{

class SumOperator
{
public:
  bool isAllocated;
  int *row;
  int w, h, n;

  SumOperator(void) : isAllocated(false), row(NULL) {};
  ~SumOperator(void) { if(isAllocated) delete [] row; };
};

class AvgOperator
{
public:
  bool isAllocated;
  int *row;
  int w, h;

  AvgOperator(void) : isAllocated(false), row(NULL) {};
  ~AvgOperator(void) { if(isAllocated) delete [] row; };
};

class PvImageProc
{
private:

  inline static void rowSum(int *data,int *row,const int n,const int w);
  inline static void rowSumRGB(unsigned char *data,int *row,const int n,const int w);

public:

  enum distConst 
  { 
    METRIC_MAHAL, 
    METRIC_PROB, 
    METRIC_RGBDIRECT 
  };

  enum colorSpaceConst 
  { 
    NORMALIZED0_SPACE = 0,
    NORMALIZED1_SPACE = 1,
    RGB_SPACE = 10, 
    I1I2I3_SPACE = 11, 
    CHROMA_SPACE = 12, 
    CRCGI_SPACE = 13, 
    HSV_SPACE = 14, 
    XYZ_SPACE = 15, 
    GRAY_SPACE = 16,
    FAST_GRAY_SPACE = 17,
    YUV_SPACE = 18,
    YCBCR_SPACE = 19,
    LUV_SPACE = 20,
    YCC_SPACE = 21,
    HLS_SPACE = 22,
    INVALID_SPACE 
  };

  inline static float I1(const float r,const float g,const float b) { return (r+g+b)/3.0f; };
  inline static float I2(const float r,const float g,const float b) { return (r-b)/2.0f; };
  inline static float I3(const float r,const float g,const float b) { return (g*2.0f-r-b)/4.0f; };

  inline static void convertRGBtoI1I2I3(const float r,const float g,const float b,
                                        float &I1,float &I2,float &I3)
  { 
		I1 = (r+g+b)/3.0f;
		I2 = (r-b)/2.0f;
		I3 = (g*2.0f-r-b)/4.0f;
  }

  inline static void convertRGBtoChroma(const float r,const float g,const float b,
                                        float &cr,float &cg,float &cb)
  { 
    float total = r+g+b+1.0f;
		cr = (float)r/total;
		cg = (float)g/total;
		cb = (float)b/total;
  }

  inline static void convertRGBtoCrCgI(const float r,const float g,const float b,
                                        float &cr,float &cg,float &i)
  { 
     i = r+g+b+1.0f;
		cr = 256.0f*r/i;
		cg = 256.0f*g/i;
     i = (i-1.0f)/3.0f;
  }

  inline static void convertRGBtoHSV(const float R, const float G, const float B,
                                        float &H, float &S, float &V)
  { 
    float sq=(R-G)*(R-G)+(R-B)*(G-B);
    V=(R+G+B)/3.0f;
    if(R==G && G==B)
    {
      H=0.0;
    }
    else
    {
      H=(float)acos(0.5f*(2.0f*R-G-B)/(float)sqrt(sq));
      if(B>G) H=2.0f*M_PI-H;
    }
    if(R<G && R<B) S=1.0f-R/V;
    else if(G<R && G<B) S=1.0f-G/V;
    else S=1.0f-B/V;
  }

  inline static void convertRGBtoXYZ(const float R, const float G, const float B,
                                        float &X, float &Y, float &Z)
  {
    // device independent color representation (according to Publication CIE No 15.2 "Colorimetry" and ITU-R Recommendation BT.709) 
    X = 0.412453f*R + 0.357580f*G + 0.180423f*B;
    Y = 0.212671f*R + 0.715160f*G + 0.072169f*B;
    Z = 0.019334f*R + 0.119193f*G + 0.950227f*B;
  }

  /**
   * This function returns the range of the color space (maximum and minimum values for each channel)
   */
  static void getSpaceRange(colorSpaceConst space, float& ch1Min, float& ch2Min, float& ch3Min,
                                                   float& ch1Max, float& ch2Max, float& ch3Max);

  static void ComputeHistogram (unsigned char *pixel, int width, int height,
                         unsigned int *histogram, unsigned int histsize);

  static void ComputeLookup(unsigned int *histogram, float *lookup, int width,
                     int height, unsigned int histsize);

  //////////////////////////////////////////////
  //Equalize an image by its histogram
  static void EqualizeImage(unsigned char *grayimage, int width,
                     int height);

  /////////////////////////////////////////////////
  //Equalize a gray image by its histogram.
  static void EqualizeImage(Image8& pvGray);

  static void ContrastEnhance(int* input,int size,int low=0,int high=255);

  inline static unsigned int sumRGB(unsigned int c0, unsigned int c1);

  //////////////////////////////////////////////////////////////////////////////
  // get mean values of RGB image
  //////////////////////////////////////////////////////////////////////////////
  static Vector3f mean(const Image32 &img);
  static Vector3f meanI1I2I3(const Image32 &img);

  //////////////////////////////////////////////////////////////////////////////
  // get mean and variance of RGB image in different color spaces, RGB is default
  //////////////////////////////////////////////////////////////////////////////
  static void meanAndVariance(const colorSpaceConst space,const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVariance(const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVariance(const Image32 &img,Vector3f &avg, Matrix<float> &var);
  static void meanAndVarianceI1I2I3(const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVarianceI2I3Value(const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVarianceChroma(const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVarianceCrCgI(const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVarianceHSV(const Image32 &img,Vector3f &avg, Vector3f &var);
  static void meanAndVarianceXYZ(const Image32 &img,Vector3f &avg, Vector3f &var);

  //////////////////////////////////////////////////////////////////////////////
  // apply summation filter of size (2n+1 x 2n+1)
  //////////////////////////////////////////////////////////////////////////////
  static void sumOperator(Image<int> &img,const int n);

  static void sumOperatorAlloc(Image<int> &img, const int n, SumOperator &sumOp);
  static void sumOperatorFree(SumOperator &sumOp);
  static void sumOperatorFast(Image<int> &img, int n, SumOperator &sumOp);

  //////////////////////////////////////////////////////////////////////////////
  // apply summation filter of size (2n+1 x 2n+1)
  //////////////////////////////////////////////////////////////////////////////
  static void avgOperator(Image32 &img,const int n);

  static void avgOperatorAlloc(Image32 &img, const int n, AvgOperator &avgOp);
  static void avgOperatorFree(AvgOperator &avgOp);
  static void avgOperatorFast(Image32 &img,const int n, AvgOperator &avgOp);

  static void subSample(const Image32 &img0, Image32 &img1, const int level);
  static void subSample(const Image32 &img0, Image32 &img1);

  //////////////////////////////////////////////////////////////////////////////
  // calculate pixelwise distance from second order statistics
  //////////////////////////////////////////////////////////////////////////////
  static void measure(const colorSpaceConst space,Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureRGB(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);
  static void measureRGB(Image32 &img, Vector3f &mean, Matrix<float> &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureChroma(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureI1I2I3(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureI2I3Value(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureHSV(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureCrCgI(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);

  static void measureXYZ(Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg);
};

/**********************************************************************************
 * Box Car Summation
 **********************************************************************************/
inline void PvImageProc::rowSum(int *data,int *row,const int n,const int w)
{
  const int m=2*n+1;            // mask size
  int sum=0;
  int *rowDest;         
  int *dataDest;
  int *dataPtr;

  // init sum
  dataPtr=data;
  dataDest=data+m;
  while(dataPtr<dataDest) sum+=*(dataPtr++); 
  
  rowDest=row+(w-1-n);  // stop condition for main while loop
  row+=n;

  while(row<rowDest)
  {
    *(row++)=sum;
    sum+=data[m]-*data;
    data++;
  }

  *row=sum;
}

/**********************************************************************************
 * Box Car Summation for RGB data
 **********************************************************************************/
inline void PvImageProc::rowSumRGB(unsigned char *data,int *row,const int n,const int w)
{
  int m=2*n+1;            // mask size
  int m2=4*(2*n+1);       // mask size times four
  int sumR=0;             // running sums
  int sumG=0;
  int sumB=0;
  int *rowDest;         
  unsigned char *dataDest;
  unsigned char *dataPtr;

  dataPtr=data;
  dataDest=data+m*4;
  while(dataPtr<dataDest)
  {
    sumB+=(int)(*(dataPtr++)); 
    sumG+=(int)(*(dataPtr++));
    sumR+=(int)(*(dataPtr++));
    dataPtr++;
  }
  
  rowDest=row+3*(w-1-n);  // stop condition for main while loop
  row+=3*n;
  while(row<rowDest)
  {
    *(row++)=sumB;
    sumB+=(int)(data[m2])-(int)(*data);data++;
    *(row++)=sumG;
    sumG+=(int)(data[m2])-(int)(*data);data++;
    *(row++)=sumR;
    sumR+=(int)(data[m2])-(int)(*data);data+=2;
  }

  *(row++)=sumB;
  *(row++)=sumG;
  *(row++)=sumR;
}


/**********************************************************************************
 * Dilation
 **********************************************************************************/
template<class T> void erode(Image<T> &image,Image<T> &tmp)
{
  int x,y,w=image.width(),h=image.height();

  tmp.resize(w,h);

  for(y=0;y<h;y++)
  {
    for(x=1;x<w-1;x++)
    {
      if(image(x-1,y)!=0 && image(x,y)!=0 && image(x+1,y)!=0) tmp(x,y)=1;
      else tmp(x,y)=0;
    }
  }

  for(y=1;y<h-1;y++)
  {
    for(x=1;x<w-1;x++)
    {
      if(tmp(x,y-1)!=0 && tmp(x,y)!=0 && tmp(x,y+1)!=0) image(x,y)=1;
      else image(x,y)=0;
    }
  }
}

/**********************************************************************************
 * Erosion
 **********************************************************************************/
template<class T> void dilate(Image<T> &image,Image<T> &tmp)
{
  int x,y,w=image.width(),h=image.height();

  tmp.resize(w,h);
  tmp.setAll(0);

  for(y=0;y<h;y++)
  {
    for(x=1;x<w-1;x++)
    {
      if(image(x,y)!=0)
      {
        tmp(x-1,y)=1;
        tmp(x,y)=1;
        tmp(x+1,y)=1;
      }
    }
  }

  for(y=1;y<h-1;y++)
  {
    for(x=1;x<w-1;x++)
    {
      if(tmp(x,y)!=0)
      {
        image(x,y-1)=1;
        image(x,y)=1;
        image(x,y+1)=1;
      }
    }
  }
}

/**********************************************************************************
 * Erosion -- using IPP, in-place version
 **********************************************************************************/
template<class T> void erode(Image<T>&out, const Vector2i& maskSize_)
{
/*
  IppiSize maskSize = {maskSize_.x,maskSize_.y};  
  IppiPoint anchor = {maskSize.width >> 1,maskSize.height>> 1};
  IppiSize dstRoiSize = {out.width()-2*anchor.x, out.height()-2*anchor.y};

  Matrix<unsigned char> pMask(maskSize.width, maskSize.height);
  pMask.setAll(1);

  ippiErode_8u_C1IR(out.pointer()+2*anchor.y*out.width() + 2*anchor.x, out.width(), dstRoiSize, pMask.pointer(), maskSize, anchor);
  */
    // NOTE: IPP currently not available for Linux. Replace this with OpenCV
}

/**********************************************************************************
 * Dilation -- using IPP, in-place version
 **********************************************************************************/
template<class T> void dilate(Image<T>&out, const Vector2i& maskSize_)
{
/*
  IppiSize maskSize = {maskSize_.x,maskSize_.y};  
  IppiPoint anchor = {maskSize.width >> 1,maskSize.height>> 1};
  IppiSize dstRoiSize = {out.width()-2*anchor.x, out.height()-2*anchor.y};

  Matrix<unsigned char> pMask(maskSize.width, maskSize.height);
  pMask.setAll(1);

  ippiDilate_8u_C1IR(out.pointer()+2*anchor.y*out.width() + 2*anchor.x, out.width(), dstRoiSize, pMask.pointer(), maskSize, anchor);
*/
  // NOTE: IPP currently not available for Linux. Replace this with OpenCV
}

// The current IPP doesn't seem to have the non-in-place versions of erode and dilate; the recent documenation does have them.
/**********************************************************************************
 * Erosion -- using IPP, non-in-place version
 **********************************************************************************/
//template<class T> void erode(Image<T>& in, Image<T>& out, const Vector2i& maskSize_)
//{
//
//  IppiSize maskSize = {maskSize_.x,maskSize_.y};  
//  IppiPoint anchor = {maskSize.width >> 1,maskSize.height>> 1};
//  IppiSize dstRoiSize = {out.width()-2*anchor.x, out.height()-2*anchor.y};
//
//  Matrix<unsigned char> pMask(maskSize.width, maskSize.height);
//  pMask.setAll(1);
//
//  ippiErode_8u_C1IR(in.pointer()+2*anchor.y*in.width() + 2*anchor.x, in.width(), 
//    out.pointer()+2*anchor.y*out.width() + 2*anchor.x, out.width(), 
//    dstRoiSize, pMask.pointer(), maskSize, anchor);
//}

/**********************************************************************************
 * Dilation -- using IPP, non-in-place version
 **********************************************************************************/
//template<class T> void dilate(Image<T>& in, Image<T>& out, const Vector2i& maskSize_)
//{
//  IppiSize maskSize = {maskSize_.x,maskSize_.y};  
//  IppiPoint anchor = {maskSize.width >> 1,maskSize.height>> 1};
//  IppiSize dstRoiSize = {out.width()-2*anchor.x, out.height()-2*anchor.y};
//
//  Matrix<unsigned char> pMask(maskSize.width, maskSize.height);
//  pMask.setAll(1);
//
//  ippiDilate_8u_C1IR(in.pointer()+2*anchor.y*in.width() + 2*anchor.x, in.width(),
//    out.pointer()+2*anchor.y*out.width() + 2*anchor.x, out.width(), 
//    dstRoiSize, pMask.pointer(), maskSize, anchor);
//}

/**********************************************************************************
 * Box Car Summation
 **********************************************************************************/
template<class T> inline void rowSum(T *data,int *row,const int n,const int w)
{
  const int m=2*n+1;            // mask size
  int sum=0;
  int *rowDest;         
  T *dataDest;
  T *dataPtr;

  // init sum
  dataPtr=data;
  dataDest=data+m;
  while(dataPtr<dataDest) sum+=int(*(dataPtr++)); 
  
  rowDest=row+(w-1-n);  // stop condition for main while loop
  row+=n;

  while(row<rowDest)
  {
    *(row++)=sum;
    sum+=data[m]-*data;
    data++;
  }

  *row=sum;
}

/*********************************************************************
 * sumOperatorFast()
 *
 * Calc sum over areas of size (2n+1 x 2n+1).
 * 
 *********************************************************************/
template<class T> void sumOperatorFast(Image<T> &img, int n, SumOperator &sumOp)
{
  int w=sumOp.w;
  int h=sumOp.h;
  int *row=sumOp.row;

  int i,j;

  const int ns=n, ne=w-n;

  // image data
  T *data=img.pointer();

  // mask width
  const int m=2*n+1;

  // sum storage
  int *sum=row+(m+1)*w;
  int *sumPtr;
  int *sumStart=sum+ns,*sumEnd=sum+ne;

  // current working accus
  int *row0=row, *rowN=row+m*w;

  // pointer variables
  int *rowPtr;
  T *dataPtr;

  if(sumOp.w!=img.width() || sumOp.h!=img.height() || sumOp.n!=n)
  {
    PvUtil::exitError("PvImageProc::sumOperatorFast(): Error, allocator is incompatible with function call.");
  }

  // init sum
  for(i=ns;i<ne;i++) sum[i]=0;

  // init row accus
  rowPtr=row;
  dataPtr=data;
  for(i=0;i<m;i++)
  {
    rowSum(dataPtr,rowPtr,n,w);
    for(j=ns;j<ne;j++) sum[j]+=rowPtr[j];
    rowPtr+=w;
    dataPtr+=w;
  }

  // perform averaging
  data=data+w*n;
  for(i=n;i<h-n-1;i++)
  {
    dataPtr=data+n;
    sumPtr=sumStart;
    while(sumPtr<sumEnd)
    {
      *(dataPtr++)=*(sumPtr++);
    }
    rowSum(data+w*(n+1),rowN,n,w);
    sumPtr=sumStart;
    j=ns;
    while(sumPtr<sumEnd)
    {
      *(sumPtr++)+=rowN[j]-row0[j];
      j++;
    }
    data+=w;
    row0+=w;
    rowN+=w;
    if(row0==sum) row0=row;
    else if(rowN==sum) rowN=row;
  }

  // store last result
  dataPtr=data+n;
  sumPtr=sumStart;
  while(sumPtr<sumEnd)
  {
    *(dataPtr++)=*(sumPtr++);
  }
}

/*********************************************************************
 * sumOperatorAlloc()
 *********************************************************************/
template<class T> void sumOperatorAlloc(Image<T> &img, const int n, SumOperator &sumOp)
{
  if(sumOp.isAllocated)
  {
    delete [] sumOp.row;
  }
  sumOp.w=img.width();
  sumOp.h=img.height();
  sumOp.n=n;
  sumOp.row=new int [(3*(2*(n)+3)*(img.width()))];
  sumOp.isAllocated=true;
}

/*********************************************************************
 * sumOperator()
 *********************************************************************/
template<class T> void sumOperator(Image<T> &img,const int n)
{
  SumOperator sumOp;

  sumOperatorAlloc(img,n,sumOp);

  sumOperatorFast(img,n,sumOp);

  PvImageProc::sumOperatorFree(sumOp);
}

unsigned int PvImageProc::sumRGB(unsigned int c0, unsigned int c1)
{
  unsigned char *pc0=(unsigned char *)&c0;
  unsigned char *pc1=(unsigned char *)&c1;
  unsigned int r=pc0[R_IDX]+pc1[R_IDX];
  unsigned int g=pc0[G_IDX]+pc1[G_IDX];
  unsigned int b=pc0[B_IDX]+pc1[B_IDX];

  if(r>255) r=255;
  if(g>255) g=255;
  if(b>255) b=255;

  return PV_RGB(r,g,b);
}


template <class T>
T minimum(T* input,int size)
{
  PVASSERT(size > 0);  

  T min = input[0];
  for(int i = 1;i < size;i++)
  {
    if(input[i] < min)
      min = input[i];
  }
  
  return min;
}

template <class T>
T maximum(T* input,int size)
{
  PVASSERT(size > 0);  
  
  T max = input[0];
  for(int i = 0;i < size;i++)
  {
    if(input[i] > max)
      max = input[i];
  }
  
  return max;
}

/**
 * Calculate an integral image from the source image provided. An integral image
 * is an image such that dst(x,y) = sum(i=0..x,sum(j=0..y,src(i,j)))
 * @param src [in] Original image with intensity values.
 * @param dst [out] Integral image.
 */
template <class SrcPixel, class DstPixel>
void computeIntegralImage(const Image<SrcPixel>& src, Image<DstPixel>& dst)
{
  int x,y;

  dst.resize(src.width(),src.height());

  // Assign dst(0,0) = src(0,0)
  dst(0,0) = src(0,0);

  // Calculate the first row
  for (x = 1; x < dst.width(); x++)
  {
    dst(x,0) = dst(x-1,0) + src(x,0);
  }

  // Now calculate the rest of the image.
  for (y = 1; y < dst.height(); y++)
  {
    dst(0,y) = dst(0,y-1) + src(0,y);
    for (x = 1; x < dst.width(); x++)
    {
      dst(x,y) = dst(x-1,y) + dst(x,y-1) - dst(x-1,y-1) + src(x,y);
    }
  }
}

/**
 * Calculate the area given an integral image.
 * @param img [in] Integral image calculated with computeIntegralImage.
 * @param x0,y0,x1,y1 [in] Rectangle that denotes the area to be calculated. The point x1,y1 is
 * not included in the area calculation.
 */
template <class Pixel>
Pixel calcIntegralImageArea(const Image<Pixel>& img, const int x0, const int y0, const int x1, const int y1)
{
  if (x0 == 0)
  {
    if (y0 == 0)
    {
      return img(x1-1,y1-1);
    }
    else
    {
      return img(x1-1,y1-1) - img(x1-1,y0-1);
    }
  }
  else
  {
    if (y0 == 0)
    {
      return img(x1-1,y1-1) - img(x0-1,y1-1);
    }
    else
    {
      return img(x1-1,y1-1) + img(x0-1,y0-1) - img(x1-1,y0-1) - img(x0-1,y1-1);
    }
  }

  return 0;
}

/**
 * Calculate the area given an integral image.
 * @param img [in] Integral image calculated with computeIntegralImage.
 * @param rect [in] Rectangle that denotes the area to be calculated. The point rect.x1 and rect.y1 is
 * not included in the area calculation.
 */
template <class Pixel>
Pixel calcIntegralImageArea(const Image<Pixel>& img, const Rectanglei& rect)
{
  return calcIntegralImageArea(img,rect.x0,rect.y0,rect.x1,rect.y1);
}

} // namespace ait

#endif


