#ifndef _SKINTONEDETECTOR_HPP_
#define _SKINTONEDETECTOR_HPP_

//#include "opencv2/opencv.hpp"
//#include "cv.h"
//#include "cv.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/imgproc/imgproc_c.h"

#include <modules/ml/AITPort/Image.hpp>

#define W_Cb 46.97
#define WL_Cb 23
#define WH_Cb 14
#define W_Cr 38.76
#define WL_Cr 20
#define WH_Cr 10
#define K_l 125
#define K_h 188

#define Cx 109.38
#define Cy 152.02
#define Theta 2.53
#define ECx 1.60
#define ECy 2.41
#define Af 25.39
#define Bf 14.03
//#define Y_min 16
//#define Y_max 235
#define Y_min 1000
#define Y_max 3000

namespace aitvml
{

class SkinToneDetector 
{
  public:

    IplImage *mTemp, *mSkinRegion, *mInputImage, *mTempIn, *mTempOut;

    // Constructor
    SkinToneDetector();

    // Destructor
   ~SkinToneDetector();

    void findSkinTone(const Image32 *pvImage, Image8 *skinRegion, float radius = 1.0, float downSampleFactor = 4);
    void findSkinTone(const IplImage *image, float radius);
    void pvFindSkinTone(const Image32 *pvImage, Image8 *skinRegion);
    double TransCr(unsigned char Y, unsigned char Cr);
    double TransCb(unsigned char Y, unsigned char Cb);
    double MeanCr(unsigned char Y);
    double MeanCb(unsigned char Y);
    double WidthCr(unsigned char Y);
    double WidthCb(unsigned char Y);
};

} // namspace aitvml

#endif /* _CV_HPP_ */

/* End of file. */
