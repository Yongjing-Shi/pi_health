/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceNormalizer_fwd_HPP
#define FaceNormalizer_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace aitvml
{

class FaceNormalizer;
typedef boost::shared_ptr<FaceNormalizer> FaceNormalizerPtr;

}; // namespace aitvml

#endif // FaceNormalizer_fwd_HPP

