/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * Recognition.hpp
 *
 *  Created on: Jan 12, 2016
 *      Author: dkim
 */

#ifndef _RECOGNITION_HPP_
#define _RECOGNITION_HPP_

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include "modules/ml/AITPort/String.hpp"
#include "modules/ml/AITPort/Image.hpp"
#include "modules/ml/AITPort/PvImageConverter.hpp"
#include "modules/ml/SVMC.hpp"

#include "modules/ml/AITPort/FaceDetectorAdaBoost.hpp"
//#include "modules/ml/AITPort/FaceDetectorAdaBoostForeground.hpp"
#include "modules/ml/AITPort/Arclet3DNN.hpp"
#include "modules/ml/AITPort/SparseYawPitchNN.hpp"
#include "modules/ml/AITPort/ModelPerson.hpp"
#include "modules/ml/AITPort/SkinToneDetector.hpp"


namespace aitvml
{

namespace vision
{
	
/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */

class Recognition  
{

public:

//-----------------------------------------------------

  class FaceData
  {
  	  public:

	  FaceData(int mId0, int isFrontLeftRight0, double time0, unsigned int x0, unsigned int y0,
			  unsigned int w0, unsigned int h0) : mId(mId0), isFrontLeftRight(isFrontLeftRight0), x(x0), y(y0), w(w0), h(h0),
			  time(time0), mFaceMarginPixels(10)
  	  {
  	  }
  
	  FaceData(double time0, unsigned int x0, unsigned int y0,
			  unsigned int w0, unsigned int h0) : x(x0), y(y0), w(w0), h(h0),
					  time(time0), mFaceMarginPixels(10)
	  {
	  }
  
	  /**
	   * An integer id associated with each face, useful for debugging.
	   */
	  int mId;

	  // 0 frontal, 1 left, 2 right, 3 face detectors
	  int isFrontLeftRight;
	
	  unsigned int x;
	  unsigned int y;
	  unsigned int w;
	  unsigned int h;

	  //midpoint of segment connecting 2 eyes
	  float ex;
	  float ey;

	  //distance between (above point) and upper lip
	  float sz;
	  float oR;

	  //yaw of face
	  float yw;

	  //pitch of face
	  float pt;

	  //previous yaw, previous pitch
	  float prYw;
	  float prPt;

	  //whether the face is frontal based on the 3D pose estimation
	  int is3DFrontal;

	  double time;

	  Image8 mFaceChip;

	  //integer values of demographics classification
	  unsigned int iGender;
	  unsigned int iEthnicity;
	  unsigned int iAge;

	  // Added by Dave
	  string iGenderString;
	  string iEthnicityString;
	  string iAgeString;


	  //scores of demographics classification
	  std::vector<float> vGenderScore;
	  std::vector<float> vAgeScore;
	  std::vector<float> vEthnicityScore;


	  float mGenderScore, mAfrScore, mCauScore, mHisScore, mOriScore;
	  float mAge;
	  unsigned int getId() {return mId;}

	  //decide wither the given query window has an overlap with the 'this' window
	  bool isContaining(int queryX, int queryY, int queryW, int queryH, bool enforceSize = false)
    {
      int q0X = queryX;        // Upper left corner
      int q0Y = queryY;
      int q1X = q0X + queryW;  // Upper right corner
      int q1Y = q0Y;
      int q2X = q0X;           // Lower left corner
      int q2Y = q0Y + queryH; 
      int q3X = q1X;           // Lower right corner
      int q3Y = q2Y;

      bool overlap = false;
      bool greaterThan = false;

      int startX = this->x;
      int endX = this->x + this->w;
      int startY = this->y;
      int endY = this->y + this->h;

//      PVMSG("%d<->%d && %d<->%d\n",startX,endX,startY,endY);
//      PVMSG("(%d,%d) (%d,%d) (%d,%d) (%d,%d)\n",q0X,q0Y,q1X,q1Y,q2X,q2Y,q3X,q3Y);
//
      if(startX <= q0X && q0X <= endX && startY <= q0Y && q0Y <= endY)
      {
        overlap = true;
      }
      if(startX <= q1X && q1X <= endX && startY <= q1Y && q1Y <= endY)
      {
        overlap = true;
      }
      if(startX <= q2X && q2X <= endX && startY <= q2Y && q2Y <= endY)
      {
        overlap = true;
      }
      if(startX <= q3X && q3X <= endX && startY <= q3Y && q3Y <= endY)
      {
        overlap = true;
      }
      
      if(queryW <= this->w && queryH <= this->h)
        greaterThan = true;

//      if(enforceSize)
//        overlap = overlap && greaterThan;

      return overlap;
    }

	  Rectanglei getRect()
	  {
		  return Rectanglei(x,y,x+w,y+h);
	  }

	  const int mFaceMarginPixels;

	  Image32Ptr createFaceImage(const Image32& fullImage)
	  {
		  Rectanglei cropRect(x-mFaceMarginPixels,y-mFaceMarginPixels,
				  x+w+mFaceMarginPixels,y+h+mFaceMarginPixels);
		  cropRect.intersect(Rectanglei(0,0,fullImage.width(),fullImage.height()));
		  Image32Ptr pFaceImage;
		  pFaceImage.reset(new Image32(cropRect.width(),cropRect.height()));
		  pFaceImage->crop(fullImage,cropRect.x0,cropRect.y0,cropRect.width(),cropRect.height());
		  return pFaceImage;
	  }

  };

  typedef boost::shared_ptr<FaceData> FaceDataPtr;

 
  Recognition();
  virtual ~Recognition();

  virtual void finish();

  // Process with the face detection
  virtual void init();
  virtual void process();
  //virtual void process_old();

  // Process from a face image without face detection
  virtual void initFromFace();
  virtual void processFromFace();

  void classifyFaceSingle(Image8 grayFrame, FaceDataPtr pFace);
  void localizeFace(Image8 grayFrame, FaceDataPtr pFace);
  void poseEstimateFace(Image8 pFaceImg, FaceDataPtr pFace);


  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "DemographicsRecognitionMod"; }



public:

  //
  // ATTRIBUTES
  //
  Image32	faceImage;
  Image8	tmpImg;

  // Additional variables by DAVE (for the purpose of module tests)
  int gVideoWidth, gVideoHeight, gCounter;
  double gVideoFPS;

  // IplImage for visualization
  //IplImage *inImg, *skinToneImg, *ginImg, *fImg1, *fImg2, *fImg3;

  // For visualization
  Image32 frame, faceDetectedImage;
  Image8	grayFrame, mSkinTone, normFaceImage;
  Face gface[20];
  int gNumDetectedFace;

  // Final estimate 
  int iEstGender, iEstAge, iEstRace;
  std::string sEstGender, sEstAge, sEstRace;

 
  //recorded when module starts, used for remembering offset information
  double initTime, time;
  int counter;


  FaceDetectorAdaBoostPtr mpFront;

  
protected:


  int videoWidth, videoHeight;

  
  //just the filename of input video, not entire path (without extension), used to generate output csv filename
  std::string mVideoFileString;

  //output filename (currently outputs demographics)
  std::string mFaceXYWHFile;

  //neural network of some sort
  Arclet3DNN mA3NN;
  Arclet3DNN mRefineA3NN;

  //number of models in use by above neural network
  int numA3NNModel;  
  int numRefineA3NNModel;  

  //3d pose estimation neural network
  SparseYawPitchNN mYPNN;
  int numYPModel;

  //SVMS for classification
  SVMC mGSvm;
  SVMC mAfrSvm, mCauSvm, mHisSvm, mOriSvm;
  SVMC mAgeSvm;
  SVMC mNonFaceSvm;


  //FILE*
  FILE *fpFaceXYWH;

  //skin tone detector 
  SkinToneDetector mSkinToneDetec;
  bool mDoSkinToneDetection;
  //skin tone segmented image
  //Image8 mSkinTone;
  //the size of the skin tone region in color space
  float mSkinRegionRadius;
  bool mDoSkinToneMotionDetection;

  //Image32 mPrevFrame;

  int doGender;
  int doEthnicity;
  int doAge;

  int doNonFaceFilter;
  int doPose;
  int doVisualizeHistory;
  int doRecordLabelsToCSV;

//  int doMotionSegment;
//  int doSkinSegment;

  int showSkinToneWindow;
  float frameSkipFactor;
  float mFaceMatchThreshold;
  float mNonFaceThres;

  float mFaceImageQuality;

//the threshold yaw and pitch angle to determine whether the face is frontal (is3DFrontal)
  float m3DFrontalYawThres;
  float m3DFrontalPitchThres;

  /**
   * Minimum ratio (proportion) of 3D frontal faces out of all the detected faces in a track, so that the track is to be determined as an impression
   */
  float mMin3DFrontalFaceRatio;

 //vector< Vector2i > mFaceHistory;

  /**
   * Minimum number of faces for real time classification.
   */
  double mRealTimeMinTrackFaces;

  /**
   * Minimum track age for real time classification.
   */
  double mRealTimeMinTrackAge;

  /**
   * Minimum track distance traveled.
   */
  double mRealTimeMinTraveledDist;

  /**
   * Minimum track displacement from start to end.
   */
  double mRealTimeMinTotalDisplacement;

  /**
   * Minimum track total (accumulated) face size variations.
   */
  double mRealTimeMinTotalSizeVariation;


  /**
   * The number of seconds the tracker waits for a new face until it terminates a dormant track
   */
  double mRealTimeTrackLatency;

  /**
   * Allow to resize the face view.
   */
  Rectanglei mFaceViewRect;

  /**
   * Visualization background image.
   */
  Image32Ptr mpBackgroundImage;

  /**
   * Display the demo rectangle around face or the debug output.
   */
  bool mDemoVisualization;

  /**
   * Display facial feature marks.
   */
  bool mVisualizeFacialFeatures;

  /**
   * Use this region of interest for the face detection.
   */
  Rectanglei mRoi;

  /**
   * Faces detected on the last frame (used only for demo visualization).
   */
  std::vector<Rectanglei> mLastFrameFaces;

  /**
   * Minimum face size for classification.
   */
  Vector2i mMinFaceClassificationSize;

  /**
   * Unknown range (min,max) for the gender classifier.
   */
  Vector2f mGenderUnknownRange;

  /**
   * See settings file.
   */
  std::string mFaceImagesOutputFolder;

  /**
   * See settings file.
   */
  bool mFaceImagesSendVmsAttribute;

  /**
   * See settings file.
   */
  int mFaceImagesMaxFacesPerHour;

  /**
   * See settings file.
   */
  bool mDoSegmentViewership;

  /*
   * See settings file.
   */
  float mViewershipLatency;

  /**
   * See settings file.
   */
  bool mClassifyAppearanceModelOnly;

  /**
   * See settings file.
   */
  float mUpscaleFactor;

  /**
   * See settings file.
   */
  std::string mCSVOutputPath;

  /**
   * See settings file.
   */
  bool mUseFaceDetectorCombined;

  /**
   * See settings file.
   */
  float mFaceDetectScaleFactor;

  float mSkinToneDownSampleFactor;

  bool mUseNormalizedDistanceValues;

};


}; // namespace vision

}; // namespace aitvml

#endif // RECOGNITION_HPP


