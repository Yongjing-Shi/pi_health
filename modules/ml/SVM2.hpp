/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved							*/
/*  File: SVM2.hpp  -  Modified by Dave														%/
/****************************************************************************/

#ifndef SVM2_HPP
#define SVM2_HPP

#include <string>
#include <modules/ml/AITPort/strutil.hpp>
#include <modules/ml/AITPort/TextCodec.hpp>
//#include <core/TextCodec.hpp>
//#include <core/strutil.hpp>
#include <modules/ml/AITPort/Matrix.hpp>
#include <modules/ml/AITPort/File.hpp>
#include <modules/ml/AITPort/String.hpp>
#include <modules/ml/AITPort/MathUtils.hpp>

#define KERNEL_PARAMETER_DEFAULT_VALUE -1

namespace aitvml
{

/**
 * The resulting label. +1, or -1. 0 for unknown.
 */
typedef int SVM_CLASS_LABEL;

/**
 * Implementation of a Support Vector Machine.
 * The three basic functions are:
 * - Read data from file into designated arrays.
 * - Classify new sample data. classify
 */
template<class T> class SVM
{

protected:
  /**
   * Bias.
   */
  double b;

  /**
   * Dimension of training data vectors.
   */
  int XDimension;
  
  /**
   * Matrix with 1 support vector per row.
   */ 
  aitvml::Matrix<T> mSupportVectors;

  /**
   * Number of support vectors.
   */
  int numSupportVectors;


  /**
   * Variances for normalizing support vectors.
   */
  aitvml::Matrix<T> mVariance;

#ifdef __INTEL_COMPILER
  // 16 byte aligned memory.
  T *mpVectorMemory;
#endif
  
  /**
   * Lagrange multipliers * class labels for the support vectors.
   */
  float* ptrLambdaSupportVectors;

  /**
   * Value of the SVM distance to the boundary in the last
   * classification.
   */
  float mLastDecisionValue;

  /**
   * Type of the kernel.
   * @see setKernelParams
   */
  char mKernelType;

  /**
   * Kernel parameter. Depends on the type.
   * @see setKernelParams
   */
  double mKernelParam;

  /**
   * Uncertainty Gap. This gap is used to classify as unknown values
   * that are too close to the SVM boundary. The range is [0..1].
   * If the distance from the model is inside the interval
   * [MinNegative*UncertaintyGap,MaxPositive*UncertaintyGap], then
   * it will be classified as unknown.
   */
  double mUncertaintyGap;

  /**
   * The maximum distance from the SVM for positive and negative 
   * classes, excluding outlayers.
   */
  double mMaxPositive, mMinNegative;

  /**
   * Whether the support vectors were normalized during training 
   * 1=Yes, 0=No
   */
  int mSVNormalized;


  /**
   * Name of the positive and negative labels.
   */
  std::string mPositiveLabel, mNegativeLabel;

  
public:
  
  /**
   * default constructor.
   */
  SVM();
  
  /**
   * Destructor.
   */
  ~SVM();
  
  /**
   * classify : classifies a new vector as belonging to one of the 
   * two classes. Returns the class label.
   */
  SVM_CLASS_LABEL classify(T* ptrX);
  float applySVM(T* ptrX);

  /**
   * Sets the dimension of training vectors
   */
  void setXDimension(int dimension);
  
  /**
   * Sets the value of the bias b.
   */
  void setBias(double bias);

  /**
   * Set the kernel parameters.
   * @param kernelType [in] Type of kernel used. ('n' = No kernel, 
   * 'r' = Radial basis function, 'p' = Polynomial, 'r'=RBF kernel).
   * @param kernelParameter [in] Has different meanings for different kernels. 
   * - Default 0.
   * - Linear kernel('n') : Dummy value
   * - Polynomial kernel('p') : Degree of the polynomial
   * - RBF kernel ('r')       : sigma in the eqn K(x,y) = exp(-(x-y)^2/(2*sgma*sigma))
   * @remark For further details and more insight refer osuna97 eqn 17.
   */
  void setKernelParams(char kernelType = 'n', double kernelParameter = KERNEL_PARAMETER_DEFAULT_VALUE)
  {
    mKernelType = kernelType;
    mKernelParam = kernelParameter;
  }

  /**
   * Uncertainty Gap. This gap is used to classify as unknown values
   * that are too close to the SVM boundary. The range is [0..1].
   */
  void setUncertaintyGap(double uGap)
  {
    mUncertaintyGap = uGap;
  }

  /**
   * Return true if the distance is in the uncertainty range 
   * [MinNegative*UncertaintyGap,MaxPositive*UncertaintyGap].
   * @param distance [in] Distance from the SVM.
   */
  bool isUnknown(double distance)
  {
    return (distance >= mUncertaintyGap * mMinNegative) &&
           (distance <= mUncertaintyGap * mMaxPositive);
  }
  
  /**
   * Reads the parameters of the SVM obtained after training. These are read 
   * from a file. The file contains the data in a specific format. Two formats
   * are supported. Refer to the templates and the documentation inside the
   * templates.
   */
  void load(const std::string& fname);

  /**
   * Return the SVM value of the last decision.
   */
  float getLastDecisionValue() { return mLastDecisionValue; }

  /**
   * Return the positive label name.
   */
  std::string getPositiveLabel() { return mPositiveLabel; }

  /**
   * Return the negative label name.
   */
  std::string getNegativeLabel() { return mNegativeLabel; }

protected:
  
  /**
   * Allocate memory for the Support vectors.
   */
  void allocateMemory();

  /**
   * Returns true if this character is a space type
   * character.
   */
  bool isBlank(unsigned char ch);

  /**
   * Read a floating point value starting from the given index, and
   * increment the index until the begining of the next token. Tokens
   * are separated by spaces or new line characters.
   */
  float readFloat(std::string& stringData, int& curCharIndex);

  /**
   * Load the svm file from memory.
   */
  void loadFromMemory(std::string& stringData);

};//end of class definition of SVM

template <class T> SVM<T>::SVM()
{
  ptrLambdaSupportVectors = NULL;
  numSupportVectors   = -1;
  mUncertaintyGap = 0.3;
  mKernelType = 'n';
  mKernelParam = KERNEL_PARAMETER_DEFAULT_VALUE;
  mSVNormalized = 0;
#ifdef __INTEL_COMPILER
  // 16 byte aligned memory.
  mpVectorMemory = NULL;
#endif

  // Default values from Gender classifier model.
  mMaxPositive = 2.0826f;
  mMinNegative = -1.8892f;
  
}

template <class T> SVM<T>::~SVM()
{
  if (ptrLambdaSupportVectors)
  {
    delete ptrLambdaSupportVectors;
  }

#ifdef __INTEL_COMPILER
  // 16 byte aligned memory.
  if (mpVectorMemory) _mm_free(mpVectorMemory);
#endif
}

template <class T> void SVM<T>::setXDimension(int dimension)
{
  XDimension = dimension;
}

template <class T> void SVM<T>::setBias(double bias)
{
  b = bias;
}

template <class T> void SVM<T>::load(const std::string& fileName)
{
  std::string stringData;
  //aitvml::File::readTextFileToMemory(fileName,stringData);
  aitvml::TextCodec::decodeFromFile(fileName, stringData);
  loadFromMemory(stringData);
}

template <class T> bool SVM<T>::isBlank(unsigned char ch)
{
  return ch == ' ' || ch == '\n' || ch == '\t' || ch == '\r';
}

template <class T> float SVM<T>::readFloat(std::string& stringData, int& curCharIndex)
{
  int beginIndex = curCharIndex;
  while (!isBlank(stringData[curCharIndex]) && stringData[curCharIndex] != '\0')
  {
    curCharIndex++;
  }

  char lastVal = stringData[curCharIndex];
  stringData[curCharIndex] = '\0';
  // atof calls strlen() internally, if we don't put this
  // end of string character, it will take ages!
  float f = atof(&stringData[beginIndex]); //printf("%s ",stringData[beginIndex]);
  stringData[curCharIndex] = lastVal;

  // Go to next non blank (or eof)   
  // modified by Dave 
  while (curCharIndex < stringData.size() && isBlank(stringData[curCharIndex]))
  {
//	  if(stringData[curCharIndex+1] == EOF) return f;

    curCharIndex++;
  }

  return f;
}

template <class T> void SVM<T>::loadFromMemory(std::string& stringData)
{
  using namespace std;

  string linebuffer;

  // Magic header.
  string magic("AI_SVM1.1");

  int linenumber;
  int curCharIndex = 0;
  for (linenumber = 1;; linenumber++)
  {
    int endLineIndex = curCharIndex;
    while (stringData[endLineIndex] != '\n' && 
      stringData[endLineIndex] != '\0')
    {
      endLineIndex++;
    }
    linebuffer = stringData.substr(curCharIndex,endLineIndex-curCharIndex+1);
    curCharIndex = endLineIndex+1;

    if (linenumber == 1)
    {
      string str = linebuffer;
      if (str.substr(0,magic.length()) != magic)
      {
        PvUtil::exitError("Invalid SVM File format!");
      }
      continue;
    }

    // Skip comments.
    if (linebuffer[0] == '#') continue;
  
    vector<string> tokens = aitvml::tokenize(linebuffer,"=");

//    printf("line %d: %d tokens, curCharIndex=%d",linenumber,tokens.size(),curCharIndex);
//    for(int i=0; i<tokens.size(); i++)
//      printf(" token %d=%s ",i,tokens[i].c_str());
//    printf("\n");
    
    // Skip empty lines.
    if (tokens.empty() || tokens.size() == 1) continue;

    if (tokens.size() == 3)
    {
      // Strip spaces
      tokens[0] = aitvml::strip(tokens[0]);

            // Skip empty lines with only spaces.
      if (tokens[0].empty()) continue;

      // The only valid single token is the "SvmVectors:" tag.
      if (tokens[0] == "SvmVectors")
      {
//        printf("%s!!!\n",tokens[0].c_str());
        // Get out of the loop and read the svm vectors.
        break;
      }
      PvUtil::exitError("SVM<T>::load() Syntax error in line %d\n",linenumber);
    }
    else if (tokens.size() == 2)
    {
      // Strip spaces
      tokens[0] = aitvml::strip(tokens[0]);
      tokens[1] = aitvml::strip(tokens[1]);

      if (tokens[0] == "Title")
      {
        // Ignore for now.
      }
      else if (tokens[0] == "PositiveClassLabel")
      {
        mPositiveLabel = tokens[1];
      }
      else if (tokens[0] == "NegativeClassLabel") 
      {
        mNegativeLabel = tokens[1];
      }
      else if (tokens[0] == "SvmBias")
      {
        b = atof(tokens[1].c_str());
      }
      else if (tokens[0] == "KernelType")
      {
        if (tokens[1].length() == 1)
        {
          mKernelType = tokens[1][0];
        }
        else
        {
          mKernelType = 'r';
//          PvUtil::exitError("SVM<T>::load() Bad value in line %d\n",linenumber);
        }
      }
      else if (tokens[0] == "KernelParameter") 
      {
        mKernelParam = atof(tokens[1].c_str());
      }
      else if (tokens[0] == "UncertaintyGap")
      {
        mUncertaintyGap = atof(tokens[1].c_str());
      }
      else if (tokens[0] == "NumSupportVectors")
      {
        numSupportVectors = atoi(tokens[1].c_str());
      }
      else if (tokens[0] == "XDimension")
      {
        XDimension = atoi(tokens[1].c_str());
      }
      else if (tokens[0] == "MaxPositive")
      {
        mMaxPositive = atof(tokens[1].c_str());
      }
      else if (tokens[0] == "MinNegative")
      {
        mMinNegative = atof(tokens[1].c_str());
      }
      else if (tokens[0] == "SVNormalized")
      {
        mSVNormalized = atoi(tokens[1].c_str());
//        printf("mSVNormalized=%d\n",mSVNormalized);
      }
      else
      {
        PvUtil::exitError("SVM<T>::load() Invalid param in line %d\n",
          linenumber);
      }
    }
    else
    {
      PvUtil::exitError("SVM<T>::load() Syntax error in line %d\n",
        linenumber);
    }
  }

  // Read the SVM values from this point.
  allocateMemory();

  int i,j;

  //static int idx = 0; FILE *fp = fopen(aitvml::aitSprintf("c:\\out%i.txt",idx++).c_str(),"w");

  for(i = 0;i < numSupportVectors ;i++)
  {
    ptrLambdaSupportVectors[i] = readFloat(stringData,curCharIndex);	
    //fprintf(fp,"%f ",ptrLambdaSupportVectors[i]);
    for(j = 0; j < XDimension;j++)
    {
      mSupportVectors(i,j) = static_cast<T>(readFloat(stringData,curCharIndex));
      //fprintf(fp,"%f ",mSupportVectors(i,j));
    }
    //fprintf(fp,"\n");
  }
  
  if(mSVNormalized)
  {
    for(j = 0; j < XDimension;j++)
    {
      mVariance(0,j) = static_cast<T>(readFloat(stringData,curCharIndex));
	  //printf("%d %f\n",j, mVariance(0,j));
      //fprintf(fp,"%f ",mVariance(0,j));
    }
  }
  //fprintf(fp,"%d support vectors has been loaded\n",numSupportVectors);
  //fclose(fp);



}

template <class T> void SVM<T>::allocateMemory()
{
#ifdef __INTEL_COMPILER
  // Align the data to 16 bytes for SIMD Extensions 2
  const int numT = 16/sizeof(T);
  int numCols = XDimension % numT ? XDimension+numT-XDimension%numT : XDimension;
  mpVectorMemory = (T *)_mm_malloc(numSupportVectors*numCols*sizeof(T),16);
  mSupportVectors.mx_fromarray(mpVectorMemory,numSupportVectors,numCols);
  // Fill extra memory with zeros.
  mSupportVectors.zero();
#else
  mSupportVectors.resize(numSupportVectors,XDimension);
#endif
  
  ptrLambdaSupportVectors = new float[numSupportVectors];

  if(mSVNormalized)
  {
    mVariance.resize(1,XDimension);
  }
}

template <class T> SVM_CLASS_LABEL SVM<T>::classify(T* ptrX)
{
  float decisionTerm = applySVM(ptrX);

#ifdef __INTEL_COMPILER
  // Copy the input into a 16-byte aligned buffer
  _mm_free(ptrX);
#endif

  if (isUnknown(decisionTerm))
  {
    return 0;
  }

  return decisionTerm < 0 ? -1 : 1;
  
}

template <class T> float SVM<T>::applySVM(T* ptrX)
{
  if(ptrX == NULL)
    return 0;
  
  float decisionTerm = 0.0f;
  float tolerance = 0.1f;

  int i = 0; //Loop variable

#ifdef __INTEL_COMPILER
  // Copy the input into a 16-byte aligned buffer
  T *pNewX = (T *)_mm_malloc(mSupportVectors.cols()*sizeof(T), 16);
  for (i = 0; i < XDimension; ++i) pNewX[i] = ptrX[i];
  for (; i < mSupportVectors.cols(); ++i) pNewX[i] = 0;
  ptrX = pNewX;
#endif
  
  switch (mKernelType)
  {
  //No specified kernel
  case 'n' :
    for(i=0; i < numSupportVectors;i++)
    { //mKernelParam is just a dummy value and is not used.
      decisionTerm += ptrLambdaSupportVectors[i] * dotProduct(ptrX,&mSupportVectors(i,0),mSupportVectors.cols());
    }
    break;
    
  //Radial basis function
  case 'r' :
//    printf("mKernelParam=%f\n",mKernelParam);
    if(mKernelParam == KERNEL_PARAMETER_DEFAULT_VALUE || mKernelParam <= 0)
    {
      for(i=0; i < numSupportVectors;i++)
      { 
        decisionTerm += ptrLambdaSupportVectors[i] * KGaussianRBF(ptrX,&mSupportVectors(i,0),mSupportVectors.cols());
      }
    }
    else
    {
      for(i=0; i < numSupportVectors;i++)
      { 
        if(mSVNormalized)
        {
          double kern = KGaussianRBFVarianceNormalized(ptrX,&mSupportVectors(i,0),mSupportVectors.cols(),mKernelParam,&mVariance(0,0));
          decisionTerm += ptrLambdaSupportVectors[i] * kern;

          //printf("svs: %f x %f = %f -> %f\n",ptrLambdaSupportVectors[i], kern, ptrLambdaSupportVectors[i]*kern,decisionTerm);

        }
        else
        {
          decisionTerm += ptrLambdaSupportVectors[i] * KGaussianRBF(ptrX,&mSupportVectors(i,0),mSupportVectors.cols(),mKernelParam);
        }
      }
    }
    
    break;
    
  //Polynomial
  case 'p':
    for(i=0; i < numSupportVectors;i++)
    { 
      if(mKernelParam == KERNEL_PARAMETER_DEFAULT_VALUE || mKernelParam <= 0)
        decisionTerm += ptrLambdaSupportVectors[i] * KPolynomial(ptrX,&mSupportVectors(i,0),mSupportVectors.cols());
      else
        decisionTerm += ptrLambdaSupportVectors[i] * KPolynomial(ptrX,&mSupportVectors(i,0),mSupportVectors.cols(),(int)mKernelParam);
    }
    break;
    
  default:
    for(i=0; i < numSupportVectors;i++)
    { 
      decisionTerm += ptrLambdaSupportVectors[i] * dotProduct(ptrX,&mSupportVectors(i,0),mSupportVectors.cols());    
    }
    break;
    
  }//end of switch
  
  decisionTerm -= b;

  mLastDecisionValue = decisionTerm;
  
#ifdef __INTEL_COMPILER
  // Copy the input into a 16-byte aligned buffer
  _mm_free(ptrX);
#endif

  return decisionTerm;
  
}//end of classify

} // namespace aitvml

#endif //SVM_HPP
