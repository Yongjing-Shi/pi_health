/*
 * MessageType.hpp
 *
 *  Created on: Jun 30, 2014
 *      Author: pshin
 */

#ifndef MESSAGETYPE_HPP_
#define MESSAGETYPE_HPP_

#include <string>
#include <list>
#include <deque>
#include <memory>

enum OS_type {ANDROID = 0x11, IOS = 0x12, OS_ETC = 0x13};		// -- Operating System type
enum device_type {Station = 0x1, AP = 0x2, ServiceAgent = 0x3, NullDevice = 0x4};	// -- Station is a client (sending Probe Request), while AP is an access point ((sending Probe Response) and ServiceAgent is not a shopper. The one whose manufacturer is not on the OUI list is marked as NullDevice.
enum wifi_protocol_type {B_802_11 = 0x21, G_802_11 = 0x22, N_802_11 = 0x23};

using namespace std;

// -- for a single OS
typedef struct {
	string addr;
	double timestamp;
	float rss;
} OmniSensr_runtime_t;

// -- for a single location. For a location a, we create a fingerprint.
typedef struct {
	float x;		// -- World coordinate of the marker position
	float y;		// -- World coordinate of the marker position
	list<OmniSensr_runtime_t> sensors;
} fingerprint_runtime_t;

// -- for a single measurement
typedef struct {
	double timestamp;
	int rss;
} meas_t;

// -- for a single OS
typedef struct {
	string addr;
	list<meas_t> meas;	// -- raw RSS measurements
	float dist_mu;		// -- mean of the Gaussian distribution of the RSS measurements
	float dist_std;		// -- std of the Gaussian distribution of the RSS measurements
} OmniSensr_t;

// -- for a single location. For a location a, we create a fingerprint.
typedef struct {
	float x;		// -- World coordinate of the marker position
	float y;		// -- World coordinate of the marker position
	struct tm time_start;	// -- Time that started recording
	struct tm time_end;		// -- Time that ended recording
	int time_v_start;	// -- Unit: sec Time that started recording
	int time_v_end;		// -- Unit: sec Time that ended recording
	string timestring_start, timestring_end;

	list<OmniSensr_t> sensors;
} fingerprint_t;

typedef struct {
	int x;
	int y;
}   Point2D_t;

typedef struct {
	Point2D_t	pos;
	double timstamp_at_AP;
	bool isReported;

	// -- temp.
	Point2D_t	sensr_center;
}  TrackedPos_t_;

//typedef struct {
//	uint i[3];
//}  Triple_uint_t;


typedef struct {
	string addr_prefix;
	string manuf;			// -- Manufacturer of the WiFi device
}   OUI_Lookup_t;

typedef struct {
	clock_t	begin;			// -- Time when begins
	clock_t	last_heard;		// -- Time that has the last activity via this connection
}  connection_t;

typedef struct  {
	int year, month, date, hour, minute;
	float second;
}  timestamp_t;

typedef struct  {
	string src;			// -- The MAC address of the source of the signal. Formated MAC address (e.g., 08:86:3b:81:7b:da)
	string dst;
	string capturedBy;		// -- The IP address of the Sniffer who captured the RSS measurement. (e.g., SerialNumber or 10.250.1.42)
	float distToAP;				// -- Estimated distance from the AP captured this packet
	timestamp_t timestamp;
	double timstamp_at_AP;
	float timediff;			// -- timediff from the previous measurement
	int current_channel;
	int rss;				// -- [dB]
	int deviceType;			// -- If the src is an AP or a Station
	int protocol;			// -- 802.11 protocol type among b/g/n
	char protocol_s[4];			// -- 802.11 protocol type among b/g/n
}  tcpdump_t;

typedef std::shared_ptr<tcpdump_t> tcpdump_t_Ptr;

typedef struct  {
	string addr;
	int channel;	// -- Applicable only for APs
	int osType;		// -- Operating System (OS) type among {Android, iOS, etc}

}  Node_info_t;

typedef struct {
	unsigned int	noTrackedShoppers;
	unsigned int	noTrackedShoppersWithinArea;
	unsigned int	noTrackedAPs;
	unsigned int	noTrackedServiceAgent;
	unsigned int	noTrackedDevs;
	unsigned int	noTrackedDevs_NULL;		// -- # of tracked devices whose manufacturer is NULL
	unsigned int	maxTrackedDevs;
//	unsigned int	maxBufferSize_ap;
//	unsigned int	avgBufferSize_ap;
	unsigned int	maxBufferSize_dev;
	float	avgBufferSize_dev;
} TrackState_t;

typedef struct {
	int x, y;				// -- Location of the ground truth
	deque<float>	distErr;	// -- List of the error distance from the ground truth location
	float mu, std;			// -- Mean and standard deviation of the distErr for this position
} AccuracyEval_Point_t;

//typedef struct {
//	deque<AccuracyEval_Point_t> locs;
//} AccuracyEval_t;

#endif /* MESSAGETYPE_HPP_ */
