/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTracker.hpp
 *
 *  Created on: Jan 8, 2015
 *      Author: dkim, yyoon
 */

#ifndef FACETRACKER_HPP_
#define FACETRACKER_HPP_

#include "opencv2/core/core.hpp"

#include "core/Settings.hpp"

#include "modules/vision/FaceDetector.hpp"
#include "modules/vision/VisualTrackers.hpp"
#include "modules/vision/VisionTarget.hpp"

#include <sys/time.h>

namespace vml {

class SingleFaceTracker: public BaseTracker
{
public:
	SingleFaceTracker(boost::shared_ptr<Settings> settings);
	virtual ~SingleFaceTracker() {}

	virtual void initialize(const cv::Mat &input_image,
			cv::Rect init_rect,
			std::vector<std::vector<cv::Point> > *init_contour);

	virtual bool update(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat *disp_map);

	virtual void drawTrackBox(cv::Mat &disp_image);

private:

	// members for face tracking goes here

};


class FaceTracker {
public:

	FaceTracker(std::string url,
			boost::shared_ptr<Settings> settings);

	~FaceTracker();

	bool process(void);	// grab a new image frame and update the current target list
	const cv::Mat&  getGrabbedImage(void);

private:

	// settings
	boost::shared_ptr<Settings>				mpSettings;

	boost::shared_ptr<FaceDetector>				mpFaceDetector;
	boost::shared_ptr<VisionTargetManager>		mpVisionTargetManager;
	boost::shared_ptr<VideoBuffer<RGBbImage> >	mpVideoBuffer;

};

} // of namespace vml

#endif
