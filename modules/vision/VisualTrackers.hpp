/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisualTrackers.hpp
 *
 *  Created on: Nov 30, 2015
 *      Author: yyoon
 */

#ifndef VISUALTRACKERS_HPP_
#define VISUALTRACKERS_HPP_

#include "opencv2/core/core.hpp"
#include "boost/shared_ptr.hpp"
#include "modules/vision/CameraCalibration.hpp"
#include "core/Settings.hpp"

#include "dlib/image_processing.h"

#define USE_CAMSHIFT 0

namespace vml {

// Trackers
// base class for color histogram tracker
enum TrackingMethods {
	TrackingMethod_HIST_HSV,
	TrackingMethod_HIST_rgCHROM,
	TrackingMethod_HIST_RGB,
	TrackingMethod_DLIB_Correlation_Tracking,
	TrackingMethod_Hybrid,
	TrackingMethod_GeodesicDist	// Geodesic distance transform based tracking
};

#define HSHIST_TRACKBOX_COLOR			cv::Scalar(255,0,0)
#define RGCHROMHIST_TRACKBOX_COLOR		cv::Scalar(0,255,0)
#define RGBHIST_TRACKBOX_COLOR			cv::Scalar(0,0,255)
#define DLIBCORRELATION_TRACKBOX_COLOR	cv::Scalar(255,255,0)

class BaseTracker
{
public:
	BaseTracker(boost::shared_ptr<Settings> settings);
	virtual ~BaseTracker() {}


	virtual void initialize(const cv::Mat &input_image,
			cv::Rect init_rect,
			std::vector<std::vector<cv::Point> > *init_contour) = 0;
	virtual bool update(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat *disp_map) = 0;

	cv::Rect getCropRect(void) const {return mCropRect;}
	cv::Rect getTrackBox(void) const {return mTrackBox;}

	cv::Point getPosition(void) const {return mCenter;}
	float	getConfidence(void) const {return mConfidence;}

	virtual void drawTrackBox(cv::Mat &disp_image) = 0;
	virtual void updateTargetActivityMap(cv::Mat &activity_map);

	static TrackingMethods readTrackingMethod(boost::shared_ptr<Settings> settings);

protected:

	void updateSearchWindow();
	void updateContourMask(std::vector<std::vector<cv::Point> > *contour);

	// search window parameters
	const float		mSearchWindowRatio;	// must be strictly larger than 1

	// image dimension for checking search window bound
	unsigned int	rows;
	unsigned int	cols;


#if USE_CAMSHIFT
	cv::RotatedRect			mTrackBox;
#else
	cv::Rect				mTrackBox;	// tight bounding box of target
#endif

	cv::Rect 				mCropRect;	// search window
	cv::Point 				mCenter;	// image location of the target
	int						mArea;
	float					mConfidence;	// tracking confidence value

	cv::Mat					mContourMask;	// for more precise contour mask

};

class HSHistogramTracker: public BaseTracker
{
public:
	HSHistogramTracker(boost::shared_ptr<Settings> settings);
	virtual ~HSHistogramTracker() {}

	virtual void initialize(const cv::Mat &input_image,
			cv::Rect init_rect,
			std::vector<std::vector<cv::Point> > *init_contour);
	virtual bool update(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat *disp_map);

	virtual void drawTrackBox(cv::Mat &disp_image);

private:
	// for HIST_HSV
	const int		mcHBins;
	const int		mcSBins;
	const float		mcHRangeMin;
	const float		mcHRangeMax;
	const float		mcSRangeMin;
	const float		mcSRangeMax;

	cv::Mat					mHistogram;	// target color histogram.
};

class RGChromHistogramTracker: public BaseTracker
{
public:
	RGChromHistogramTracker(boost::shared_ptr<Settings> settings);
	virtual ~RGChromHistogramTracker() {}

	virtual void initialize(const cv::Mat &input_image,
			cv::Rect init_rect,
			std::vector<std::vector<cv::Point> > *init_contour);
	// update returns true if the updated target has high confidence
	// false otherwise.
	virtual bool update(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat *disp_map);

	virtual void drawTrackBox(cv::Mat &disp_image);

private:
	const int		mcRGBins;
	const float		mcColorRangeMin;
	const float		mcColorRangeMax;	// avoid saturated colors

	cv::Mat					mHistogram;

};

class RGBHistogramTracker: public BaseTracker
{
public:
	RGBHistogramTracker(boost::shared_ptr<Settings> settings);
	virtual ~RGBHistogramTracker() {}

	virtual void initialize(const cv::Mat &input_image,
			cv::Rect init_rect,
			std::vector<std::vector<cv::Point> > *init_contour);
	virtual bool update(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat *disp_map);

	virtual void drawTrackBox(cv::Mat &disp_image);

private:
	const int		mcRGBBins;
	const float		mcColorRangeMin;
	const float		mcColorRangeMax;	// avoid saturated colors

	cv::Mat					mRHistogram;
	cv::Mat					mGHistogram;	// for three channel histograms
	cv::Mat					mBHistogram;

};

class DlibCorrelationTracker: public BaseTracker
{
public:
	DlibCorrelationTracker(boost::shared_ptr<Settings> settings);
	virtual ~DlibCorrelationTracker() {}

	virtual void initialize(const cv::Mat &input_image,
			cv::Rect init_rect,
			std::vector<std::vector<cv::Point> > *init_contour);

	virtual bool update(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat *disp_map);

	virtual void drawTrackBox(cv::Mat &disp_image);

private:
	// dlib::correlation tracker
	dlib::correlation_tracker	mDlibTracker;

	const float	mcBoundingBoxRatio;
};

} // of namespace vml

#endif
