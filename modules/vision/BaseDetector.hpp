/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseDetector.hpp
 *
 *  Created on: Oct 6, 2015
 *  Updated on Jan 5, 2016
 *      Author: yyoon
 *
 */

#ifndef BASEDETECTOR_HPP_
#define BASEDETECTOR_HPP_

#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>

#include <core/Video.hpp>

namespace vml {

struct DetectedTargetData
{
	// contour represents the convex hull
	std::vector<boost::shared_ptr<std::vector<cv::Point> > >	contours;	// pointers to constituting contours
	int						contours_area;			// sum of areas of the contituting contours
	std::vector<cv::Point> 	contour1;		// optional contour of the target. Could be convex hull
	int						contour1_area;
	std::vector<cv::Point> 	contour2;  		// optional contour. Could be expected person shape based on the centroid location
	int						contour2_area;
	cv::Point				center;			// centroid of the detected target
	cv::Rect				bounding_box;	// bounding box of the target
	bool					valid;			// flag for validation
};

// functor for detected target contours area comparison
class DetectedTargetContoursAreaCompare {
public:
	int operator() (const boost::shared_ptr<DetectedTargetData> ct1,
			const boost::shared_ptr<DetectedTargetData> ct2)
	{
		return (ct1->contours_area > ct2->contours_area);
	}
};

class DetectedTargetContour1AreaCompare {
public:
	int operator() (const boost::shared_ptr<DetectedTargetData> ct1,
			const boost::shared_ptr<DetectedTargetData> ct2)
	{
		return (ct1->contour1_area > ct2->contour1_area);
	}
};

class DetectedTargetContour2AreaCompare {
public:
	int operator() (const boost::shared_ptr<DetectedTargetData> ct1,
			const boost::shared_ptr<DetectedTargetData> ct2)
	{
		return (ct1->contour2_area > ct2->contour2_area);
	}
};

// Abstract base class for target detectors
class BaseDetector
{
public:
	BaseDetector(boost::shared_ptr<VideoBuffer<RGBbImage> > video_buffer);
	virtual ~BaseDetector();

	// virtual methods
	// NOTE: detect() must update mBlobMap for target tracking
	// TODO: think about how to enforce this.
	virtual	void detect(void) = 0;	// detect new targets

	void getDetectedTargetList(std::vector<boost::shared_ptr<DetectedTargetData> > &d) const;

	void getBlobMap(cv::Mat &bmap) const {mBlobMap.copyTo(bmap);}

protected:
	// parameters
	int		mMaxTargets;
	int		mMinTargetArea;

	// list of detected target
	std::vector<boost::shared_ptr<DetectedTargetData> >	mDetectedList;

	// detected blob map
	cv::Mat_<unsigned char>		mBlobMap;
};

} // of namespace vml
#endif
