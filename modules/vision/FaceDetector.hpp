/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceDetector.hpp
 *
 *  Created on: Jan 8, 2016
 *      Author: dkim, yyoon
 */

#ifndef FACEDETECTOR_HPP_
#define FACEDETECTOR_HPP_


#include <core/Image.hpp>
#include <core/Video.hpp>
#include <core/Settings.hpp>

#include <modules/vision/BaseDetector.hpp>
#include "opencv2/objdetect/objdetect.hpp"

namespace vml {

// Face Target detector
class FaceDetector : public BaseDetector
{
public:

	FaceDetector(
			boost::shared_ptr<VideoBuffer<RGBbImage> > video_buffer,
			boost::shared_ptr<Settings> settings);

	virtual ~FaceDetector();

	// detect methods
	virtual void detect(void);

	// display
	void drawValidFaces(cv::Mat &image) const;

private:

	// members
	cv::CascadeClassifier	mpCascade;


	// other modules
	boost::shared_ptr<VideoBuffer<RGBbImage> >	mpVideoBuffer;
};

} // of namespace vml
#endif
