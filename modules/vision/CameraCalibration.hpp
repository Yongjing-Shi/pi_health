/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * CameraCalibration.hpp
 *
 *  Created on: Nov 16, 2015
 *      Author: yyoon
 */

#ifndef CAMERACALIBRATION_HPP_
#define CAMERACALIBRATION_HPP_

#include "opencv2/core/core.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include <sys/time.h>
#include <math.h>

#include <boost/shared_ptr.hpp>

#include <core/Settings.hpp>

namespace vml {

// person shape renderer using camera calibration parameter

class CameraModelOpenCV
{
public:

	CameraModelOpenCV(int rows, int cols, boost::shared_ptr<Settings> settings);

	~CameraModelOpenCV();

	// estimate person shape
	void estimatePersonShapeConvexHull(int x, int y, std::vector<cv::Point> &cvxhull);
	void undistortImage(cv::Mat &image_input, cv::Mat &image_output);


private:
	// image parameter
	const int		mRows;
	const int		mCols;
	const float	mCameraDimensionRatio;

	// Camera parameters
	const float	mfx;
	const float	mfy;
	const float	mcx;
	const float	mcy;

	cv::Mat_<float>	mK;	// intrinsic camera matrix
	cv::Vec<float,5> mD;	// distortion parameters

	const float	mk1;
	const float	mk2;
	const float	mk3;
	const float	mk4;
	const float	mk5;
	const float	mp1;
	const float	mp2;
	const float	mCameraHeight;	// height of the camera from the floor

	// person parameters
	const float	mPersonRadius;
	const float	mPersonHeight;

	const float	mCircleInc;

};

} // of namespace vml

#endif
