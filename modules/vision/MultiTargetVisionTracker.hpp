/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * MultiTargetVisionTracker.hpp
 *
 *  Created on: Oct 6, 2015
 *      Author: yyoon
 */

#ifndef MULTITARGETVISIONTRACKER_HPP_
#define MULTITARGETVISIONTRACKER_HPP_

#include "opencv2/core/core.hpp"

#include "core/Settings.hpp"

#include "modules/vision/BackgroundSubtractDetector.hpp"
#include "modules/vision/VisionTarget.hpp"
#include "modules/vision/CameraCalibration.hpp"

#include <sys/time.h>

namespace vml {

class MultiTargetVisionTracker {
public:

	MultiTargetVisionTracker(std::string url,
			boost::shared_ptr<Settings> settings);

	~MultiTargetVisionTracker();

	bool process(void);	// grab a new image frame and update the current target list

	const cv::Mat&  getGrabbedImage(void);
	const cv::Mat&  getForeground(void);
	const cv::Mat& getBackground(void);
	const cv::Mat& getBackProjection(void);

	void getUndistortImage(cv::Mat &cur_undist);

	// indicators
	int getCurrentFrameNumber() {return mpVideoBuffer->getCurrentFrameNumber();}
	unsigned int	getNumActiveTargets() {return mpVisionTargetManager->getNumActiveTargets();}
	unsigned int	getNumTotalTargets() {return mpVisionTargetManager->getNumTotalTargets();}
	unsigned int	getNumInactiveTargets() {return mpVisionTargetManager->getNumInactiveTargets();}
	unsigned int	getNumCompletedTargets() {return mpVisionTargetManager->getNumCompletedTargets();}

	// display methods

	void drawActiveTargets(cv::Mat &display_image) {mpVisionTargetManager->drawActiveTargets(display_image);}
	void drawValidNewTargets(cv::Mat &display_image) {mpTargetDetector->drawValidTargets(display_image);}

	void setTrackingMethod(const TrackingMethods hm) {mpVisionTargetManager->setTrackingMethod(hm);}

private:

	// settings
	boost::shared_ptr<Settings>				mpSettings;

	boost::shared_ptr<TargetDetectorBackgroundSubtraction>	mpTargetDetector;
	boost::shared_ptr<VisionTargetManager>		mpVisionTargetManager;
	boost::shared_ptr<VideoBuffer<RGBbImage> >	mpVideoBuffer;
	boost::shared_ptr<CameraModelOpenCV>		mpCameraModel;

};

} // of namespace vml

#endif
