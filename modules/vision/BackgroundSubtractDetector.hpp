/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BackgroundSubtractDetector.hpp
 *
 *  Created on: Oct 6, 2015
 *  Updated on: Jan 5, 2016
 *      Author: yyoon
 */

#ifndef BACKGROUNDSUBTRACTDETECTOR_HPP_
#define BACKGROUNDSUBTRACTDETECTOR_HPP_


#include <core/Image.hpp>
#include <core/Video.hpp>
#include <core/Settings.hpp>

#include <modules/vision/CameraCalibration.hpp>
#include <modules/vision/BaseDetector.hpp>

#include "opencv2/core/version.hpp"

namespace vml {

// Target detector using background subtraction
class TargetDetectorBackgroundSubtraction : public BaseDetector
{
public:

	TargetDetectorBackgroundSubtraction(
			boost::shared_ptr<VideoBuffer<RGBbImage> > video_buffer,
			boost::shared_ptr<CameraModelOpenCV> camera_model,
			boost::shared_ptr<Settings> settings);

	virtual ~TargetDetectorBackgroundSubtraction();


	// methods
	virtual void detect(void);

#if CV_MAJOR_VERSION == 2
	boost::shared_ptr<cv::BackgroundSubtractorMOG2> getBackgroundSubtractor() {return mpBgSubtractor;}
#elif CV_MAJOR_VERSION == 3
	cv::Ptr<cv::BackgroundSubtractorMOG2> getBackgroundSubtractor() {return mpBgSubtractor;};
#endif

	//void setBackgroundSubtractor(cv::BackgroundSubtractorMOG2 *bgsubtrptr) {mBgSubtractor = bgsubtrptr;}	// for using 3rd party background subtractor;

	const cv::Mat &getBackgroundImage() const {return mBackground;}
	const cv::Mat &getForegroundImage() const {return mForeground;}


	// display
	void drawValidTargets(cv::Mat &image) const;

private:

	typedef struct {
		int		contour_label;
		float	distance;
	} ContourDistanceType;

	// functor for contour distance sort
	class ContourDistCompare {
	public:
		int operator() (const ContourDistanceType &ct1,
				const ContourDistanceType &ct2)
		{
			return (ct1.distance < ct2.distance);
		}
	};
	// members

#if CV_MAJOR_VERSION == 2
	boost::shared_ptr<cv::BackgroundSubtractorMOG2>	mpBgSubtractor;
#elif CV_MAJOR_VERSION == 3
	cv::Ptr<cv::BackgroundSubtractorMOG2> mpBgSubtractor;
#endif

	cv::Mat							mBackground;	// possibly RGBbImage
	GraybImage						mForeground;	// stores processed foreground

	// background subtractor parameters
	const bool	mShadowDetect;

	int		mMaxBlobSize;
	int		mMinBlobSize;
	float	mMaxBlobDistance;

	// temporary list for blob contours
	std::vector<std::vector<cv::Point> > mTempContourList;

	// other modules
	boost::shared_ptr<VideoBuffer<RGBbImage> >	mpVideoBuffer;
	boost::shared_ptr<CameraModelOpenCV>		mpCameraModel;
};

} // of namespace vml
#endif
