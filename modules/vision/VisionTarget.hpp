/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTarget.hpp
 *
 *  Created on: Nov 2, 2015
 *      Author: yyoon
 */

#ifndef VISIONTARGET_HPP_
#define VISIONTARGET_HPP_

#include <sys/time.h>

#include "opencv2/core/core.hpp"

#include "modules/vision/BackgroundSubtractDetector.hpp"
#include "modules/vision/CameraCalibration.hpp"
#include "modules/vision/VisualTrackers.hpp"

namespace vml {

class VisionTrajectory {
public:
	typedef struct {
		cv::Point	loc;
		double		time;
	} TrajectoryPointType;

	VisionTrajectory() {}
	~VisionTrajectory() {}

	void addTrajectoryPoint(cv::Point loc, double t);

	// return trajectory for output
	const std::vector<TrajectoryPointType> & getTrajectory(void) const;

	// return last point time
	double getLastTime(void) const;

	// motion model here
	// currently just use the previous location and enlarged window size

	// trajectory manipulation codes here

private:

	std::vector<TrajectoryPointType>	mTrajectoryPts;
};

class VisionTarget {	// base class for visual tracking target
	friend class VisionTargetManager;
public:

	VisionTarget(unsigned int id);
	VisionTarget(unsigned int id, TrackingMethods hm, boost::shared_ptr<Settings> settings);
	~VisionTarget() {}

	// methods
	// virtual method for target matching or searching
	bool updateTargetStatus(const cv::Mat &input_image,
			boost::shared_ptr<CameraModelOpenCV> cam_model,
			cv::Mat &bpimage,
			double time);
	bool verifyTargetStatus(const cv::Mat &foreground_image);

	void initializeFeature(const cv::Mat &input_image);

	double getLastTime(void) const;

	// virtual for target feature

	// display methods
	void drawTarget(cv::Mat &image) const;
	void updateTargetActivityMap(cv::Mat &activity_map);

private:
	unsigned int			mID;	// target id
	bool					mActive;	// active target or not?
	// geometric features
	std::vector<cv::Point>  mCvxHull;	// initial convex hull
	std::vector<std::vector<cv::Point> >	mContours; // initial contours of the aggregated blobs

	std::vector<cv::Point>	mEstimatedPersonShape;	// estimated person shape convex hull based on the target center

	boost::shared_ptr<BaseTracker>	mpTracker;

	// target trajectory
	VisionTrajectory				mTrajectory;

	// Minimum threshold to keep the target active
	// proportion of the detected blob area to the size of the track box
	const	float			mcTargetVerificationProportionToTrackbox;
};

class VisionTargetManager {
public:

	VisionTargetManager(boost::shared_ptr<VideoBuffer<RGBbImage> > video,
			boost::shared_ptr<BaseDetector> target_detector,
			boost::shared_ptr<CameraModelOpenCV> camera_model,
			boost::shared_ptr<Settings> settings);

	~VisionTargetManager() {}


	bool hasTargets() {return (not mTargetList.empty());}

	void updateTargets(void);
	// target manipulation methods
	void addNewTargets(std::vector<boost::shared_ptr<DetectedTargetData> > &blob_list);
//	void removeTarget();

	void setTrackingMethod(TrackingMethods hm) {mTrackingMethod = hm;}

	// display
	void drawActiveTargets(cv::Mat &image) const;

	cv::Mat	&getBackProjImage(void) {return mBackProjImage;}

	void retrieveCompletedTrajectories(std::vector<VisionTrajectory> &trajlist);	// change return type accordingly

	/* class VisionTrajectory */
	unsigned int getNumActiveTargets(void) {return mnActiveTargets;}
	unsigned int getNumTotalTargets(void);
	unsigned int getNumInactiveTargets(void);
	unsigned int getNumCompletedTargets(void) {return mCompletedTargetList.size();}

private:

	// helpers
	void targetTrackerAssignHelper(boost::shared_ptr<VisionTarget> target,
			DetectedTargetData &blob);

	// members
	std::map<unsigned int,boost::shared_ptr<VisionTarget> > mTargetList;	//
	std::vector<boost::shared_ptr<VisionTarget> >			mCompletedTargetList;

	unsigned int				mLastID;

	unsigned int				mnActiveTargets;	// number of active targets. To save time for checking this

	cv::Mat_<int>	mInactiveTargetMap;		// for managing inactive targets
	cv::Mat_<unsigned char>		mTargetActivityMap;

	cv::Mat	mBackProjImage;	// for debugging

	TrackingMethods	mTrackingMethod;

	// other modules
	boost::shared_ptr<VideoBuffer<RGBbImage> > mpVideoBuffer;
	// TODO: declare TargetDetector as superclass and create pointer here
	boost::shared_ptr<BaseDetector>	mpTargetDetector;
	boost::shared_ptr<CameraModelOpenCV>  mpCameraModel;

	boost::shared_ptr<Settings>	mpSettings;
	// const parameters
	double	 	mInactiveTargetInterval;
	int			mInactiveMapWindowRadius;

};

} // of namespace vml

#endif
