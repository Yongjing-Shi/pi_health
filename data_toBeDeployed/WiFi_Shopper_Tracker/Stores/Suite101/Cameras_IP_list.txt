# -- Unit: [cm]
# -- Note: Interfaces' (X,Y) coordinates must be different even when they are in the same AP
# -- IP_Addr 	X_coordinates 	Y_coordinates

#10.250.1.40 1150 2350
#10.250.1.41 1150 2050
#10.250.1.42 1180 1650
#10.250.1.43 1100 1450
#10.250.1.44 1250 1050


# -- DON'T DELETE THIS AND THE FOLLOWING LINES

