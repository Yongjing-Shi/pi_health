//============================================================================
// Name        : test_JsonByNlohmann.cpp
// Author      : Paul J. Shin, Yongjing
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <iosfwd>
#include <string>
#include <algorithm>
#include <unordered_map>
#include "json.hpp"

using json = nlohmann::json;
using namespace std;

int main() {

//	//--------------- Test Case 1: a file in JSON format---------------------------
//	std::ifstream ifs("test3.json");
//	json j(ifs);
//	json::iterator it = j.find("glossary");
//	std::cout << it.key() << " : " << it.value() << "\n";
//	json j_f = j.flatten();
//	json::iterator it1 = j_f.find("/glossary/GlossDiv/GlossList/GlossEntry/float");
//	cout << "it1 is found? " << (it1 != j_f.end()) << endl;


	//----------------Test case 2: healthMetric.json as input---------------------
	ifstream healthIfs("healthMetric.json");
	json healthJsonObj(healthIfs);

//	// Iterate all keys in json file
//	for (json::iterator it = healthJsonObj.begin(); it != healthJsonObj.end(); ++it) {
//		string key = it.key();
//		ostringstream value;
//		value << it.value();
//		std::cout << key << " : " << value.str() << "\n";
//	}
//
//	// Flat Json Object to find string
//	healthJsonObj = healthJsonObj.flatten();
//	json::iterator it1 = healthJsonObj.find("/Thing Shadow/frequency/Report");
//	if (it1 != healthJsonObj.end())
//	{
//		cout << "Found? " << (it1 != healthJsonObj.end()?"Yes":"No") << endl;
//		string key = it1.key();
//		cout << key << " : " << it1.value() << endl;
//	}
//
//	// Add judgement of key found or not
//	string str2 = "/Device management/temperature/Collect";
//	json::iterator it2 = healthJsonObj.find(str2);
//	if (it2 != healthJsonObj.end()){
//		cout << "Found? " << (it2 != healthJsonObj.end()?"Yes":"No") << endl;
//		string key2 = it2.key();
//		cout << key2 << " : " << it2.value() << endl;
//	}else{
//		std::cout << "Not Found" << std::endl;
//	}

	string str3 = "{\"Clean\": \"null\",\"CPU\": {\"Collect\":5,\"Report\":6},\"Temp\": {\"Collect\":7,\"Report\":8}}";	// Assume this string is from Delta message of IoT thing shadow
	auto j1 = json::parse(str3);
	std::cout << j1.dump(4) << std::endl;

	// --Try to combine parser and hashmap
	// --(1) Insert new metric in HashMap
	unordered_map<string, string> metric_map;		// Assume this HashMap is already set up before we get latest Delta message
	metric_map.insert({"CPU", "{\"Collect\":3,\"Report\":3}"});
	metric_map.insert({"Clean", "{\"Collect\":2,\"Report\":2}"});

	// --(2) Update existing metric in HashMap
	for (json::iterator it = j1.begin(); it != j1.end(); ++it) {		// Traversal all keys in Delta Message
		string key = it.key();
		ostringstream value;
		value << it.value();
		auto search = metric_map.find(key);
		if(search != metric_map.end()) {	// if this metric is already in HashMap
			cout << "Metric is Found" << endl;
			std::cout << "[Old] " << search->first << ":" << search->second << "  ";
			search->second = value.str();	// Update value of metric
			std::cout << "[New] " << search->first << ":" << search->second << '\n';
		}else{	// this metric is new to HashMap
			cout << "Metric is New" << endl;
			metric_map.insert({key,value.str()});
			std::cout << "[New] " << key << ":" << value.str() << '\n';
		}
	}


	//---------------Test case 3: stream string as input-------------------------

//	json j_f = j.flatten();
//
//	// special iterator member functions for objects
//	for (json::iterator it = j_f.begin(); it != j_f.end(); ++it) {
//		string key = it.key();
//		ostringstream value;
//		value << it.value();
//		std::cout << key << " : " << value.str() << "\n";
//	}
//
//	json::iterator it1 = j_f.find("/glossary/GlossDiv/GlossList/GlossEntry/boolean");
//	cout << "it1 is found? " << (it1 != j_f.end()) << endl;


//	it1.value() = 4568.99;
////	j_f["/glossary/GlossDiv/GlossList/GlossEntry/float"] = 4568.99;
//	std::cout << it.key() << " : " << it1.value() << "\n";

//	float value_f = j["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["float"].get<float>();
//	std::cout << it.key() << " : " << value_f << "\n";
//
//	j["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["float"] = 987.8888;
//	value_f = j["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["float"].get<float>();
//	std::cout << it.key() << " : " << value_f << "\n";
//
//	json j_uf = j_f.unflatten();
//	// special iterator member functions for objects
//	 std::cout << std::setw(4) << j << '\n';
//	 std::cout << std::setw(4) << j_uf << '\n';


//
//	// create object from string literal
//	json j1 = "{ \"happy\": true, \"pi\": 3.141 }"_json;
//
//	// or even nicer (thanks http://isocpp.org/blog/2015/01/json-for-modern-cpp)
//	auto j2 = R"(
//	  {
//	    "Unhappy": true,
//	    "2pi": 6.282
//	  }
//	)"_json;
//
//	// or explicitly
//	auto j3 = json::parse("{ \"happy\": true, \"pi\": 3.141 }");
//
//	// explicit conversion to string
//	std::string s = j2.dump();    // {\"happy\":true,\"pi\":3.141}
//
//	// serialization with pretty printing
//	// pass in the amount of spaces to indent
//	std::cout << s << std::endl;
////	std::cout << j1.dump(4) << std::endl;
//	// {
//	//     "happy": true,
//	//     "pi": 3.141
//	// }
//
//	// the setw manipulator was overloaded to set the indentation for pretty printing
//	cout << setw(4) << j2 << endl;

	return 0;
}
