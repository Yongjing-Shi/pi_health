//============================================================================
// Name        : OmniSensr_SampleApp.cpp
// Author      : Paul J. Shin
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

//#include <string.h>
//#include <sstream>
//#include <cstdio>
//#include "unistd.h"
//#include <pthread.h>
//#include <iostream>
//#include <iomanip>

// -- AWS IoT SDK files for State Parameter Type definition
//#include "aws_iot_error.h"
//#include "aws_iot_shadow_json_data.h"
#include "aws-iot-sdk/aws_iot_src/utils/aws_iot_error.h"
#include "aws-iot-sdk/aws_iot_src/shadow/aws_iot_shadow_json_data.h"
#include "modules/iot/IoT_Common.hpp"
#include "modules/iot/IoT_Gateway.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "OmniSensr_SampleApp.hpp"

/* -- How to deploy and run
 *
 * sshpass -p 'videomininglabs' scp PacketProcessing_at_Sniffer.cpp root@10.250.1.45:
 * sshpass -p 'videomininglabs' ssh root@10.250.1.45 'g++ PacketProcessing_at_Sniffer.cpp -o PacketProcessing_at_Sniffer'
 * sshpass -p 'videomininglabs' ssh root@10.250.1.45 './PacketProcessing_at_Sniffer'
 *
 */

using namespace std;

#define MQTT_DEBUG		true
#define DEBUG			true

#define MAX_BUFFER_SIZE		299

pthread_mutex_t gMutex;
IoT_Gateway	OmniSensr_SampleApp::mIoTGateway;

OmniSensr_SampleApp::OmniSensr_SampleApp() {
	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);
}

OmniSensr_SampleApp::~OmniSensr_SampleApp() {
}


bool OmniSensr_SampleApp::ParseSetting(string configpath) {
	FILE *fp;

	fp = fopen(configpath.c_str(), "r");
	if(fp == NULL) {
		fprintf(stderr, "Error opening %s file\n", configpath.c_str());
		exit(0);
	}
	else {

		printf("[OmniSensr_SampleApp] ParseSetting(%s): \n", configpath.c_str());

		char buffer[MAX_BUFFER_SIZE];
		// -- Read from remote broker and publish to local broker
		string lineInput;
		while(fgets(buffer, MAX_BUFFER_SIZE, fp)) {	// -- Reads a line from the pipe and save it to buffer
			lineInput.clear();
			lineInput.append(buffer);

			// -- If there's any comment, then discard the rest of the line
			std::size_t found = lineInput.find_first_of("#");
			// -- If '#' is found, then discard the rest.
			if(found != string::npos)
				lineInput.resize(found);

			// -- Break the line input into words and save it into a word vector
			istringstream iss (lineInput);
			vector<string> v;
			string s;
			while(iss >> s)
				v.push_back(s);

			// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #SN, IP}
			if(v.size() >= 2 && v[0].size() > 0) {
				if(v[0].compare("VERSION") == 0) {
					mSoftwareVersion = v[1];
					printf("VERSION = %s\n", mSoftwareVersion.c_str());
				} else

				if(v[0].compare("PERIODIC_BUFFER_PROCESSING_INTERVAL") == 0) {
					mPeriodicBufferProcessingInterval = atoi(v[1].c_str());
					printf("PERIODIC_BUFFER_PROCESSING_INTERVAL = %d\n", mPeriodicBufferProcessingInterval);
				} else

				if(v[0].compare("PERIODIC_STATE_REPORTING_INTERVAL") == 0) {
					mPeriodicStateReportingInterval = atoi(v[1].c_str());
					printf("PERIODIC_STATE_REPORTING_INTERVAL = %d\n", mPeriodicStateReportingInterval);
				}
			}
		}
	}

	fclose(fp);
	return true;
}

void OmniSensr_SampleApp::IoT_ControlChannel_CallBack_func(string controlMsg) {
	if(DEBUG)
		printf("******* Control Channel Msg Received *******\n%s\n********************************************\n", controlMsg.c_str());

// -- Parse the message as you wish and take actions if needed
}

/*
 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
 *
 * TODO: This is basically a JSON parser plus extra, so we can make this better by using a formal JSON parser.
 */

void OmniSensr_SampleApp::IoT_DeltaState_CallBack_func(string deltaMsg) {
	if(DEBUG)
		printf("+++++++ Delta State Received ++++++++++++++++++\n%s+++++++++++++++++++++++++++++++++++++++++++++++\n", deltaMsg.c_str());

	// parse the line into blank-delimited tokens
	int n_token = 0; // a for-loop index
	// array to store memory addresses of the tokens in buf
	char* token[MAX_IOT_STATE_TOKENS_PER_LINE] = {}; // initialize to 0

	// parse the line
	token[0] = strtok((char *)deltaMsg.c_str(), AWS_IOT_THINGSHADOW_DELIMITER); // first token
	if (token[0]) // zero if line is blank
	{
	  for (n_token = 1; n_token < MAX_IOT_STATE_TOKENS_PER_LINE; n_token++)
	  {
		token[n_token] = strtok(0, AWS_IOT_THINGSHADOW_DELIMITER); // subsequent tokens
		if (!token[n_token]) break; // no more tokens
	  }
	}

//		if(DEBUG) {
//			//printf("/////////// IoT_DeltaState_CallBack_func() Tokens: n_token = %d\n", n_token);
//			printf("[Parsed messages] ");
//			for (int i = 0; i < n_token; i++) { // -- n_token = #of tokens
//				printf("[%s] ", token[i]);
//			}
//		}

	for (int i = 0; i < n_token; i+=2) { // -- n_token = #of tokens

		string keyStr(token[i]);

		if(keyStr.size() == 0) {
			i++;
			continue;
		}

		// -- In case of a string-type state, there could be a single comma token. In that case, just skip it.
		if(token[i][0] == ',') {
			i++;
			continue;
		}

		// -- Erase comma in the string: The comma must be at the end of the string, so erase it only at the end word.
		if(keyStr.c_str()[keyStr.size()-1] == ',')
			keyStr.resize(keyStr.size()-1);

		//printf("keyStr = [%s]\n", keyStr.c_str());

		devState_t *stateParam = mIoTGateway.FindStateParam(keyStr);

		pthread_mutex_lock(&gMutex);
		// -- Check if there's matching state parameter
		if(stateParam != NULL) {
			if(DEBUG) printf("[Delta Message] stateParam FOUND (key = %s)\n", stateParam->key.c_str());

			// -- Do something here to react to the received delta message to this particular state parameter.
			switch(stateParam->type) {
				case SHADOW_JSON_INT32:
				case SHADOW_JSON_INT16:
				case SHADOW_JSON_INT8: {

					int stateParam_desired;
					string actualValue(token[i+1]);
					// -- Erase comma in the string: The comma must be at the end of the string, so erase it only at the end word.
					if(actualValue.c_str()[actualValue.size()-1] == ',')
						actualValue.resize(actualValue.size()-1);

					sscanf(actualValue.c_str(), "%d", &stateParam_desired);

					if(DEBUG)
						printf("(report, desired) = (%d, %d) -- > ", *(int*)stateParam->value, stateParam_desired);

					// -- Replace the state parameter with the desired value
					*(int*)stateParam->value = stateParam_desired;

					if(DEBUG)
						printf("(%d)\n", *(int*)stateParam->value);

				}
					break;
				case SHADOW_JSON_UINT32:
				case SHADOW_JSON_UINT16:
				case SHADOW_JSON_UINT8: {
					unsigned int stateParam_desired;
					string actualValue(token[i+1]);
					// -- Erase comma in the string
					if(actualValue.c_str()[actualValue.size()-1] == ',')
						actualValue.resize(actualValue.size()-1);

					sscanf(actualValue.c_str(), "%u", &stateParam_desired);

					if(DEBUG)
						printf("(report, desired) = (%u, %u) --> ", *(unsigned int*)stateParam->value, stateParam_desired);

					// -- Replace the state parameter with the desired value
					*(unsigned int*)stateParam->value = stateParam_desired;

					if(DEBUG)
						printf("(%u)\n", *(unsigned int*)stateParam->value);
				}
					break;
				case SHADOW_JSON_FLOAT: {
					float stateParam_desired;
					string actualValue(token[i+1]);
					// -- Erase comma in the string
					if(actualValue.c_str()[actualValue.size()-1] == ',')
							actualValue.resize(actualValue.size()-1);

					sscanf(actualValue.c_str(), "%f", &stateParam_desired);

					if(DEBUG) {
						printf("[%s] ", actualValue.c_str());
						cout << fixed << setprecision(IOT_FLOAT_PRECISION);
						cout << " (report, desired) = ("<< *(float*)stateParam->value << ", " << stateParam_desired << ") --> ";
					}

					// -- Replace the state parameter with the desired value
					*(float*)stateParam->value = stateParam_desired;

					if(DEBUG) {
						cout << fixed << setprecision(IOT_FLOAT_PRECISION);
						cout << "("<< *(float*)stateParam->value << ")" << endl;
					}
				}
					break;
				case SHADOW_JSON_DOUBLE: {
					double stateParam_desired;
					string actualValue(token[i+1]);
					// -- Erase comma in the string
					if(actualValue.c_str()[actualValue.size()-1] == ',')
						actualValue.resize(actualValue.size()-1);

					sscanf(actualValue.c_str(), "%lf", &stateParam_desired);

					if(DEBUG) {
						printf("[%s] ", actualValue.c_str());
						cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
						cout << " (report, desired) = ("<< *(double*)stateParam->value << ", " << stateParam_desired << ") --> ";
					}

					// -- Replace the state parameter with the desired value
					*(double*)stateParam->value = stateParam_desired;

					if(DEBUG) {
						cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
						cout << "("<< *(double*)stateParam->value << ")" << endl;
					}
				}
					break;
				case SHADOW_JSON_BOOL: {
					char stateParam_desired[99] = {'\0'};
					string actualValue(token[i+1]);
					// -- Erase comma in the string
					if(actualValue.c_str()[actualValue.size()-1] == ',')
						actualValue.resize(actualValue.size()-1);

					sscanf(actualValue.c_str(), "%s", stateParam_desired);

					if(DEBUG)
						printf("(report, desired) = (%d, %s) --> ", *(bool*)stateParam->value, stateParam_desired);

					// -- Replace the state parameter with the desired value
					if(stateParam_desired[0] == 't')	// -- true
						*(bool*)stateParam->value = true;
					if(stateParam_desired[0] == 'f')	// -- false
						*(bool*)stateParam->value = false;

					if(DEBUG)
						printf("(%d)\n", *(bool*)stateParam->value);

				}
					break;

				case SHADOW_JSON_STRING: {
//					char stateParam_desired[99] = {'\0'};
					string actualValue(token[i+1]);
					// -- Erase comma in the string: The comma must be at the end of the string, so erase it only at the end word.
					if(actualValue.c_str()[actualValue.size()-1] == ',')
						actualValue.resize(actualValue.size()-1);

					if(DEBUG)
						printf("(report, desired) = (%s, %s) -->", (*(string*)stateParam->value).c_str(), actualValue.c_str());

					// -- Replace the state parameter with the desired value
					*(string*)stateParam->value = actualValue;

					if(DEBUG)
						printf("(%s)\n", (*(string*)stateParam->value).c_str());

				}
					break;

				default:
					break;
			}
		}
		pthread_mutex_unlock(&gMutex);
	}

	// -- Put into a message buffer for transmission
	mIoTGateway.SendStateInfo();

}

void OmniSensr_SampleApp::PeriodicProcessing_Loop() {

	double currTime, prevTime_bufferProc,  prevTime_stateReport;
	prevTime_bufferProc = gCurrentTime();

	// -- Infinite loop to periodically send data and state update.
	int data_cnt = 0;
	do {

		currTime = gCurrentTime();

		// -- Periodically send the State Info to Thing Shadow
		if(currTime - prevTime_stateReport >= mPeriodicStateReportingInterval) {
			prevTime_stateReport = currTime;

			// -- Put into a message buffer for transmission
			mIoTGateway.SendStateInfo();
		}

		// -- Periodically process/send data to the IoT Cloud
		if(currTime - prevTime_bufferProc >= mPeriodicBufferProcessingInterval) {
			prevTime_bufferProc = currTime;


			// -- ######################################################################
			// -- User-defined Data Generation code
			ostringstream data_cnt_str;

			data_cnt_str << "Data Count: " << data_cnt++;

			// -- Send data
			if(data_cnt_str.str().size() > 0)
			{
				// -- Put into a message buffer for transmission
				// -- The data message will be sent to a topic named by 'AWS_IOT_APP_DATA_CHANNEL_PREAMBLE/<store_name>/<serial_number>/<app_id>'
				mIoTGateway.SendData(data_cnt_str.str());

				if(DEBUG)
					printf("[SampleApp] Sending Data: %s\n", data_cnt_str.str().c_str());
			}
			// -- ######################################################################

		}

	} while(1);
}

void* OmniSensr_SampleApp::PeriodicProcessingThread(void *apdata) {

	OmniSensr_SampleApp* pb = (OmniSensr_SampleApp*)apdata;

	pb->PeriodicProcessing_Loop();

    return NULL;
}

void OmniSensr_SampleApp::Run(string configFile)
{
	// -- Set the initial time clock. Unit: [sec]
	mInitialTime = gCurrentTime();

	// -- Read config file
	ParseSetting(configFile);

	{
		mAppID.clear();
		mAppID.append(APP_ID_SAMPLE_APP);

		mMqttTopic_State.clear();
		mMqttTopic_State.append(AWS_IOT_APP_STATE_CHANNEL_PREAMBLE);
		mMqttTopic_State.append("/");
		mMqttTopic_State.append(mAppID);

		printf("mMqttTopic_State = %s\n", mMqttTopic_State.c_str());
	}

	// -- Register Delta State callback function
	mIoTGateway.RegisterDeltaCallbackFunc(&IoT_DeltaState_CallBack_func);

	// -- Register Control Channel callback function
	mIoTGateway.RegisterControlCallbackFunc(&IoT_ControlChannel_CallBack_func);

	// -- Start running aws-iot-gateway with a ThingShadow topic
	mIoTGateway.Run(mAppID, mMqttTopic_State, IOT_GATEWAY_CONFIG_FILE);

	// -- ######################################################################
	// -- User-defined State Parameter Definition
	// -- Add state parameters
	// -- Note: You don't need to declare the state parameters to be 'global' since the pointers to the state parameters will be carried through.
	/*
	 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
	 * So just use the 6 parameter types when defining a state parameter.
	 */
	float iot_stateParam_float = 3.345678f;
	mIoTGateway.AddStateParam(SHADOW_JSON_FLOAT, "state-float", &iot_stateParam_float, NULL);

	double iot_stateParam_double = 3.345678912345678d;
	mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "state-double", &iot_stateParam_double, NULL);

	int iot_stateParam_int = 123456789;
	mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "state-int", &iot_stateParam_int, NULL);

	bool iot_stateParam_bool = true;
	mIoTGateway.AddStateParam(SHADOW_JSON_BOOL, "state-bool", &iot_stateParam_bool, NULL);

	string iot_stateParam_string = "OmniSensr_SampleApp Testing \"Hello World!\" (x,y,z, 3456.876423) ";
	mIoTGateway.AddStateParam(SHADOW_JSON_STRING, "state-string", &iot_stateParam_string, NULL);
	// -- ######################################################################

	if(pthread_create(&mPeriodicProcessing_thread, &mThreadAttr, PeriodicProcessingThread, (void *)this))
	{
		fprintf(stderr, "Error in pthread_create(PeriodicProcessingThread in AWS_IoT_Module)\n");
		exit(0);
	}

	// -- ######################################################################
	// -- Below is a sample code on updating the state info. User should implement based on their needs.

	double currTime, prevTime_stateUpdate;

	double periodicStateUpdateInterval = 4.0;

	prevTime_stateUpdate = gCurrentTime();

	// -- Infinite loop to demonstrate some dynamic state change.
	do {

		currTime = gCurrentTime();

		// -- Assume we make periodic changes in the state parameters
		if(currTime - prevTime_stateUpdate >= periodicStateUpdateInterval) {
			prevTime_stateUpdate = currTime;

			// -- User-defined State Update code
			{
				pthread_mutex_lock(&gMutex);
				iot_stateParam_float = iot_stateParam_float*1.1 > 999999.0f ? iot_stateParam_float - 899999.0f : iot_stateParam_float*1.1;
				iot_stateParam_double = iot_stateParam_double*1.1 > 999999.0f ? iot_stateParam_double - 899999.0f : iot_stateParam_double*1.1;
				iot_stateParam_int = iot_stateParam_int*1.1 > 999999999 ? iot_stateParam_int - 899999999 : iot_stateParam_int*1.1;
				iot_stateParam_bool = !iot_stateParam_bool;
				pthread_mutex_unlock(&gMutex);
			}
		}

	} while(1);
	// -- ######################################################################

}

//int main() {
//
//	OmniSensr_SampleApp sampleApp;
//
//	sampleApp.Run(APP_CONFIG_FILE);
//
//	return 0;
//}
