# << Mosquitto MQTT Broker for Raspberry Pi Jessie  >>
cd ~/Downloads
wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
sudo apt-key add mosquitto-repo.gpg.key
cd /etc/apt/sources.list.d/
sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
sudo apt-get update; sudo apt-get -y install mosquitto     

# << Boost 1.59 >>
sudo apt-get -y install build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev libboost-all-dev
cd ~; mkdir Downloads; cd Downloads
wget -O boost_1_59_0.tar.gz http://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.gz/download
tar xvf boost_1_59_0.tar.gz
cd boost_1_59_0
./bootstrap.sh --prefix=/usr/local
./b2
sudo ./b2 install
