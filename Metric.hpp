/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef Metric_HPP
#define Metric_HPP
#include <string>
#include "CountDownTimer.hpp"
#include "IntervaledTaskFactory.hpp"

class Metric
{
public:
	std::vector<std::string> parseJSON(std::string s);
	Metric(std::string str);
	~Metric();
//	TODO:create UpdateInterval()
	void UpdateInterval(std::string str);
protected:
//	IntervaledTaskFactory factory;
//	IntervaledTaskPtr CollectTask;
//	IntervaledTaskPtr ReportTask;
	CountDownTimerPtr CollectTimer;
	CountDownTimerPtr ReportTimer;
};

typedef boost::shared_ptr<Metric> MetricPtr;
#endif
