/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "CountDownTimer.hpp"
//boost::mutex mCoutMutex;

CountDownTimer::CountDownTimer(int timerInterval, IntervaledTaskPtr workerObject)
        : mLocService()
		, mQuit(false)
        , mWork( mLocService )
        , mTimerThread( boost::bind( &CountDownTimer::RunIOService,this ) )
        , mTimer( mLocService,boost::posix_time::seconds(timerInterval) )
		, mExpCallback(workerObject)
{
	mTimer.async_wait( boost::bind( &CountDownTimer::OnTimerExpire, this ) );
	if(mExpCallback)
	{
		mExpCallback->updateFromShadow(timerInterval);
	}
}

void CountDownTimer::RunIOService()
{
	mLocService.run();
};

CountDownTimer::~CountDownTimer()
{
//	if(1){
//	  boost::mutex::scoped_lock lock(mCoutMutex);
//	  std::cout <<"~CountDownTimer() Interval: " << mExpCallback->getTaskInterval() << std::endl;
//	}
	boost::mutex::scoped_lock lock(mTimerMutex);
	mQuit =true;
	mLocService.stop();
	mTimerThread.join();
}

void CountDownTimer::OnTimerExpire()
{
//	if(1){
//		boost::mutex::scoped_lock lock(mCoutMutex);
//		std::cout <<" OnTimerExpire fired in thread:" << boost::this_thread::get_id() << std::endl;
//	}
	boost::mutex::scoped_lock lock(mTimerMutex);
	if(mQuit)
		return;
	if(mExpCallback)
	{
		mExpCallback->DoTask();
		mTimer.expires_at(mTimer.expires_at() + boost::posix_time::seconds(mExpCallback->getTaskInterval()));
		mTimer.async_wait(boost::bind(&CountDownTimer::OnTimerExpire, this));
	}
	// Post Each IntetvalTask Callback to Thread Pool Queue
};

void CountDownTimer::TimerReset(int newInterval)
{
	boost::mutex::scoped_lock lock(mTimerMutex);
	if(mQuit)	// check destructor
		return;
	if(mExpCallback)
	{
		mExpCallback->setInterval(newInterval);
//		TODO:update this part using mTimer....
		std::cout << mTimer.expires_at() << std::endl;
//		std::cout << boost::posix_time::microsec_clock::local_time() << std::endl;
//		std::cout << boost::posix_time::seconds(mExpCallback->getTaskInterval()) << std::endl;
		mTimer.expires_from_now(boost::posix_time::seconds(newInterval));
//		mTimer.expires_at(mTimer.expires_at() + boost::posix_time::seconds(mExpCallback->getTaskInterval()));
//		mTimer.async_wait(boost::bind(&CountDownTimer::OnTimerExpire, this));
		std::cout << mTimer.expires_at() << std::endl;
//		sleep(10);
	}
}
