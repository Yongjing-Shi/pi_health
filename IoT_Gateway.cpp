/*
 * IoT_Gateway.cpp
 *
 *  Created on: Mar 3, 2016
 *      Author: paulshin
 */
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include "unistd.h"
#include <string.h>
#include <iomanip>
#include <pthread.h>

#include "modules/iot/IoT_Common.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "modules/iot/IoT_Gateway.hpp"


#include "aws-iot-sdk/aws_iot_src/protocol/mqtt/aws_iot_mqtt_interface.h"

using namespace std;

#define DEBUG			false
#define MQTT_DEBUG		false

#define	PERIODIC_PROCESSING_CHECK_INTERVAL		0.1		// -- Unit: sec
#define MAX_BUFFER_SIZE		299

extern pthread_mutex_t gMutex;

vector<devState_t > IoT_Gateway::mDevState;

// -- Functions for MosqMQTT class
void IoT_Gateway::on_message(const struct mosquitto_message *msg) {
    if (msg == NULL) { return; }

    if(DEBUG)
		printf("-- got message @ %s: (%d, QoS %d, %s) '%s'\n",
			msg->topic, msg->payloadlen, msg->qos, msg->retain ? "R" : "!r",
			(char *)msg->payload);

    if(msg->topic == NULL || msg->payload == NULL)
    	return;

    string msg_topic(msg->topic);
    string msg_payload((char *)msg->payload);
    msg_payload.resize(msg->payloadlen);

    string topic_deltaState(AWS_IOT_SHADOW_DELTA_CHANNEL_NAME);
	string topic_control(AWS_IOT_CONTROL_CHANNEL_PREAMBLE);

    if(msg_topic.empty() == true || msg_payload.empty() == true)
		return;

    // -- If received Delta Thing Shadow message, then pass the entire message to the internal apps.
    if(msg_topic.compare(0,topic_deltaState.size(), topic_deltaState) == 0) {

    	// -- TODO: In the next phase, we may want to enable per-parameter callback function
    	// -- To do that, we need to parse the delta message here and pass the 'desired' value by calling the per-parameter callback function

    	mDeltaStateCallbackFunc(msg_payload);
    }

    // -- If received a message via Control channel, then pass the entire message to the internal apps.
    else if(msg_topic.compare(0,topic_control.size(), topic_control) == 0) {

    	mControlChannelCallbackFunc(msg_payload);
    }

}

// -- Functions for MosqMQTT class
void IoT_Gateway::on_connect(int rc)
{
	if ( rc == 0 ) {	 /* success */
		if(MQTT_DEBUG)
			cout << ">> myMosq - connected with server" << std::endl;

		mIsMqttConnected = true;

    	// -- Subscribe to topics after a successful connection is made.
    	for(std::vector<string>::iterator it = mSubTopics.begin(); it != mSubTopics.end(); ++it) {
    		string itt = *it;

    		if(MQTT_DEBUG)
    			printf("[IoT_Gateway] Subscribing to [%s]\n", itt.c_str());

    		try {
    			this->subscribe(NULL, (const char*)(itt.c_str()), QOS_1);
			} catch (exception& e) {
				if(MQTT_DEBUG)
					printf("Error: Failed to subscribe\n%s\n", e.what());
			}
    	}

	} else {
		if(MQTT_DEBUG)
			cout << ">> myMosq - Impossible to connect with server(" << rc << ")" << std::endl;

		// -- Start the mosquitto MQTT client
		start();
	}
 }

// -- Functions for MosqMQTT class
void IoT_Gateway::on_disconnect(int rc)
{

	if(MQTT_DEBUG)
		cout << ">> myMosq - disconnection(" << rc << ")" << endl;

	mIsMqttConnected = false;

	while(1) {

		sleep(1);

		int ret;

		try {
			ret = this->reconnect();
		} catch (exception& e) {
			if(MQTT_DEBUG)
				printf("Error: Failed to reconnect\n%s\n", e.what());
		}


		if(MQTT_DEBUG)
			cout << ">> myMosq - reconnect(" << ret << ")" << endl;

		if(ret == MOSQ_ERR_SUCCESS) break;
		else usleep(1e5);
	}

 }

// -- Functions for MosqMQTT class
void IoT_Gateway::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	if(MQTT_DEBUG)
		cout << ">>>>>>>>>>>> myMosq - Message (" << mid << ") succeed to be subscribed  >>>>>>>" << endl;
}

IoT_Gateway::IoT_Gateway(const char * _id, bool clean_session) : MosqMQTT(_id, clean_session) {

	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);


	// -- Read Serial Number
	{
		string cmd;
		cmd.clear();
		cmd = "cat /usr/local/www/serial_number.txt | grep Serial | sed 's/:/ /' | awk '{print $2}'";
		FILE *fp_serial = popen(cmd.c_str(), "r");
		char serialNumber[24];
		fscanf(fp_serial, "%s", serialNumber);
		mSerialNumber.append(serialNumber);

		if(DEBUG)
			printf("serialNumber = %s\n", mSerialNumber.c_str());
	}

	// -- Initialization for MosqMQTT class
	{
		 mIsMqttConnected = false;

		 mosqpp::lib_init();        // Mandatory initialization for mosquitto library
		 mPid = getpid();

		 mClientId << _id << "-" << mPid;

		 reinitialise((const char*)mSerialNumber.c_str(), true);
	}
}

IoT_Gateway::~IoT_Gateway() {

	// -- Setting for MosqMQTT class
	{
		 loop_stop();            // Kill the thread
		 mosqpp::lib_cleanup();    // Mosquitto library cleanup
	}
}


void IoT_Gateway::MessageSending_Loop() {

	if(DEBUG)
		printf("[IoT_Gateway] PeriodicProcessing loop is created. \n");

	int ret = 0;

	// -- Periodically send the state report to the ThingShadow.
	while(1) {

		// -- Send State Information to Thing Shadow if there's any thing to send.
		if(this->mIsMqttConnected == true && mStateMsgToSend.str().size() > 0) {

			string msgToSend;

			pthread_mutex_lock(&gMutex);
			msgToSend = mStateMsgToSend.str();
			pthread_mutex_unlock(&gMutex);

			if((ret = this->send_message(mMqttTopic_State.c_str(), mStateMsgToSend.str().c_str(), this->mQoS)) != 0) {
				// -- Do something

				if(DEBUG)
					printf("MQTT: State info sending was FAILED.\n");

			} else {
				// -- Do something
				pthread_mutex_lock(&gMutex);
				mStateMsgToSend.clear();
				mStateMsgToSend.str("");

				if(DEBUG)
					printf("MQTT: State info sending was successful.\n");
				pthread_mutex_unlock(&gMutex);
			}
		}


		// -- Send Data to Data channel
		if(this->mIsMqttConnected == true && mDataMsgToSend.str().size() > 0)
		{
			ostringstream dataToSend;

			pthread_mutex_lock(&gMutex);
			// -- Insert the IP address of this packet
			dataToSend << "#" << mSerialNumber << " " << mAP_IPaddr << endl;

			// -- Insert the rest data
			dataToSend << mDataMsgToSend.str();
			pthread_mutex_unlock(&gMutex);

			if(this->send_message(mMqttTopic_Data.c_str(), dataToSend.str(), this->mQoS) != 0) {
				// -- Do something

				if(DEBUG)
					printf("MQTT: Data sending was FAILED.\n");

			} else {
				// -- Do something
				pthread_mutex_lock(&gMutex);
				mDataMsgToSend.str("");
				mDataMsgToSend.clear();

				if(DEBUG)
					printf("MQTT: Data sending was successful.\n");
				pthread_mutex_unlock(&gMutex);
			}
		}

		usleep(1000*1000*PERIODIC_PROCESSING_CHECK_INTERVAL);
	}
}

void* IoT_Gateway::MessageSendingThread(void *apdata) {

	IoT_Gateway* pb = (IoT_Gateway*)apdata;

	pb->MessageSending_Loop();

	return NULL;
}

bool IoT_Gateway::Run(string appID, string topic_state, string iot_gateway_configfile) {

	mMqttConfigPath = iot_gateway_configfile;
	mMqttTopic_State = topic_state;

	mInitialTime = gCurrentTime();

	// -- Read IP address
	{
		string cmd;
		FILE *fp;

		//cmd = "curl http://myip.dnsomatic.com";											// -- Global Internet IP address
		cmd = "/sbin/ifconfig | grep Mask | head -1 | sed 's/:/ /' | awk '{print $3}'";		// -- Local IP address

		fp = popen(cmd.c_str(), "r");
		if(fp == NULL) {
			perror ("Error opening [gStreamFromAP] connection");
			exit(0);
		}

		char ip[24];
		fscanf(fp, "%s", ip);
		mAP_IPaddr.append(ip);

		if(DEBUG)
			printf("My IP address = %s\n", mAP_IPaddr.c_str());

		pclose(fp);
	}

	// -- Read config file
	ParseSetting(mMqttConfigPath);

	{
		// -- Read Store Name
		char topic_store[99] = {'\0'};
		FILE *fp_store = fopen("/usr/local/www/store_name.txt", "r");
		fscanf(fp_store, "%s", topic_store);
		mStoreName.append(topic_store);
		printf("[IoT_Gateway] topic_store = %s\n", mStoreName.c_str());

		// -- Compose the publish topic
		mMqttTopic_Data.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
		mMqttTopic_Data.append("/");
		mMqttTopic_Data.append(mStoreName);
		mMqttTopic_Data.append("/");
		mMqttTopic_Data.append(mSerialNumber);
		mMqttTopic_Data.append("/");
		mMqttTopic_Data.append(appID);


		printf("mqtt->mPubTopic = %s\n", mMqttTopic_Data.c_str());
	}

	// -- Initialize and start the mosquitto MQTT client
	{
		//mqtt->threaded_set(true);		// -- Tell if this application is threaded or not
		this->username_pw_set((const char*)this->mUsername.c_str(), (const char*)this->mPassword.c_str());		// -- Set username and pwd to the broker
		//mqtt->will_set(mqtt->mPubTopic.c_str(), 0, NULL, 1, true);

		// -- Subscribe to Delta State channel
		// -- Note: This should be done before start(), since the topics will be subscribed upon connection.
		// --       Otherwise, you need to directly call subscribe() to subscribe to a topic immediately assuming the connection is already made.
		this->subscribeToTopic(AWS_IOT_SHADOW_DELTA_CHANNEL_NAME);

		// -- Subscribe to Control channel
		// -- Note: This should be done before start(), since the topics will be subscribed upon connection.
		// --       Otherwise, you need to directly call subscribe() to subscribe to a topic immediately assuming the connection is already made.
		this->subscribeToTopic(AWS_IOT_CONTROL_CHANNEL_PREAMBLE);
		this->subscribeToTopic(AWS_IOT_CONTROL_CHANNEL_PREAMBLE"/#");

		// -- Start the mosquitto MQTT client
		this->start();
	}

	if(pthread_create(&mMessageSending_thread, &mThreadAttr, MessageSendingThread, (void *)this))
	{
		fprintf(stderr, "Error in pthread_create(MessageSendingThread in IoT_Gateway)\n");
		return false;
	} else {
		return true;
	}
}

bool IoT_Gateway::ParseSetting(string configpath) {
	FILE *fp;
//	char variable[255], c;

	fp = fopen(configpath.c_str(), "r");
	if(fp == NULL) {
		fprintf(stderr, "Error opening %s file\n", configpath.c_str());
		exit(0);
	}
	else {

		printf("[IoT_Gateway] ParseSetting(%s): \n", configpath.c_str());

		char buffer[MAX_BUFFER_SIZE];
		// -- Read from remote broker and publish to local broker
		string lineInput;
		while(fgets(buffer, MAX_BUFFER_SIZE, fp)) {	// -- Reads a line from the pipe and save it to buffer
			lineInput.clear();
			lineInput.append(buffer);

			// -- If there's any comment, then discard the rest of the line
			std::size_t found = lineInput.find_first_of("#");
			// -- If '#' is found, then discard the rest.
			if(found != string::npos)
				lineInput.resize(found);

			// -- Break the line input into words and save it into a word vector
			istringstream iss (lineInput);
			vector<string> v;
			string s;
			while(iss >> s)
				v.push_back(s);

			// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #SN, IP}
			if(v.size() >= 2 && v[0].size() > 0) {

				if(v[0].compare("VERSION") == 0) {
					mSoftwareVersion = v[1];
					printf("VERSION = %s\n", mSoftwareVersion.c_str());
				} else

				if(v[0].compare("KEEPALIVE_SECONDS") == 0) {
					this->mKeepalive = atoi(v[1].c_str());
					printf("KEEPALIVE_SECONDS = %d\n", this->mKeepalive);
				} else

				if(v[0].compare("BROKER_HOSTNAME") == 0) {
					this->mBrokerAddr.append(v[1]);
					printf("BROKER_HOSTNAME = %s\n", this->mBrokerAddr.c_str());
				} else

					if(v[0].compare("BROKER_USERNAME") == 0) {
						this->mUsername.append(v[1]);
						printf("BROKER_USERNAME = %s\n", this->mUsername.c_str());
					} else

					if(v[0].compare("BROKER_PASSWORD") == 0) {
						this->mPassword.append(v[1]);
						printf("BROKER_PASSWORD = %s\n", this->mPassword.c_str());
					} else

					if(v[0].compare("BROKER_QOS") == 0) {
						this->mQoS = atoi(v[1].c_str());
						printf("BROKER_QOS = %d\n", this->mQoS);
					} else

				if(v[0].compare("BROKER_PORT") == 0) {
					this->mBrokerPort = atoi(v[1].c_str());
					printf("BROKER_PORT = %d\n", this->mBrokerPort);
				}
			}
		}
	}

	fclose(fp);
	return true;
}

size_t IoT_Gateway::AddStateParam(unsigned int type, string key, void *value, void *cb(void *)) {
	devState_t state;
	state.type = type;
	state.key = key;
	state.value = value;
	state.cb_func = (void (*)(void*))*cb;

	mDevState.push_back(state);
	return mDevState.size();
}

// -- Compose a state info message and put it into out-going buffer.
void IoT_Gateway::SendStateInfo() {

	pthread_mutex_lock(&gMutex);

	if(mDevState.size() > 0) {
		mStateMsgToSend.clear();

		for(vector<devState_t >::iterator it = mDevState.begin(); it != mDevState.end(); it++) {
			devState_t *state = &*it;

			switch(state->type) {
				case SHADOW_JSON_INT32:
				case SHADOW_JSON_INT16:
				case SHADOW_JSON_INT8: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(int *)state->value << endl;
				}
					break;
				case SHADOW_JSON_UINT32:
				case SHADOW_JSON_UINT16:
				case SHADOW_JSON_UINT8: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(unsigned int *)state->value << endl;
				}
					break;
				case SHADOW_JSON_FLOAT: {
					// -- Insert the state information
					mStateMsgToSend << fixed << setprecision(IOT_FLOAT_PRECISION);
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(float *)state->value << endl;
				}
					break;
				case SHADOW_JSON_DOUBLE: {
					// -- Insert the state information
					mStateMsgToSend << fixed << setprecision(IOT_DOUBLE_PRECISION);
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(double *)state->value << endl;
				}
					break;
				case SHADOW_JSON_BOOL: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(bool *)state->value << endl;
				}
					break;

				case SHADOW_JSON_STRING: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(string *)state->value << endl;
				}
					break;

				default:
					break;
			}
		}
	}

	pthread_mutex_unlock(&gMutex);

	if(DEBUG)
		printf("==> Sending state info [Topic]: %s, [State Msg]:\n%s", mMqttTopic_State.c_str(), mStateMsgToSend.str().c_str());
}

// -- TODO: Allow the App to specify a topic to publish. Once the topic is specified and passed, it will be appended to the default data topic as a subtopic.
void IoT_Gateway::SendData(const string &payload) {

	// -- Insert the rest data
	pthread_mutex_lock(&gMutex);
	mDataMsgToSend << payload << endl;
	pthread_mutex_unlock(&gMutex);

	if(DEBUG)
		printf("[App --> Connector] Msg: [Topic: %s]\n%s", mMqttTopic_Data.c_str(), mDataMsgToSend.str().c_str());

}

devState_t *IoT_Gateway::FindStateParam(string paramKey) {

	devState_t *ret = NULL;

	pthread_mutex_lock(&gMutex);
	if(mDevState.size() > 0)
		for(vector<devState_t>::iterator it = mDevState.begin(); it != mDevState.end(); ++it) {
			devState_t *stateParam = &*it;
			if(stateParam->key.compare(paramKey) == 0) {
				ret = stateParam;
				break;
			}
		}
	pthread_mutex_unlock(&gMutex);

	return ret;
}
