/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "IntervaledTaskFactory.hpp"
#include "IntervaledTask.hpp"
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
class IntervaledTaskCPUCollect : public IntervaledTask{
private:
	int GetCPULoad()
	{
	   int FileHandler;
	   char FileBuffer[1024];
	   float load;
	   FileHandler = open("/proc/loadavg", O_RDONLY);
	   if(FileHandler < 0) {
		  return -1; }
	   read(FileHandler, FileBuffer, sizeof(FileBuffer) - 1);
	   sscanf(FileBuffer, "%f", &load);
	   close(FileHandler);
	   return (int)(load * 100);
	}
public:
	void DoTask()
	{
		std::cout << "Task Interval: " << mTaskInterval << "Seconds. cpu=" << GetCPULoad() << "%" << std::endl;
	}
};

class IntervaledTaskCPUReport : public IntervaledTask{
public:
	void DoTask()
	{
		std::cout << "Task Interval: " << mTaskInterval << "Seconds.  Reporting CPU now" << std::endl;
		int i;
		i=system ("mosquitto_pub -q 1 -d -h localhost -p 1883 -I test_temp_2 -t health -m \"Reporting CPU now\"");
	}
};

class IntervaledTaskTempCollect : public IntervaledTask{
public:
	void DoTask()
	{
		std::cout << "Task Interval: " << mTaskInterval  << "Seconds.  " << system("/opt/vc/bin/vcgencmd measure_temp");
	}
};

class IntervaledTaskTempReport : public IntervaledTask{
public:
	void DoTask()
	{
		std::cout << "Task Interval: " << mTaskInterval << "Seconds. Reporting Temperature now" << std::endl;
		int i;
		i=system ("mosquitto_pub -q 1 -d -h localhost -p 1883 -I test_temp_2 -t health -m \"Reporting Temperature now\"");
	}
};

class IntervaledTaskHeartBeat : public IntervaledTask{
public:
	void DoTask()
	{
		std::cout << "Task Interval: " << mTaskInterval << "Seconds. Missing Heartbeat!";
	}
};

IntervaledTaskPtr IntervaledTaskFactory::make_task(std::string choose) // string
{
	if (choose == "CPUCollect"){
		return IntervaledTaskPtr(new IntervaledTaskCPUCollect);
	}else if (choose == "CPUReport"){
		return IntervaledTaskPtr(new IntervaledTaskCPUReport);
	}else if (choose == "TempCollect"){
		return IntervaledTaskPtr(new IntervaledTaskTempCollect);
	}else if (choose == "TempReport"){
		return IntervaledTaskPtr(new IntervaledTaskTempReport);
	}else if (choose == "WifiAppHeartBeat"){
		return IntervaledTaskPtr(new IntervaledTaskHeartBeat);
	}
}

