#include "IntervaledTaskFactory.hpp"
#include "CountDownTimer.hpp"
#include "Metric.hpp"
#include "OmniSensr_Health_App.hpp"
// How to compile these files
// g++ -std=c++11 *.cpp -o output -L /urs/lib/ -lboost_system -lboost_thread -pthread -lboost_timer
// How to execute the executable file
// ./output
int main()
{
	OmniSensr_SampleApp sampleApp;

	sampleApp.Run(APP_CONFIG_FILE);
//	std::cout <<"Create Tasks" << std::endl;
//	std::list <MetricPtr> metric_list;
//	metric_list.push_back(MetricPtr(new Metric("{WifiApp:{HeartBeat:9}}")));
//	metric_list.push_back(MetricPtr(new Metric("{CPU:{Report:14,Collect:5}}")));
//	metric_list.push_back(MetricPtr(new Metric("{Temp:{Report:6,Collect:3}}")));
//
//#ifdef WIN32
//	std::cout << "Start Sleeping 1sec" << std::endl;
//	Sleep(1000);
//	std::cout << "End Sleeping 1sec" << std::endl;
//#else
//	std::cout << "Start Sleeping 1sec" << std::endl;
//	sleep(4);
//	std::cout << "End Sleeping 1sec" << std::endl;
//#endif
//
//	(*(metric_list.begin()))->UpdateInterval("{WifiApp:{HeartBeat:9}}");
//
//	std::cout << "Task Created" << std::endl;
//
//	//pause the main thread
//	//later we will use this thread to do work while the other tasks work
//#ifdef WIN32
//	std::cout << "Start Sleeping 25sec" << std::endl;
//	Sleep(25000);
//	std::cout << "End Sleeping 25sec" << std::endl;
//#else
//	std::cout << "Start Sleeping 25sec" << std::endl;
//	sleep(25);
//	std::cout << "End Sleeping 25sec" << std::endl;
//#endif
//
//	std::cout <<"Main Thread Exiting" << std::endl;

	return 0;
}
