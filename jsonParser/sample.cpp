// Reference: http://www.codeproject.com/Tips/805182/A-Fast-JSON-Parser-Loader-For-Your-Cplusplus-Proje
#include "CParserJson.h"
#include <algorithm>
//#include <Metric.hpp>
#include <iostream>

typedef struct{
	CParserVarNumber<_CONST_CHAR("Report")> m_report;
	CParserVarNumber<_CONST_CHAR("Collect")> m_collect;
}level4;

// json data to c-structure
typedef struct{
	CParserVarString<_CONST_CHAR("color")>  m_color;
	CParserVarPropertyGroup<level4,_CONST_CHAR("CPU")> m_cpu;
//	CParserVarBool<_CONST_CHAR("wifi_test_bool")> m_wifi_test_bool;
//	CParserVarNumber<_CONST_CHAR("appParam_float")>  m_appParam_float;
//	CParserVarNumber<_CONST_CHAR("temperature")>  m_temperature;
//	CParserVarBool<_CONST_CHAR("windowOpen")> m_windowOpen;
}level3;

typedef struct{
	CParserVarPropertyGroup<level3,_CONST_CHAR("desired")>  m_desired;
	CParserVarPropertyGroup<level3,_CONST_CHAR("reported")>  m_reported;
	CParserVarPropertyGroup<level3,_CONST_CHAR("delta")>  m_delta;
}level2;

typedef struct
{
//    // Default encoding for text
//    CParserVarString<_CONST_CHAR("encoding")> 		m_encoding;
//    // Example number
//    CParserVarNumber<_CONST_CHAR("number")> 		m_number;
//    // Plug-ins loaded at start-up
//    CParserVarArrayString<_CONST_CHAR("plug-ins")> 	 m_plugins;
//    // Tab indent size
//    CParserVarPropertyGroup<tIdent,_CONST_CHAR("indent")> m_indent;
	
	CParserVarPropertyGroup<level2,_CONST_CHAR("state")> 		m_state;
	CParserVarPropertyGroup<level2,_CONST_CHAR("metadata")> 	m_metadata;
	CParserVarNumber<_CONST_CHAR("version")> 					m_version;
	CParserVarString<_CONST_CHAR("clientToken")> 				m_clientToken;
	CParserVarNumber<_CONST_CHAR("timestamp")> 					m_timestamp;
}tSampleJson;


int main(int argc, char *argv[]){
	// Declare a hash_map to store<metric, interval>
//	unordered_map<metric, int> metirc_list;

//	std::cout << "FastJsonC++ ver. "<< FASTJSON_MAJOR_VERSION << "." << FASTJSON_MINOR_VERSION << "."<<FASTJSON_PATCH_VERSION << std::endl;
	if(argc <=1){
		std::cerr << "put file to parse"<< std::endl;
		return 0;
	}
    // declare our data var interface.
    CParserVarPropertyGroup<tSampleJson> * data_json;

    // create json-parser
    CParserJson<tSampleJson> * parser = new CParserJson<tSampleJson>();

    // parse our file
    if(parser->parseFile(argv[1])){
        // get data from parser.
//		std::cout << "elements:" << parser->getData()->size()<< std::endl;
		// the values before modifications.
//		std::cout << " Before modifications:"<< std::endl;
//		std::cout << parser->getData()->cpp2json();
		for(unsigned g = 0; g < parser->getData()->size(); g++){
			data_json = parser->getData()->at(g); // gets first element group...

   			// From here we can operate with loaded data in our program using c++ operators
			// put m_use_space to false...
//			data_json->m_indent.m_use_space = false;
			std::cout <<"CPU_Collect:"<< data_json->m_state.m_delta.m_cpu.m_collect << std::endl;
			data_json->m_state.m_delta.m_cpu.m_collect = "2222";
			std::cout <<"CPU_Collect:"<< data_json->m_state.m_delta.m_cpu.m_collect << std::endl;
			std::cout <<"CPU_Report:"<< data_json->m_state.m_delta.m_cpu.m_report << std::endl;
			data_json->m_state.m_delta.m_cpu.m_report = "11111";
			std::cout <<"CPU_Report:"<< data_json->m_state.m_delta.m_cpu.m_report << std::endl;

			// iterate of all m_plugins var and replace with random strings...
//			for(unsigned i = 0; i < data_json->m_plugins.size(); i++) {
//				data_json->m_plugins[i] = "my_randomstring"+intToString(i+g+1);
//			}
//			data_json->m_version = (int) 1111111;
//			data_json->m_timestamp = 2222222;

		}

//		std::cout << "------------------------------------------------------------------------------" << std::endl;
//		std::cout << " After modifications:"<< std::endl;
//
//		// show the modifications at screen (it can be saved in file too)
//		std::cout << parser->getData()->cpp2json();

    }
    
    // deallocates parser
    delete parser;

    // that's all :)
    return 0;

}
