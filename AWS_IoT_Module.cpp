/*
 * AWS_IoT_Module.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: paulshin
 */
#include "modules/iot/AWS_IoT_Module.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include <vector>

#define DEBUG_MSG 	false

using namespace std;

fpActionCallback_t		AWS_IoT_Module::mShadowUpdateCallbackPtr;
ostringstream AWS_IoT_Module::mDeltaMsgReceived;
bool AWS_IoT_Module::mIsDeltaMsgRecived;

pthread_mutex_t _gMutexx_;

AWS_IoT_Module::AWS_IoT_Module() {

	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

	mIsDeltaMsgRecived = false;

	mThingShadowInfo = ShadowParametersDefault;

	mJsonDocumentBuffer_Periodic_size = sizeof(mJsonDocumentBuffer_Periodic) / sizeof(mJsonDocumentBuffer_Periodic[0]);
	mJsonDocumentBuffer_Delta_size = sizeof(mJsonDocumentBuffer_Delta) / sizeof(mJsonDocumentBuffer_Delta[0]);

	mDeltaObject.pData = mJsonDocumentBuffer_Delta;
	mDeltaObject.pKey = "state";
	mDeltaObject.type = SHADOW_JSON_OBJECT;
	mDeltaObject.cb = DeltaCallback;
}

AWS_IoT_Module::~AWS_IoT_Module() {

}

void AWS_IoT_Module::ThingShadow_Init(char *thingName, char *myClientID, char *hostUrl, unsigned int hostPort, char *certDirect
									, char *rootCaFileName, char *certFileName, char *privateKeyFileName, unsigned int mqttCommandTimeout_ms, unsigned int tlsHandshakeTimeout_ms
		) {
	mThingShadowInfo.pMyThingName = thingName;
	mThingShadowInfo.pMqttClientId = myClientID;
	mThingShadowInfo.pHost = hostUrl;
	mThingShadowInfo.port = hostPort;

	mThingShadowInfo.mqttCommandTimeout_ms = mqttCommandTimeout_ms;
	mThingShadowInfo.tlsHandshakeTimeout_ms = tlsHandshakeTimeout_ms;


	sprintf(mCertDirectory, "%s", certDirect);
	sprintf(mHostAddress, "%s", hostUrl);
	mMQTT_Port = hostPort;

	char mCurrentWD[PATH_MAX + 1];
	getcwd(mCurrentWD, sizeof(mCurrentWD));
	sprintf(mRootCA, "%s/%s/%s", mCurrentWD, mCertDirectory, rootCaFileName);
	sprintf(mClientCRT, "%s/%s/%s", mCurrentWD, mCertDirectory, certFileName);
	sprintf(mClientKey, "%s/%s/%s", mCurrentWD, mCertDirectory, privateKeyFileName);

//	DEBUG("Using rootCA %s", mRootCA);
//	DEBUG("Using clientCRT %s", mClientCRT);
//	DEBUG("Using clientKey %s", mClientKey);

	mThingShadowInfo.pClientCRT = mClientCRT;
	mThingShadowInfo.pClientKey = mClientKey;
	mThingShadowInfo.pRootCA = mRootCA;

	DEBUG("ThingShadowInfo.pMyThingName = %s", mThingShadowInfo.pMyThingName);
	DEBUG("ThingShadowInfo.pMqttClientId = %s", mThingShadowInfo.pMqttClientId);
	DEBUG("ThingShadowInfo.pHost = %s", mThingShadowInfo.pHost);
	DEBUG("ThingShadowInfo.port = %d", mThingShadowInfo.port);
	DEBUG("ThingShadowInfo.pClientCRT = %s", mThingShadowInfo.pClientCRT);
	DEBUG("ThingShadowInfo.pClientKey = %s", mThingShadowInfo.pClientKey);
	DEBUG("ThingShadowInfo.pRootCA = %s", mThingShadowInfo.pRootCA);

}

IoT_Error_t AWS_IoT_Module::Shadow_ConnectInit() {

	// -- MQTT init
	aws_iot_mqtt_init(&mMqttClient);
	aws_iot_mqtt_autoreconnect_set_status(true);


	INFO("[AWS_IoT_Module] Shadow Init");

	IoT_Error_t rc = NONE_ERROR;

	rc = aws_iot_shadow_init(&mMqttClient);
	if (NONE_ERROR != rc) {
		ERROR("Shadow Connection Error");
		return rc;
	}

	INFO("[AWS_IoT_Module] Shadow Connect");
	rc = aws_iot_shadow_connect(&mMqttClient, &mThingShadowInfo);
	if (NONE_ERROR != rc) {
		ERROR("Shadow Connection Error");
		return rc;
	}

	if (NONE_ERROR != rc) {
		ERROR("Shadow Connection Error %d", rc);
	}

	/*
	 * Register the jsonStruct object as delta object
	 */
	rc = aws_iot_shadow_register_delta(&mMqttClient, &mDeltaObject);

	for(vector<jsonStruct_t *>::iterator it = mAppParams.begin(); it != mAppParams.end(); ++it) {
		jsonStruct_t * appParams = *it;

		if(appParams->cb != NULL)
			rc = aws_iot_shadow_register_delta(&mMqttClient, appParams);
	}

	if (NONE_ERROR != rc) {
		ERROR("Shadow Register Delta Error");
	}

	return rc;
}

int AWS_IoT_Module::Shadow_addParam(jsonStruct_t *paramNew) {
	int ret;
	pthread_mutex_lock(&_gMutexx_);
	mAppParams.push_back(paramNew);
	ret = mAppParams.size();
	pthread_mutex_unlock(&_gMutexx_);

//	printf("mAppParams [%d]th: Key = %s\n", mAppParams.size(), paramNew->pKey);

	return ret;

}

void AWS_IoT_Module::DeltaCallback(const char *pJsonValueBuffer, const uint32_t valueLength, jsonStruct_t *pContext) {

	char valueOnlyBuf[AWS_IOT_MQTT_RX_BUF_LEN] = {'\0'};
	snprintf(valueOnlyBuf, valueLength+1, "%.*s", valueLength+1, pJsonValueBuffer);

	INFO("---------------------------------------------------------------------------------------");
	INFO("[AWS_IoT_Module] Received Delta message %.*s --> [%s]", valueLength, pJsonValueBuffer, valueOnlyBuf);
	INFO("---------------------------------------------------------------------------------------");

	mDeltaMsgReceived.str("");
	mDeltaMsgReceived.clear();
	mIsDeltaMsgRecived = true;

	mDeltaMsgReceived << valueOnlyBuf << endl;
}

/**
 * @brief This function builds a full Shadow expected JSON document by putting the data in the reported section
 *
 * @param pJsonDocument Buffer to be filled up with the JSON data
 * @param maxSizeOfJsonDocument maximum size of the buffer that could be used to fill
 * @param pReceivedDeltaData This is the data that will be embedded in the reported section of the JSON document
 * @param lengthDelta Length of the data
 */
//bool AWS_IoT_Module::buildJSONForReported(char *pJsonDocument, size_t maxSizeOfJsonDocument, const char *pReceivedDeltaData, uint32_t lengthDelta) {
//	int32_t ret;
//
//	if (pJsonDocument == NULL) {
//		return false;
//	}
//
//	char tempClientTokenBuffer[MAX_SIZE_CLIENT_TOKEN_CLIENT_SEQUENCE];
//
//	if(aws_iot_fill_with_client_token(tempClientTokenBuffer, MAX_SIZE_CLIENT_TOKEN_CLIENT_SEQUENCE) != NONE_ERROR){
//		return false;
//	}
//
//	ret = snprintf(pJsonDocument, maxSizeOfJsonDocument, "{\"state\":{\"reported\":%.*s}, \"clientToken\":\"%s\"}", lengthDelta, pReceivedDeltaData, tempClientTokenBuffer);
//
//	if (ret >= (int32_t)maxSizeOfJsonDocument || ret < 0) {
//		return false;
//	}
//
//	return true;
//}

void AWS_IoT_Module::Shadow_UpdateMsg(float temperature, char *shadowMsg) {

	sprintf(shadowMsg, "(Temperature = %f)", temperature);

}

//void AWS_IoT_Shadow::parseInputArgsForConnectParams(int argc, char** argv) {
//	int opt;
//
//	while (-1 != (opt = getopt(argc, argv, "h:p:c:n:"))) {
//		switch (opt) {
//		case 'h':
//			strcpy(mHostAddress, optarg);
//			DEBUG("Host %s", optarg);
//			break;
//		case 'p':
//			mMQTT_Port = atoi(optarg);
//			DEBUG("arg %s", optarg);
//			break;
//		case 'c':
//			strcpy(mCertDirectory, optarg);
//			DEBUG("cert root directory %s", optarg);
//			break;
//		case 'n':
//			mNumPubs = atoi(optarg);
//			DEBUG("num pubs %s", optarg);
//			break;
//		case '?':
//			if (optopt == 'c') {
//				ERROR("Option -%c requires an argument.", optopt);
//			} else if (isprint(optopt)) {
//				WARN("Unknown option `-%c'.", optopt);
//			} else {
//				WARN("Unknown option character `\\x%x'.", optopt);
//			}
//			break;
//		default:
//			ERROR("ERROR in command line argument parsing");
//			break;
//		}
//	}
//
//}

void AWS_IoT_Module::PeriodicProcessing_Loop() {

	IoT_Error_t rc = NONE_ERROR;

	double currTime, prevTime_shadowUpdate, prevTime_delta, prevTime_pub;

	prevTime_shadowUpdate = prevTime_delta = prevTime_pub = gCurrentTime() - mInitialTime;

	INFO("[AWS_IoT_Module] PeriodicProcessing loop is created. \n");

	while(1) {

		if(mMQTT_Subscriber.size() > 0)
			for(vector<SubParam_t>::iterator it = mMQTT_Subscriber.begin(); it != mMQTT_Subscriber.end(); ++it) {
				SubParam_t *subParam = &*it;

				INFO("[AWS_IoT_Module] Subscribing to %s...", subParam->param.pTopic);

				rc = aws_iot_mqtt_subscribe(&subParam->param);
				if (NONE_ERROR != rc) {
					ERROR("Error subscribing %d", rc);
				}
			}

		// loop and publish a change to Thing Shadow in mAppParams
		while (NONE_ERROR == rc) {

			// -- Lets check for the incoming messages (ThingShadow & Data) for a while like 200 ms.
			rc = aws_iot_shadow_yield(&mMqttClient, 100);
			rc = aws_iot_mqtt_yield(100);

			currTime = gCurrentTime() - mInitialTime;

			// -- Send data by publishing to a topic via MQTT
			if(currTime - prevTime_pub >= mMinPeriodicDataPublishInterval) {
				prevTime_pub = currTime;

				pthread_mutex_lock(&_gMutexx_);
				mPubMsg_Queue_out = mPubMsg_Queue_in;
				mPubMsg_Queue_in.clear();
				pthread_mutex_unlock(&_gMutexx_);

					if(mPubMsg_Queue_out.size() > 0) {
						for(vector<PubMsg_t>::iterator it = mPubMsg_Queue_out.begin(); it != mPubMsg_Queue_out.end(); ++it) {
							PubMsg_t *msg_to_pub = &*it;

								//DEBUG("mPubMsg_Publisher.mPubParams:  topic [%s]\n%s",  msg_to_pub.topic.c_str(), (char *)msg_to_pub.msg.c_str());
							int ret = 99;
							if ((ret = mMQTT_Publisher.Publish(msg_to_pub->qos, msg_to_pub->topic, msg_to_pub->msg) != NONE_ERROR)) {
								ERROR("Error publishing [%d]", ret);
							}
						}
						mPubMsg_Queue_out.clear();
					}
			}

			// -- Upon the detection of a update delta message, it tries to update Shadow with the new value from the delta message
//			if (*mMessageArrivedOnDelta == true && currTime - prevTime_delta >= mAWS_IOT_MODULE_MIN_DELTA_MSG_RESPONSE_WAIT_TIME) {
//				prevTime_delta = currTime;
//
//				// -- Resend the current Shadow message to let AWS IoT know the current Shadow values are updated according to the 'desired' Shadow values.
//				// -- AWS IoT then looks at the 'reported' Shadow values and see if they are the same as the 'desired' values. If they are not the same, then issues another Shadow Delta message to the device.
//				{
//					// -- Initialize the Shadow message buffer
//					rc = aws_iot_shadow_init_json_document(mJsonDocumentBuffer_Delta, mJsonDocumentBuffer_Delta_size);
//
//					// -- Update Shadow at AWS IoT with the new values created within the device
//					if (rc == NONE_ERROR) {
//
//						// -- Add the Shadow values for the attributes to the Shadow message buffer
//						if(mAppParams.size() > 0) {
//							for(vector<jsonStruct_t *>::iterator it = mAppParams.begin(); it != mAppParams.end(); ++it) {
//									jsonStruct_t * param = *it;
//									rc = aws_iot_shadow_add_reported(mJsonDocumentBuffer_Delta, mJsonDocumentBuffer_Delta_size, 1, param);
//							}
//						}
//
//						if (rc == NONE_ERROR) {
//
//							// -- Add some more meta-data to the message buffer in the JSON format and finalize the message composition
//							rc = aws_iot_finalize_json_document(mJsonDocumentBuffer_Delta, mJsonDocumentBuffer_Delta_size);
//
//							if (rc == NONE_ERROR) {
//								DEBUG("*****************************************************************************************\n");
//								DEBUG("Update upon Delta Thing Shadow: %s", mJsonDocumentBuffer_Delta);
//
//								// -- Actually send the Shadow update to AWS IoT
//								rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Delta, mShadowUpdateCallbackPtr, NULL, 4, true);
//								DEBUG("*****************************************************************************************\n");
//							}
//						}
//					}
//				}
//
//				//pthread_mutex_lock(&_gMutexx_);
//				*mMessageArrivedOnDelta = false;
//				//pthread_mutex_unlock(&_gMutexx_);
//			}

			// -- Send periodic Thing Shadow report
			if(currTime - prevTime_shadowUpdate >= mPeriodicShadowReportInterval) {
				prevTime_shadowUpdate = currTime;

					// -- Initialize the Shadow message buffer
					rc = aws_iot_shadow_init_json_document(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size);

					// -- Update Shadow at AWS IoT with the new values created within the device
					if (rc == NONE_ERROR) {

//						// -------------------------
//						printf("BEFORE adding a string\n");
//						jsonStruct_t param;
//
//						string param_value("123 fgr g w\"e \" ' ;' kkl $ @test string test string");
//						param.cb = NULL;
//						param.pKey = "test_string";
//						param.pData = (void *)param_value.c_str();	// --  This parameter is automatically updated with the 'desired' parameter whenever the corresponding delta callback is called.
//						param.type = SHADOW_JSON_STRING;
//
//						aws_iot_shadow_add_reported(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size, 1, &param);
//						printf("AFTER adding a string\n");
//						// -------------------------

						// -- Add the Shadow values for the attributes to the Shadow message buffer
						pthread_mutex_lock(&_gMutexx_);
						if(mAppParams.size() > 0) {
							for(vector<jsonStruct_t *>::iterator it = mAppParams.begin(); it != mAppParams.end(); ++it) {
									jsonStruct_t * param = *it;

									//printf("aws_iot_shadow_add_reported() ==> mAppParams [%s]\n", param->pKey);

									rc = aws_iot_shadow_add_reported(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size, 1, param);

									if (NONE_ERROR != rc)
										ERROR("aws_iot_shadow_add_reported() error %d", rc);
							}

							// -- Once all the Thing Shadow parameters are sent, then erase all and wait for the state info from individual apps.
							mAppParams.clear();
						}
						pthread_mutex_unlock(&_gMutexx_);

						if (rc == NONE_ERROR) {

							// -- Add some more meta-data to the message buffer in the JSON format and finalize the message composition
							rc = aws_iot_finalize_json_document(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size);

							if (rc == NONE_ERROR) {
								INFO("\n=======================================================================================\n");
								INFO("[AWS_IoT_Module] Report Thing Shadow: %s", mJsonDocumentBuffer_Periodic);

								// -- Actually send the Shadow update to AWS IoT
								rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, mShadowUpdateCallbackPtr, NULL, 4, true);
								INFO("\n=======================================================================================\n");
							} else if (NONE_ERROR != rc)
								ERROR("[AWS_IoT_Module] aws_iot_shadow_update() error %d", rc);
						}
					} else if (NONE_ERROR != rc)
						ERROR("[AWS_IoT_Module] aws_iot_shadow_init_json_document() error %d", rc);

			}


			//usleep(1000*10);
		}

		INFO("[AWS_IoT_Module] Disconnecting");

		if (NONE_ERROR != Shadow_Disconnect()) {
			ERROR("[AWS_IoT_Module] Disconnect error %d", rc);
		}

		sleep(1);

		if (NONE_ERROR != (rc = Shadow_ConnectInit())) {
			ERROR("[AWS_IoT_Module] awsShadow.ShadowConnectInit() error %d", rc);
		}

	}
}


void* AWS_IoT_Module::PeriodicProcessingThread(void *apdata) {

	AWS_IoT_Module* pb = (AWS_IoT_Module*)apdata;

	pb->PeriodicProcessing_Loop();

    return NULL;
}

IoT_Error_t AWS_IoT_Module::Run() {

	IoT_Error_t rc = NONE_ERROR;

	mInitialTime = gCurrentTime();

	rc = Shadow_ConnectInit();

	if (NONE_ERROR != rc) {
		ERROR("awsShadow.ShadowConnectInit() error %d", rc);
	}

	// -- If set, then erase the remote ThingShadow at AWS IoT Cloud
	if(mNeedToEraseThingShadow > 0)
	{
		sleep(1);

			DEBUG("[AWS_IoT_Module] Deleting Thing Shadow");

		int ret = 99;
		// -- Delete the Thing Shadow at the remote AWS IoT Server
		while((ret = Shadow_Delete()) != NONE_ERROR) {

			DEBUG("[AWS_IoT_Module] Delete Failed [%d].. re-attempting", ret);

			sleep(2);
		}

		DEBUG("[AWS_IoT_Module] Deleted Thing Shadow");

		// -- Reset the local version number of the Thing Shadow
		Shadow_Reset_LastReceivedVersion();
	}

/*
	Thread 1:
		- Send periodic Shadow report and queued publish messages
	Main thread:
		- Add the on-demand publish messages into queue
*/
	if(pthread_create(&mPeriodicProcessing_thread, &mThreadAttr, PeriodicProcessingThread, (void *)this))
	{
		fprintf(stderr, "Error in pthread_create(PeriodicProcessingThread in AWS_IoT_Module)\n");
		return GENERIC_ERROR;
	}

		return rc;
}

