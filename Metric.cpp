/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "Metric.hpp"

// Split long string into string[5], e.g. {CPU:{Report:35,Collect:5}} to {"CPU","Report","35","Collect","5"}
std::vector<std::string> Metric::parseJSON(std::string s)
{
	std::vector<std::string> strArray;
	s.erase(std::remove(s.begin(),s.end(),'{'),s.end());
	s.erase(std::remove(s.begin(),s.end(),'}'),s.end());
	std::replace( s.begin(), s.end(), ',', ':'); // replace all ',' to ':'
	std::string delimiter = ":";
	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		strArray.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	strArray.push_back(s);
	return strArray;
}

Metric::Metric(std::string str)
{
	// Split long string into string[5], e.g. {CPU:{Report:35,Collect:5}} to {"CPU","Report","35","Collect","5"}
	std::vector<std::string> strArray = Metric::parseJSON(str);
	if(strArray.size()<5)
		strArray.push_back("");
	for(int i=1; i<4; i+=2)
	{
		if (strArray[i] == "Collect")
		{
//			CollectTask = IntervaledTaskFactory::make_task(strArray[0]+strArray[i]);
			CollectTimer = CountDownTimerPtr(new CountDownTimer(std::stoi(strArray[i+1]),IntervaledTaskFactory::make_task(strArray[0]+strArray[i])));
		}
		if (strArray[i] == "Report")
		{
//			ReportTask = IntervaledTaskFactory::make_task(strArray[0]+strArray[i]);
			ReportTimer = CountDownTimerPtr(new CountDownTimer(std::stoi(strArray[i+1]),IntervaledTaskFactory::make_task(strArray[0]+strArray[i])));
		}
		if (strArray[i] == "HeartBeat")
		{
//			ReportTask = IntervaledTaskFactory::make_task(strArray[0]+strArray[i]);
			ReportTimer = CountDownTimerPtr(new CountDownTimer(std::stoi(strArray[i+1]),IntervaledTaskFactory::make_task(strArray[0]+strArray[i])));
		}
	}
}

void Metric::UpdateInterval(std::string str)
{
		std::vector<std::string> strArray = Metric::parseJSON(str);
		if (strArray[1] == "Collect")
		{
			CollectTimer->TimerReset(std::stoi(strArray[2]));
		}
		if (strArray[1] == "Report")
		{
			ReportTimer->TimerReset(std::stoi(strArray[2]));
		}
		if (strArray[1] == "HeartBeat")
		{
			ReportTimer->TimerReset(std::stoi(strArray[2]));
		}
}

Metric::~Metric()
{
}
