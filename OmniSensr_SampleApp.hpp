/*
 * OmniSensr_SampleApp.hpp
 *
 *  Created on: Mar 15, 2016
 *      Author: paulshin
 */
#include <string>
#include <string.h>
#include <sstream>
#include <cstdio>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include <iomanip>
#include "modules/iot/IoT_Gateway.hpp"
using namespace std;
#ifndef OMNISENSR_SAMPLE_HPP_
#define OMNISENSR_SAMPLE_HPP_

#define APP_CONFIG_FILE				"data_toBeDeployed/OmniSensr_SampleApp/OmniSensr_SampleApp_config.txt"
#define IOT_GATEWAY_CONFIG_FILE		"data_toBeDeployed/IoT_Gateway/IoT_Gateway_config.txt"

class OmniSensr_SampleApp {

public:

	OmniSensr_SampleApp();
	virtual ~OmniSensr_SampleApp();

	bool ParseSetting(string configpath);
	void Run(string configFile);

protected:

	pthread_attr_t mThreadAttr;
	pthread_t mPeriodicProcessing_thread;

	int mPeriodicBufferProcessingInterval;
	int mPeriodicStateReportingInterval;
	string mSoftwareVersion;

	string mAppID;
	string mMqttTopic_State;
	double mInitialTime;

	static void *PeriodicProcessingThread(void *apData);
	void PeriodicProcessing_Loop();

	// -- ######################################################################
	// -- Declare iot gateway
	static IoT_Gateway	mIoTGateway;

	// -- Define callback functions for messages from Delta State and Control Channel
	static void IoT_DeltaState_CallBack_func(string deltaMsg);
	static void IoT_ControlChannel_CallBack_func(string controlMsg);

};

#endif /* OMNISENSR_SAMPLE_HPP_ */
